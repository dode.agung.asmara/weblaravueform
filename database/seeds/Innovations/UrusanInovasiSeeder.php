<?php

use App\Models\Innovations\UrusanInovasi;
use Illuminate\Database\Seeder;

class UrusanInovasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UrusanInovasi::create(array('name' => 'Unselected'));
        UrusanInovasi::create(array('name' => 'Pendidikan'));
        UrusanInovasi::create(array('name' => 'Kesehatan'));
        UrusanInovasi::create(array('name' => 'Pekerjaan Umum dan Penataan Ruang'));
        UrusanInovasi::create(array('name' => 'Perumahan Rakyat dan Kawasan Pemukiman'));
        UrusanInovasi::create(array('name' => 'Ketentraman'));
        UrusanInovasi::create(array('name' => 'Ketertiban Umum'));
        UrusanInovasi::create(array('name' => 'Perlindungan Masyarakat'));
        UrusanInovasi::create(array('name' => 'Sosial'));
        UrusanInovasi::create(array('name' => 'Tenaga Kerja'));
        UrusanInovasi::create(array('name' => 'Pemberdayaan Perempuan dan Perlindungan Anak'));
        UrusanInovasi::create(array('name' => 'Pangan'));
        UrusanInovasi::create(array('name' => 'Pertanahan'));
        UrusanInovasi::create(array('name' => 'Lingkungan Hidup'));
        UrusanInovasi::create(array('name' => 'Administrasi Kependudukan dan Pencatatan Sipil'));
        UrusanInovasi::create(array('name' => 'Pemberdayaan Masyarakat dan Desa'));
        UrusanInovasi::create(array('name' => 'Pengendalian Penduduk dan Keluarga Berencana'));
        UrusanInovasi::create(array('name' => 'Perhubungan'));
        UrusanInovasi::create(array('name' => 'Komunikasi dan Informatika'));
        UrusanInovasi::create(array('name' => 'Koperasi, usaha kecil dan menengah'));
        UrusanInovasi::create(array('name' => 'Penanaman Modal'));
        UrusanInovasi::create(array('name' => 'Kepemudaan dan Olahraga'));
        UrusanInovasi::create(array('name' => 'Statistik'));
        UrusanInovasi::create(array('name' => 'Persandian'));
        UrusanInovasi::create(array('name' => 'Kebudayaan'));
        UrusanInovasi::create(array('name' => 'Perpustakaan'));
        UrusanInovasi::create(array('name' => 'Kearsipan'));
        UrusanInovasi::create(array('name' => 'Kelautan dan Perikanan'));
        UrusanInovasi::create(array('name' => 'Pariwisata'));
        UrusanInovasi::create(array('name' => 'Pertanian'));
        UrusanInovasi::create(array('name' => 'Kehutanan'));
        UrusanInovasi::create(array('name' => 'Energi dan Sumber Daya Mineral'));
        UrusanInovasi::create(array('name' => 'Perdagangan'));
        UrusanInovasi::create(array('name' => 'Perindustrian'));
        UrusanInovasi::create(array('name' => 'Transmigrasi'));
        UrusanInovasi::create(array('name' => 'Perencanaan'));
        UrusanInovasi::create(array('name' => 'Keuangan'));
        UrusanInovasi::create(array('name' => 'Kepegawaian'));
        UrusanInovasi::create(array('name' => 'Pendidikan dan pelatihan'));
        UrusanInovasi::create(array('name' => 'Penelitian dan pengembangan'));
        UrusanInovasi::create(array('name' => 'Fungsi penunjang lainnya'));
    }
}
