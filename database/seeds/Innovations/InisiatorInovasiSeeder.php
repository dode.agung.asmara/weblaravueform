<?php


use Illuminate\Database\Seeder;
use App\Models\Innovations\InisiatorInovasi;

class InisiatorInovasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            // DB::table('inisiator_inovasi')->delete();
            InisiatorInovasi::create(array('name' => 'Unselected'));
            InisiatorInovasi::create(array('name' => 'Kepala Daerah'));
            InisiatorInovasi::create(array('name' => 'Anggota Daerah'));
            InisiatorInovasi::create(array('name' => 'OPD'));
            InisiatorInovasi::create(array('name' => 'ASN'));
            InisiatorInovasi::create(array('name' => 'Masyarakat'));

    }
}
