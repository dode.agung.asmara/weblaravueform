<?php

use Illuminate\Database\Seeder;
use App\Models\Innovations\BentukInovasi;

class BentukInovasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            // DB::table('bentuk_inovasi')->delete();
            BentukInovasi::create(array('name' => 'Unselected'));
            BentukInovasi::create(array('name' => 'Inovasi Pelayanan Publik'));
            BentukInovasi::create(array('name' => 'Inovasi Tata Kelola Pemerintahan Daerah'));
            BentukInovasi::create(array('name' => 'Inovasi Daerah Lainnya (Sesuai Dengan Urusan Pemerintahan yang Menjadi Kewenangan Daerah)'));
    }
}
