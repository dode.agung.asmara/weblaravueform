<?php

use App\Models\Innovations\AnggaranInovasi;
use Illuminate\Database\Seeder;

class AnggaranInovasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AnggaranInovasi::create(array('name' => 'Unselected'));
        AnggaranInovasi::create(array('name' => 'Anggaran tersedia'));
        AnggaranInovasi::create(array('name' => 'Belum tersedia'));

    }
}
