<?php

use Illuminate\Database\Seeder;
use App\Models\Innovations\Innovation;
use App\Models\Innovations\Hki;
use App\Models\Innovations\PengalamanInovasi;
use App\Models\Innovations\PenghargaanInovasi;
use App\Models\VerifikasiDokumen;

class InovasiSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        Innovation::truncate();
        Hki::truncate();
        VerifikasiDokumen::truncate();
        PengalamanInovasi::truncate();
        PenghargaanInovasi::truncate();

        collect([
            [
                'innovator_id' => 1,
                'kuisioner_id' => 1,
                // 'checkinovasi' => 0,
                'status' => 'draft',
                'nama_singkat' => 'inovasi opd',
                'nama_lengkap' => 'Inovasi OPD',
                // 'tahapan_inovasi_id' => 2,
                // 'inisiator_inovasi_id' => 2,
                // 'sistem_inovasi_id' => 2,
                // 'bentuk_inovasi_id' => 2,
                // 'jenis_inovasi_id' => 2,
                // 'covid_inovasi_id' => 2,
                // 'bidang_inovasi_id' => 2,
                // 'waktu_ujicoba' => '2022-02-02',
                // 'waktu_implementasi' => '2022-02-02',
                // 'rancang_bangun' => '-',
                // 'tujuan' => '-',
                // 'manfaat' => '-',
                // 'hasil' => '-',
                // 'anggaran_inovasi_id' => 2,
                // 'ket_anggaran' => '-',
                // 'file_anggaran' => '',
                // 'proses_inovasi_id' => 2,
                // 'ket_proses' => '-',
                // 'file_proses' => '',
                // 'logo' => '',
                // 'photos' => '',
                'video_inovasi_id' => 5,
                'ket_video' => '-',
                'linkvideos' => '[{"link_video":""}]',
            ],
            [
                'innovator_id' => 2,
                'kuisioner_id' => 2,
                // 'checkinovasi' => 0,
                'status' => 'draft',
                'nama_singkat' => 'inovasi nonopd',
                'nama_lengkap' => 'Inovasi NonOPD',
                // 'tahapan_inovasi_id' => 2,
                // 'inisiator_inovasi_id' => 2,
                // 'sistem_inovasi_id' => 2,
                // 'bentuk_inovasi_id' => 2,
                // 'jenis_inovasi_id' => 2,
                // 'covid_inovasi_id' => 2,
                // 'bidang_inovasi_id' => 2,
                // 'waktu_ujicoba' => '2022-02-02',
                // 'waktu_implementasi' => '2022-02-02',
                // 'rancang_bangun' => '-',
                // 'tujuan' => '-',
                // 'manfaat' => '-',
                // 'hasil' => '-',
                // 'anggaran_inovasi_id' => 2,
                // 'ket_anggaran' => '-',
                // 'file_anggaran' => '',
                // 'proses_inovasi_id' => 2,
                // 'ket_proses' => '-',
                // 'file_proses' => '',
                // 'logo' => '',
                // 'photos' => '',
                'video_inovasi_id' => 5,
                'ket_video' => '-',
                'linkvideos' => '[{"link_video":""}]',
            ]

        ])->each(function($data){
            Innovation::create($data);
        });

        collect([
            [
                'innovation_id' => 1,
            ],
            [
                'innovation_id' => 2,
            ]
        ])->each(function($data){
            Hki::create($data);
        });

        collect([
            [
                'innovation_id' => 1,
            ],
            [
                'innovation_id' => 2,
            ]
        ])->each(function($data){
            PengalamanInovasi::create($data);
        });

        collect([
            [
                'innovation_id' => 1,
            ],
            [
                'innovation_id' => 2,
            ]
        ])->each(function($data){
            PenghargaanInovasi::create($data);
        });

        collect([
            [
                'auditor_id' => 1,
                'innovation_id' => 1,
                'data' => '{"nama_inovasi":[],"tahapan_inovasi":[],"inisiator_inovasi":[],"sistem_inovasi":[],"bentuk_inovasi":[],"jenis_inovasi":[],"covid_inovasi":[],"bidang_inovasi":[],"urusan_inovasi":[],"waktu_ujicoba":[],"waktu_implementasi":[],"rancang_bangun":[],"tujuan_inovasi":[],"manfaat_inovasi":[],"hasil_inovasi":[],"anggaran_inovasi":[],"proses_inovasi":[],"logo_inovasi":[],"foto_inovasi":[],"video_inovasi":[],"pengalaman_inovasi":[],"penghargaan_inovasi":[],"hki_inovasi":[]}'
            ],
            [
                'auditor_id' => 1,
                'innovation_id' => 2,
                'data' => '{"nama_inovasi":[],"tahapan_inovasi":[],"inisiator_inovasi":[],"sistem_inovasi":[],"bentuk_inovasi":[],"jenis_inovasi":[],"covid_inovasi":[],"bidang_inovasi":[],"urusan_inovasi":[],"waktu_ujicoba":[],"waktu_implementasi":[],"rancang_bangun":[],"tujuan_inovasi":[],"manfaat_inovasi":[],"hasil_inovasi":[],"anggaran_inovasi":[],"proses_inovasi":[],"logo_inovasi":[],"foto_inovasi":[],"video_inovasi":[],"pengalaman_inovasi":[],"penghargaan_inovasi":[],"hki_inovasi":[]}'
            ]
        ])->each(function($data){
            VerifikasiDokumen::create($data);
        });

        $this->enableForeignKeys();

    }
}
