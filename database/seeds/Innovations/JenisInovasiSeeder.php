<?php

use Illuminate\Database\Seeder;
use App\Models\Innovations\JenisInovasi;

class JenisInovasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            // DB::table('jenis_inovasi')->delete();
            JenisInovasi::create(array('name' => 'Unselected'));
            JenisInovasi::create(array('name' => 'Inovasi Produk'));
            JenisInovasi::create(array('name' => 'Inovasi Pelayanan'));
            JenisInovasi::create(array('name' => 'Inovasi Proses'));
    }
}
