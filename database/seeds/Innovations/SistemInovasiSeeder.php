<?php

use App\Models\Innovations\SistemInovasi;
use Illuminate\Database\Seeder;

class SistemInovasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SistemInovasi::create(array('name' => 'Unselected'));
        SistemInovasi::create(array('name' => 'Digital'));
        SistemInovasi::create(array('name' => 'Non Digital'));
    }
}
