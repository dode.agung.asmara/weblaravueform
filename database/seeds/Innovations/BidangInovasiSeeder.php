<?php

use App\Models\Innovations\BidangInovasi;
use Illuminate\Database\Seeder;

class BidangInovasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BidangInovasi::create(array('name' => 'Unselected'));
        BidangInovasi::create(array('name' => 'Pangan'));
        BidangInovasi::create(array('name' => 'Pertahanan dan Keamanan'));
        BidangInovasi::create(array('name' => 'Manufaktur'));
        BidangInovasi::create(array('name' => 'Material Maju'));
        BidangInovasi::create(array('name' => 'Kesehatan dan Obat'));
        BidangInovasi::create(array('name' => 'Teknologi Informasi'));
        BidangInovasi::create(array('name' => 'Energi dan Sumber Daya Mineral'));
        BidangInovasi::create(array('name' => 'Transportasi'));

    }
}
