<?php

use Illuminate\Database\Seeder;
use App\Models\Innovations\TahapanInovasi;

class TahapanInovasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            // DB::table('tahapan_inovasi')->delete();
            TahapanInovasi::create(array('name' => 'Unselected'));
            TahapanInovasi::create(array('name' => 'Inisiatif'));
            TahapanInovasi::create(array('name' => 'Uji Coba'));
            TahapanInovasi::create(array('name' => 'Penerapan'));

    }
}
