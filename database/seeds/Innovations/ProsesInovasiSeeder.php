<?php

use App\Models\Innovations\ProsesInovasi;
use Illuminate\Database\Seeder;

class ProsesInovasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProsesInovasi::create(array('name' => 'Unselected'));
        ProsesInovasi::create(array('name' => 'Inkubasi Inisiatif Inovasi Daerah'));
        ProsesInovasi::create(array('name' => 'Evaluasi Inisiatif Inovasi Daerah'));
        ProsesInovasi::create(array('name' => 'Penetapan Inovasi Daerah'));
        ProsesInovasi::create(array('name' => 'Uji Coba Inovasi Daerah'));
        ProsesInovasi::create(array('name' => 'Evaluasi Uji Coba Inovasi Daerah'));
        ProsesInovasi::create(array('name' => 'Penerapan Inovasi Daerah'));

    }
}
