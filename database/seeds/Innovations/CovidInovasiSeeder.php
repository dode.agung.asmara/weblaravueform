<?php

use Illuminate\Database\Seeder;
use App\Models\Innovations\CovidInovasi;

class CovidInovasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            DB::table('covid_inovasi')->delete();
            CovidInovasi::create(array('name' => 'Unselected'));
            CovidInovasi::create(array('name' => 'Inovasi Covid-19'));
            CovidInovasi::create(array('name' => 'Inovasi Non Covid-19'));
    }
}
