<?php

use App\Models\Innovations\VideoInovasi;
use Illuminate\Database\Seeder;

class VideoInovasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        VideoInovasi::create(array('name' => 'Unselected'));
        VideoInovasi::create(array('name' => 'Memenuhi 1 atau 2 unsur substansi'));
        VideoInovasi::create(array('name' => 'Memenuhi 3 atau 4 unsur substansi'));
        VideoInovasi::create(array('name' => 'Memenuhi 5 unsur substansi'));
        VideoInovasi::create(array('name' => 'Belum tersedia'));
    }
}
