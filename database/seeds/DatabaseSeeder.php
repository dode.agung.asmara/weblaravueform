<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use App\Models\Auth\User;
use App\Models\Forms\RegulasiInovasi;
use App\Models\Innovators\Innovators;
use App\Models\Sisfo\Student;
use App\Models\Sisfo\Employee;
use App\Models\Sisfo\Program;
use App\Models\Sisfo\Faculty;
use App\Models\Sisfo\Curriculum;
use App\Models\Sisfo\CurriculumPlan;
// use App\Models\Sisfo\MapperCoursePlanItemTableSeeder;

class DatabaseSeeder extends Seeder
{
    use TruncateTable;

    /**
     * Seed the application's database.
     */
    public function run()
    {
        Model::unguard();

        $this->truncateMultiple([
            'cache',
            'failed_jobs',
            'ledgers',
            'jobs',
            'sessions',
        ]);

        $this->call([
            AuthTableSeeder::class,
            
        //innovators
            ContactCategorysSeeder::class,
            InnovatorCategorysSeeder::class,
            JenisInnovatorSeeder::class,
            InnovatorsSeeder::class,
        //innovations
            TahapanInovasiSeeder::class,
            InisiatorInovasiSeeder::class,
            SistemInovasiSeeder::class,
            BentukInovasiSeeder::class,
            JenisInovasiSeeder::class,
            CovidInovasiSeeder::class,
            BidangInovasiSeeder::class,
            UrusanInovasiSeeder::class,
            AnggaranInovasiSeeder::class,
            ProsesInovasiSeeder::class,
            VideoInovasiSeeder::class,
        //kuisioner
            RegulasiInovasiSeeder::class,
            KetersediaanSdmSeeder::class,
            DukunganAnggaranSeeder::class,
            PenggunaanItSeeder::class,
            BimtekSeeder::class,
            ProgramRenstraSeeder::class,
            JejaringInovasiSeeder::class,
            ReplikasiSeeder::class,
            PedomanTeknisSeeder::class,
            LayananPengaduanSeeder::class,
            KemudahanLayananSeeder::class,
            KemudahanProsesSeeder::class,
            OnlineSistemSeeder::class,
            KecepatanInovasiSeeder::class,
            KemanfaatanInovasiSeeder::class,
            SosialisasiInovasiSeeder::class,
            KeterlibatanAktorSeeder::class,
            PelaksanaInovasiSeeder::class,
            MonitoringInovasiSeeder::class,
            KuisionerSeeder::class,

            AuditorSeeder::class,
            InovasiSeeder::class,
        ]);

        Model::reguard();
    }
}
