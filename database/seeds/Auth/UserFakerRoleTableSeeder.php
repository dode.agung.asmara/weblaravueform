<?php

use App\Models\Auth\User;
use Illuminate\Database\Seeder;

/**
 * Class UserRoleTableSeeder.
 */
class UserFakerRoleTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        $idusers = User::find([2,5,8])->pluck('id')->toArray();
        foreach ($idusers as $iduser) {
            User::find($iduser)->assignRole(config('access.users.student_role'));
        }

        $idusers = User::find([3,6,9])->pluck('id')->toArray();
        foreach ($idusers as $iduser) {
            User::find($iduser)->assignRole(config('access.users.lecture_role'));
        }            


        $idusers = User::find([4,7])->pluck('id')->toArray();
        foreach ($idusers as $iduser) {
            User::find($iduser)->assignRole(config('access.users.employee_role'));
        }        



        $this->enableForeignKeys();
    }
}
