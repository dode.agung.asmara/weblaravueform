<?php

use App\Models\Kuisioners\PelaksanaInovasi;
use Illuminate\Database\Seeder;

class PelaksanaInovasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PelaksanaInovasi::create(array('name' => 'Unselected'));
        PelaksanaInovasi::create(array('name' => 'Ada pelaksana namun tidak ditetapkan dengan SK Kepala Daerah'));
        PelaksanaInovasi::create(array('name' => 'Ada pelaksana dan ditetapkan dengan SK Kepala Perangkat Daerah'));
        PelaksanaInovasi::create(array('name' => 'Ada pelaksana dan ditetapkan dengan SK Kepala Daerah'));
        PelaksanaInovasi::create(array('name' => 'Belum tersedia'));

    }
}
