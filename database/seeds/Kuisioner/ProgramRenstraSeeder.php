<?php

use App\Models\Kuisioners\ProgramRenstra;
use Illuminate\Database\Seeder;

class ProgramRenstraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('program_renstras')->delete();
        ProgramRenstra::create(array('name' => 'Unselected'));
        ProgramRenstra::create(array('name' => 'Pemerintah daerah sudah menuangkan program inovasi daerah dalam RPJMD'));
        ProgramRenstra::create(array('name' => 'Pemerintah daerah sudah menuangkan program inovasi daerah dalam RKPD dan telah diterapkan dalam 1 tahun terakhir'));
        ProgramRenstra::create(array('name' => 'Pemerintah daerah sudah menuangkan program inovasi daerah dalam RKPD dan telah diterapkan dalam 2 tahun terakhir'));
        ProgramRenstra::create(array('name' => 'Belum tersedia'));
    }
}
