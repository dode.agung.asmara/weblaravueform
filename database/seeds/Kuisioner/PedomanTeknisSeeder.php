<?php

use App\Models\Kuisioners\PedomanTeknis;
use Illuminate\Database\Seeder;

class PedomanTeknisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pedoman_teknis')->delete();
        PedomanTeknis::create(array('name' => 'Unselected'));
        PedomanTeknis::create(array('name' => 'Telah terdapat pedoman teknis berupa buku manual'));
        PedomanTeknis::create(array('name' => 'Telah terdapat pedoman teknis berupa buku dalam bentuk elektronik'));
        PedomanTeknis::create(array('name' => 'Telah terdapat pedoman teknis berupa buku yang dapat diakses secara online'));
        PedomanTeknis::create(array('name' => 'Belum tersedia'));
    }
}
