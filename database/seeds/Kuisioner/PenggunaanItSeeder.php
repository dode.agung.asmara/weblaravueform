<?php

use App\Models\Kuisioners\PenggunaanIt;
use Illuminate\Database\Seeder;

class PenggunaanItSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('penggunaan_its')->delete();
        PenggunaanIt::create(array('name' => 'Unselected'));
        PenggunaanIt::create(array('name' => 'Pelaksanaan kerja secara manual/non elektronik'));
        PenggunaanIt::create(array('name' => 'Pelaksanaan kerja secara elektronik'));
        PenggunaanIt::create(array('name' => 'Pelaksanaan kerja sudah didukung sistem informasi online/daring'));
        PenggunaanIt::create(array('name' => 'Belum tersedia'));
    }
}
