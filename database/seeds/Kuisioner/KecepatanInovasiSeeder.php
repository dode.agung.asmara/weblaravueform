<?php

use Illuminate\Database\Seeder;
use App\Models\Kuisioners\KecepatanInovasi;

class KecepatanInovasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kecepatan_inovasis')->delete();
        KecepatanInovasi::create(array('name' => 'Unselected'));
        KecepatanInovasi::create(array('name' => 'Inovasi dapat diciptakan dalam waktu 9 bulan keatas'));
        KecepatanInovasi::create(array('name' => 'Inovasi dapat diciptakan dalam waktu 5-8 bulan'));
        KecepatanInovasi::create(array('name' => 'Inovasi dapat diciptakan dalam waktu 1-4 bulan'));
        KecepatanInovasi::create(array('name' => 'Belum tersedia'));

    }
}
