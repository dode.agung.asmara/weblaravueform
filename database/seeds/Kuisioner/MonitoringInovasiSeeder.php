<?php

use App\Models\Kuisioners\MonitoringInovasi;
use Illuminate\Database\Seeder;

class MonitoringInovasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MonitoringInovasi::create(array('name' => 'Unselected'));
        MonitoringInovasi::create(array('name' => 'Hasil laporan monev internal Perangkat Daerah'));
        MonitoringInovasi::create(array('name' => 'Hasil pengukuran kepuasan pengguna dari evaluasi Survei Kepuasan Masyarakat'));
        MonitoringInovasi::create(array('name' => 'Hasil laporan monev eksternal berdasarkan hasil penelitian'));
        MonitoringInovasi::create(array('name' => 'Belum tersedia'));

    }
}
