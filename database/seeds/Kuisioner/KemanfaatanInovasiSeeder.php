<?php

use App\Models\Kuisioners\KemanfaatanInovasi;
use Illuminate\Database\Seeder;

class KemanfaatanInovasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kemanfaatan_inovasis')->delete();
        KemanfaatanInovasi::create(array('name' => 'Unselected'));
        KemanfaatanInovasi::create(array('name' => 'Jumlah pengguna atau penerima manfaat 1-100 orang'));
        KemanfaatanInovasi::create(array('name' => 'Jumlah pengguna atau penerima manfaat 101-200 orang'));
        KemanfaatanInovasi::create(array('name' => 'Jumlah pengguna atau penerima manfaat 201 orang keatas'));
        KemanfaatanInovasi::create(array('name' => 'Belum tersedia'));

    }
}
