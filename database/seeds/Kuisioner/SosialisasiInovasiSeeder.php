<?php

use App\Models\Kuisioners\SosialisasiInovasi;
use Illuminate\Database\Seeder;

class SosialisasiInovasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SosialisasiInovasi::create(array('name' => 'Unselected'));
        SosialisasiInovasi::create(array('name' => 'Foto kegiatan berspanduk'));
        SosialisasiInovasi::create(array('name' => 'URL media sosial'));
        SosialisasiInovasi::create(array('name' => 'Media berita'));
        SosialisasiInovasi::create(array('name' => 'Belum tersedia'));

    }
}
