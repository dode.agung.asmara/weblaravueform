<?php

use Illuminate\Database\Seeder;
use App\Models\Kuisioners\Replikasi;

class ReplikasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('replikasis')->delete();
        Replikasi::create(array('name' => 'Unselected'));
        Replikasi::create(array('name' => 'Pernah 1 kali direplikasi di daerah lain'));
        Replikasi::create(array('name' => 'Pernah 2 kali direplikasi di daerah lain'));
        Replikasi::create(array('name' => 'Pernah 3 kali direplikasi di daerah lain'));
        Replikasi::create(array('name' => 'Belum Tersedia'));
    }
}
