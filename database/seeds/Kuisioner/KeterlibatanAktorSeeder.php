<?php

use App\Models\Kuisioners\KeterlibatanAktor;
use Illuminate\Database\Seeder;

class KeterlibatanAktorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        KeterlibatanAktor::create(array('name' => 'Unselected'));
        KeterlibatanAktor::create(array('name' => 'Inovasi melibatkan 4 aktor'));
        KeterlibatanAktor::create(array('name' => 'Inovasi melibatkan 5 aktor'));
        KeterlibatanAktor::create(array('name' => 'Inovasi melibatkan lebih dari 5 aktor'));
        KeterlibatanAktor::create(array('name' => 'Belum tersedia'));

    }
}
