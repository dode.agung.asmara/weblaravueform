<?php

use Illuminate\Database\Seeder;
use App\Models\Kuisioners\Bimtek;

class BimtekSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bimteks')->delete();
        Bimtek::create(array('name' => 'Unselected'));
        Bimtek::create(array('name' => 'Dalam 2 tahun terakhir pernah 1 kali bimtek'));
        Bimtek::create(array('name' => 'Dalam 2 tahun terakhir pernah 2 kali bimtek'));
        Bimtek::create(array('name' => 'Dalam 2 tahun terakhir pernah 3 kali bimtek atau lebih'));
        Bimtek::create(array('name' => 'Belum tersedia'));
    }
}
