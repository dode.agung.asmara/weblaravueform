<?php

use App\Models\Kuisioners\KetersediaanSdm;
use Illuminate\Database\Seeder;

class KetersediaanSdmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ketersediaan_sdms')->delete();
        KetersediaanSdm::create(array('name' => 'Unselected'));
        KetersediaanSdm::create(array('name' => '1 s/d 10 orang'));
        KetersediaanSdm::create(array('name' => '11 s/d 30 orang'));
        KetersediaanSdm::create(array('name' => 'Lebih dari 31 orang'));
        KetersediaanSdm::create(array('name' => 'Belum Tersedia'));
    }
}
