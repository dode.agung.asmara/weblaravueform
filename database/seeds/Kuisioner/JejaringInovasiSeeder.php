<?php

use Illuminate\Database\Seeder;
use App\Models\Kuisioners\JejaringInovasi;

class JejaringInovasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JejaringInovasi::create(array('name' => 'Unselected'));
        JejaringInovasi::create(array('name' => 'Inovasi melibatkan 1-2 Perangkat Daerah'));
        JejaringInovasi::create(array('name' => 'Inovasi melibatkan 3-4 Perangkat Daerah'));
        JejaringInovasi::create(array('name' => 'Inovasi melibatkan 5 Perangkat Daerah atau lebih'));
        JejaringInovasi::create(array('name' => 'Belum tersedia'));

    }
}
