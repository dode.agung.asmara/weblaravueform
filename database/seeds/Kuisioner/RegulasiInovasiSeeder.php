<?php

use Illuminate\Database\Seeder;
use App\Models\Kuisioners\RegulasiInovasi;


class RegulasiInovasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('regulasi_inovasis')->delete();
        RegulasiInovasi::create(array('name' => 'Unselected'));
        RegulasiInovasi::create(array('name' => 'SK Kepala Perangkat Daerah'));
        RegulasiInovasi::create(array('name' => 'SK Kepala Daerah'));
        RegulasiInovasi::create(array('name' => 'Peraturan Kepala Daerah/Peraturan Daerah'));
        RegulasiInovasi::create(array('name' => 'Belum tersedia'));
    }
}
