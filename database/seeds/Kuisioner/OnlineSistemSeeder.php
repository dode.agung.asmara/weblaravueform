<?php

use App\Models\Kuisioners\OnlineSistem;
use Illuminate\Database\Seeder;

class OnlineSistemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('online_sistems')->delete();
        OnlineSistem::create(array('name' => 'Unselected'));
        OnlineSistem::create(array('name' => 'Ada dukungan melalui informasi website atau media sosial'));
        OnlineSistem::create(array('name' => 'Ada dukungan melalui web aplikasi'));
        OnlineSistem::create(array('name' => 'Ada dukungan melalui web aplikasi dan aplikasi mobile (android atau ios)'));
        OnlineSistem::create(array('name' => 'Belum Tersedia'));
    }
}
