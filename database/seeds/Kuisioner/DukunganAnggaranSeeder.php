<?php

use Illuminate\Database\Seeder;
use App\Models\Kuisioners\DukunganAnggaran;

class DukunganAnggaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dukungan_anggarans')->delete();
        DukunganAnggaran::create(array('name' => 'Unselected'));
        DukunganAnggaran::create(array('name' => 'Anggaran tersedia pada kegiatan inisiasi inovasi daerah'));
        DukunganAnggaran::create(array('name' => 'Anggaran tersedia pada kegiatan uji coba inovasi daerah'));
        DukunganAnggaran::create(array('name' => 'Anggaran tersedia pada kegiatan penerapan inovasi daerah'));
        DukunganAnggaran::create(array('name' => 'Belum tersedia'));
    }
}
