<?php

use App\Models\Kuisioners\KemudahanProses;
use Illuminate\Database\Seeder;

class KemudahanProsesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kemudahan_proses')->delete();
        KemudahanProses::create(array('name' => 'Unselected'));
        KemudahanProses::create(array('name' => 'Hasil inovasi diperoleh dalam waktu 6 hari keatas'));
        KemudahanProses::create(array('name' => 'Hasil inovasi diperoleh dalam waktu 2-5 hari'));
        KemudahanProses::create(array('name' => 'Hasil inovasi diperoleh dalam waktu 1 hari'));
        KemudahanProses::create(array('name' => 'Belum tersedia'));
    }
}
