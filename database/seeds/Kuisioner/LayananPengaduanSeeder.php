<?php

use App\Models\Kuisioners\LayananPengaduan;
use Illuminate\Database\Seeder;

class LayananPengaduanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('layanan_pengaduans')->delete();
        LayananPengaduan::create(array('name' => 'Unselected'));
        LayananPengaduan::create(array('name' => 'Dibawah 31%'));
        LayananPengaduan::create(array('name' => '31% s/d 60%'));
        LayananPengaduan::create(array('name' => 'Diatas 60%'));
        LayananPengaduan::create(array('name' => 'Belum tersedia'));

    }
}
