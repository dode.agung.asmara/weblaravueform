<?php

use App\Models\Kuisioners\KemudahanLayanan;
use Illuminate\Database\Seeder;

class KemudahanLayananSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kemudahan_layanans')->delete();
        KemudahanLayanan::create(array('name' => 'Unselected'));
        KemudahanLayanan::create(array('name' => 'Layanan telepon atau tatap muka langsung/noken'));
        KemudahanLayanan::create(array('name' => 'Layanan email/media sosial'));
        KemudahanLayanan::create(array('name' => 'Layanan melalui aplikasi online'));
        KemudahanLayanan::create(array('name' => 'Belum tersedia'));

    }
}
