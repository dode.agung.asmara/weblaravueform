<?php

use App\Models\Auditor;
use Illuminate\Database\Seeder;

class AuditorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Auditor::create([
            'user_id' => '1',
            'nama' => 'Default',
            'keterangan' => 'Default',
        ]);
        Auditor::create([
            'user_id' => '4',
            'nama' => 'Auditor Verifikasi',
            'keterangan' => 'Auditor Sebagai Verifikasi Inovasi',
        ]);
    }
}
