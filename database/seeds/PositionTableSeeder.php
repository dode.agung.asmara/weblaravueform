<?php

use App\Models\Sisfo\Position;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class PositionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
        for ($i = 1; $i <= 10; $i++) {
            Position::create([
                'id' => $i,
                'supervisor' => $i == 1? $i : $i-1,
                'status' => $faker->randomElement([false, true]),
                'name' => $faker->jobTitle
            ]);
        }
    }
}
