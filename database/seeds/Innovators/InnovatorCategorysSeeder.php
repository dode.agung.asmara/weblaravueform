<?php

use App\Models\Innovators\InnovatorCategory;
use Illuminate\Database\Seeder;

class InnovatorCategorysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // InnovatorCategories::create(array('name' => 'Unselected'));
        InnovatorCategory::create(array('name' => 'OPD'));
        InnovatorCategory::create(array('name' => 'NON OPD'));
    }
}
