<?php

use App\Models\Innovators\ContactCategory;
use Illuminate\Database\Seeder;

class ContactCategorysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ContactCategory::create(array('name' => 'Person Incharge'));
        ContactCategory::create(array('name' => 'Alamat'));
        ContactCategory::create(array('name' => 'Whatsapp'));
        ContactCategory::create(array('name' => 'Email'));
    }
}
