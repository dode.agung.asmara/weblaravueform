<?php

use App\Models\Innovators\Innovator;
use Illuminate\Database\Seeder;

class InnovatorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Innovator::create([
            'user_id' => '2',
            'category_id' => '1',
            'jenis_id' => '1',
            'nama' => 'Innovator OPD',
            'nama_pic' => 'OPD',
            'map' => '{"lat":-8.696607735445523,"lng":115.22698337053137}',
            'keterangan' => '',
        ]);
        Innovator::create([
            'user_id' => '3',
            'jenis_id' => '1',
            'category_id' => '2',
            'nama' => 'Innovator NON OPD',
            'nama_pic' => 'NON OPD',
            'map' => '{"lat":-8.69681984497296,"lng":115.25474959819411}',
            'keterangan' => '',
        ]);
    }
}
