<?php

use App\Models\Innovators\JenisInnovator;
use Illuminate\Database\Seeder;

class JenisInnovatorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JenisInnovator::create(array('name' => 'Perorangan'));
        JenisInnovator::create(array('name' => 'Komunitas'));
        JenisInnovator::create(array('name' => 'Pelajar'));
        JenisInnovator::create(array('name' => 'Mahasiswa'));
        JenisInnovator::create(array('name' => 'Akademisi'));
        JenisInnovator::create(array('name' => 'Lembaga/Institusi/Korporasi'));
        JenisInnovator::create(array('name' => 'Organisasi Perangkat Daerah'));
    }
}
