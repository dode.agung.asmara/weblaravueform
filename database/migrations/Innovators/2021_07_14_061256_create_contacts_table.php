<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('innovator_id');
            $table->unsignedInteger('category_id');
            $table->string('value',500)->nullable();
            $table->text('keterangan')->nullable();
            $table->text('map')->nullable();
            $table->timestamps();
        });

        Schema::table('contacts', function (Blueprint $table) {
            $table->foreign('innovator_id')->references('id')->on('innovators')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('category_id')->references('id')->on('contact_categorys')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
