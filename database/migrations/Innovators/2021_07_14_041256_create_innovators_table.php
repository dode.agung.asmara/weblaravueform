<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInnovatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('innovators', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('jenis_id');
            $table->string('nama',500)->nullable();
            $table->string('nama_pic',500)->nullable();
            $table->string('email_cadangan')->nullable();
            $table->string('kontak')->nullable();
            $table->string('kontak_cadangan')->nullable();
            $table->string('jalan')->nullable();
            $table->string('desa')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('kota')->nullable();
            $table->string('provinsi')->nullable();
            $table->text('map')->nullable();
            $table->text('keterangan')->nullable();
            $table->timestamps();
        });

        Schema::table('innovators', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('category_id')->references('id')->on('innovator_categorys')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('jenis_id')->references('id')->on('jenis_innovators')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('innovators');
    }
}
