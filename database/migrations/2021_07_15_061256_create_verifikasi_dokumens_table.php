<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVerifikasiDokumensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('verifikasi_dokumens', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('auditor_id')->default(1);
            $table->unsignedInteger('innovation_id');
            $table->text('data')->nullable();
            $table->timestamps();
        });

        Schema::table('verifikasi_dokumens', function (Blueprint $table) {
            $table->foreign('innovation_id')->references('id')->on('innovations')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('auditor_id')->references('id')->on('auditors')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verifikasi_dokumens');
    }
}
