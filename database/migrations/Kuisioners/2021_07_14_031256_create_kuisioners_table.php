<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKuisionersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kuisioners', function (Blueprint $table) {
            $table->increments('id');
            // $table->unsignedInteger('innovator_id');
            //Has Foreign key at component 2
            $table->unsignedInteger('regulasi_inovasi_id')->default(1);
            $table->text('regulasi_file')->nullable();
            $table->string('regulasi_filename',300)->nullable();

            $table->unsignedInteger('ketersediaan_sdm_id')->default(1);
            $table->text('sdm_file')->nullable();
            $table->string('sdm_filename',300)->nullable();

            $table->unsignedInteger('dukungan_anggaran_id')->default(1);
            $table->text('dukungananggaran_file')->nullable();
            $table->string('anggaran_filename',300)->nullable();

            $table->unsignedInteger('penggunaan_it_id')->default(1);
            $table->text('it_file')->nullable();
            $table->string('it_filename',300)->nullable();

            $table->unsignedInteger('bimtek_id')->default(1);
            $table->text('bimtek_file')->nullable();
            $table->string('bimtek_filename',300)->nullable();

            $table->unsignedInteger('program_renstra_id')->default(1);
            $table->text('renstra_file')->nullable();
            $table->string('renstra_filename',300)->nullable();

            $table->unsignedInteger('keterlibatan_aktor_id')->default(1);
            $table->text('keterlibatan_file')->nullable();
            $table->string('keterlibatan_filename',300)->nullable();

            $table->unsignedInteger('pelaksana_id')->default(1);
            $table->text('pelaksana_file')->nullable();
            $table->string('pelaksana_filename',300)->nullable();

            $table->unsignedInteger('jejaring_inovasi_id')->default(1);
            $table->text('jejaring_file')->nullable();
            $table->string('jejaring_filename',300)->nullable();

            $table->unsignedInteger('sosialisasi_id')->default(1);
            $table->text('sosialisasi_file')->nullable();
            $table->string('sosialisasi_filename',300)->nullable();

            $table->unsignedInteger('pedoman_teknis_id')->default(1);
            $table->text('pedoman_file')->nullable();
            $table->string('pedoman_filename',300)->nullable();

            $table->unsignedInteger('kemudahan_layanan_id')->default(1);
            $table->text('layanan_file')->nullable();
            $table->string('layanan_filename',300)->nullable();

            $table->unsignedInteger('kemudahan_proses_id')->default(1);
            $table->text('proses_file')->nullable();
            $table->string('proses_filename',300)->nullable();

            $table->unsignedInteger('layanan_pengaduan_id')->default(1);
            $table->text('pengaduan_file')->nullable();
            $table->string('pengaduan_filename',300)->nullable();

            $table->unsignedInteger('online_sistem_id')->default(1);
            $table->text('online_file')->nullable();
            $table->string('online_filename',300)->nullable();

            $table->unsignedInteger('replikasi_id')->default(1);
            $table->text('replikasi_file')->nullable();
            $table->string('replikasi_filename',300)->nullable();

            $table->unsignedInteger('kecepatan_inovasi_id')->default(1);
            $table->text('kecepatan_file')->nullable();
            $table->string('kecepatan_filename',300)->nullable();

            $table->unsignedInteger('kemanfaatan_inovasi_id')->default(1);
            $table->text('kemanfaatan_file')->nullable();
            $table->string('kemanfaatan_filename',300)->nullable();

            $table->unsignedInteger('monitoring_id')->default(1);
            $table->text('monitoring_file')->nullable();
            $table->string('monitoring_filename',300)->nullable();

            $table->timestamps();
        });

        Schema::table('kuisioners', function (Blueprint $table) {
            // $table->foreign('innovator_id')->references('id')->on('innovators')
            //         ->onDelete('cascade')
            //         ->onUpdate('cascade');
            //set foreign key for component 2
            $table->foreign('regulasi_inovasi_id')->references('id')->on('regulasi_inovasis')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('ketersediaan_sdm_id')->references('id')->on('ketersediaan_sdms')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('dukungan_anggaran_id')->references('id')->on('dukungan_anggarans')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('penggunaan_it_id')->references('id')->on('penggunaan_its')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('bimtek_id')->references('id')->on('bimteks')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('program_renstra_id')->references('id')->on('program_renstras')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('jejaring_inovasi_id')->references('id')->on('jejaring_inovasis')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('replikasi_id')->references('id')->on('replikasis')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            //set foreign key for component 3
            $table->foreign('pedoman_teknis_id')->references('id')->on('pedoman_teknis')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('layanan_pengaduan_id')->references('id')->on('layanan_pengaduans')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('kemudahan_layanan_id')->references('id')->on('kemudahan_layanans')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('kemudahan_proses_id')->references('id')->on('kemudahan_proses')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('online_sistem_id')->references('id')->on('online_sistems')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            //set foreign key for component 4
            $table->foreign('kecepatan_inovasi_id')->references('id')->on('kecepatan_inovasis')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('kemanfaatan_inovasi_id')->references('id')->on('kemanfaatan_inovasis')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('sosialisasi_id')->references('id')->on('sosialisasi_inovasis')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('keterlibatan_aktor_id')->references('id')->on('keterlibatan_aktors')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('pelaksana_id')->references('id')->on('pelaksana_inovasis')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('monitoring_id')->references('id')->on('monitoring_inovasis')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kuisioners');
    }
}
