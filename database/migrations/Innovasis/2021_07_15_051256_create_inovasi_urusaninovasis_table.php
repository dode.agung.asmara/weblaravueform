<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInovasiUrusaninovasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inovasi_urusaninovasis', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('innovation_id');
            $table->unsignedInteger('urusaninovasi_id');
            $table->timestamps();
        });

        Schema::table('inovasi_urusaninovasis', function (Blueprint $table) {
            $table->foreign('innovation_id')->references('id')->on('innovations')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('urusaninovasi_id')->references('id')->on('urusan_inovasis')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inovasi_urusaninovasis');
    }
}
