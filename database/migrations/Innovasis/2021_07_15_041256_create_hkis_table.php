<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHkisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hkis', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('innovation_id');
            $table->string('nomor',300)->nullable();
            $table->date('tanggal')->nullable();
            $table->string('nama_pencipta',300)->nullable();
            $table->string('pemegang_hakcipta',300)->nullable();
            $table->string('jenis_ciptaan',300)->nullable();
            $table->string('judul_ciptaan',300)->nullable();
            $table->string('nomor_pencatatan',300)->nullable();
            $table->timestamps();
        });

        Schema::table('hkis', function (Blueprint $table) {
            $table->foreign('innovation_id')->references('id')->on('innovations')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hkis');
    }
}
