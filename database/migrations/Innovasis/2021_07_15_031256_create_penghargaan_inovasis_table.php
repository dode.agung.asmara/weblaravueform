<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenghargaanInovasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penghargaan_inovasis', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('innovation_id');
            $table->string('nama_penghargaan',300)->nullable();
            $table->string('pemberi_penghargaan',300)->nullable();
            $table->string('tahun_penghargaan',10)->nullable();
            $table->timestamps();
        });

        Schema::table('penghargaan_inovasis', function (Blueprint $table) {
            $table->foreign('innovation_id')->references('id')->on('innovations')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penghargaan_inovasis');
    }
}
