<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengalamanInovasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengalaman_inovasis', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('innovation_id');
            $table->string('nama_pengalaman',300)->nullable();
            $table->string('penyelenggara',300)->nullable();
            $table->string('tahun_pengalaman',10)->nullable();
            $table->string('predikat',300)->nullable();
            $table->timestamps();
        });

        Schema::table('pengalaman_inovasis', function (Blueprint $table) {
            $table->foreign('innovation_id')->references('id')->on('innovations')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengalaman_inovasis');
    }
}
