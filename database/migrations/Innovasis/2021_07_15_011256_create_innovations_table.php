<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInnovationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('innovations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('innovator_id');
            $table->unsignedInteger('kuisioner_id')->default(1);
            $table->boolean('checkinovasi')->default(0);
            // $table->string('status',200)->nullable();
            $table->enum('status', ['draft','proses','revisi','approve','deleted'])->default('draft');
            $table->string('nama_singkat',100)->nullable();
            $table->string('nama_lengkap',200)->nullable();
            $table->unsignedInteger('tahapan_inovasi_id')->default(2);
            $table->unsignedInteger('inisiator_inovasi_id')->default(1);
            $table->unsignedInteger('sistem_inovasi_id')->default(1);
            $table->unsignedInteger('bentuk_inovasi_id')->default(1);
            $table->unsignedInteger('jenis_inovasi_id')->default(1);
            $table->unsignedInteger('covid_inovasi_id')->default(1);
            $table->unsignedInteger('bidang_inovasi_id')->default(1);
            // $table->unsignedInteger('urusan_inovasi_id')->default(1);
            $table->date('waktu_ujicoba')->nullable();
            $table->date('waktu_implementasi')->nullable();
            $table->text('rancang_bangun')->nullable();
            $table->text('tujuan')->nullable();
            $table->text('manfaat')->nullable();
            $table->text('hasil')->nullable();
            $table->unsignedInteger('anggaran_inovasi_id')->default(1);
            $table->string('ket_anggaran',300)->nullable();
            $table->text('file_anggaran')->nullable();
            $table->unsignedInteger('proses_inovasi_id')->default(2);
            $table->string('ket_proses',300)->nullable();
            $table->text('file_proses')->nullable();
            $table->text('file_sk')->nullable();
            $table->text('logo')->nullable();
            $table->text('photos')->nullable();
            $table->unsignedInteger('video_inovasi_id')->default(1);
            $table->string('ket_video',300)->nullable();
            $table->text('linkvideos')->nullable();

            $table->timestamps();
        });

        Schema::table('innovations', function (Blueprint $table) {
            $table->foreign('innovator_id')->references('id')->on('innovators')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('kuisioner_id')->references('id')->on('kuisioners')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('tahapan_inovasi_id')->references('id')->on('tahapan_inovasi')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('inisiator_inovasi_id')->references('id')->on('inisiator_inovasi')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('sistem_inovasi_id')->references('id')->on('sistem_inovasis')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('bentuk_inovasi_id')->references('id')->on('bentuk_inovasi')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('jenis_inovasi_id')->references('id')->on('jenis_inovasi')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('covid_inovasi_id')->references('id')->on('covid_inovasi')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('bidang_inovasi_id')->references('id')->on('bidang_inovasis')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            // $table->foreign('urusan_inovasi_id')->references('id')->on('urusan_inovasis')
            //         ->onDelete('cascade')
            //         ->onUpdate('cascade');
            $table->foreign('anggaran_inovasi_id')->references('id')->on('anggaran_inovasis')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('proses_inovasi_id')->references('id')->on('proses_inovasis')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('video_inovasi_id')->references('id')->on('video_inovasis')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('innovations');
    }
}
