const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('public')
    .setResourceRoot('../') // Turns assets paths in css relative to css file
    // .options({
    //     processCssUrls: false,
    // })
    
    .js('resources/js/frontend/home/app.js', 'js/home.js')
    .js('resources/js/frontend/homepage/app.js', 'js/homepage.js')
    .js('resources/js/frontend/register/app.js', 'js/register.js')
    .js('resources/js/frontend/register_auditor/app.js', 'js/register_auditor.js')
    .js('resources/js/frontend/inovasi/app.js', 'js/inovasi.js')

    .js('resources/js/backend/innovationall/app.js', 'js/innovationall.js')
    .js('resources/js/backend/innovations/app.js', 'js/innovations.js')
    .js('resources/js/backend/auditors/app.js', 'js/auditors.js')
    .js('resources/js/backend/mapping/app.js', 'js/mapping.js')
    .js('resources/js/backend/user/auditor/app.js', 'js/back-user-auditor.js')
    .js('resources/js/backend/user/inovator/app.js', 'js/back-user-inovator.js')

    .js([
        'resources/js/backend/before.js',
        'resources/js/backend/dashboard/app.js',
        'resources/js/backend/after.js'
    ], 'js/dashboard.js')
    .extract([
        // Extract packages from node_modules to vendor.js
        'jquery',
        'bootstrap',
        'popper.js',
        'axios',
        'sweetalert2',
        'lodash'
    ])
    .sourceMaps();

if (mix.inProduction()) {
    mix.version()
        .options({
            // Optimize JS minification process
            terser: {
                cache: true,
                parallel: true,
                sourceMap: true
            }
        });
} else {
    // Uses inline source-maps on development
    mix.webpackConfig({
        devtool: 'inline-source-map'
    });
}
