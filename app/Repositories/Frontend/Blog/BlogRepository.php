<?php

namespace App\Repositories\Frontend\Blog;


use App\Exceptions\GeneralException;
use App\Models\Blog\BlogentryModel;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class UserRepository.
 */
class BlogRepository extends BaseRepository
{
    /**
     * UserRepository constructor.
     *
     * @param  User  $model
     */
    protected $blogentry;

    public function __construct(BlogentryModel $blogentry)
    {
        $this->blogentry = $blogentry;
    }

    //MEMBUAT FUNGSI UNTUK MENGAMBIL DATA YANG TELAH DI PAGINATE
    //DAN DIFUNGSI INI TELAH DIURUTKAN BERDASARKAN CREATED_AT
    //FUNGSI INI MEMINTA PARAMETER JUMLAH DATA YANG AKAN DITAMPILKAN
    public function getPaginate($per_page)
    {
        return $this->blogentry->orderBy('created_at', 'ASEC')->paginate($per_page);
    }   


    public function showall()
    {
        return $this->blogentry::all();
    }

    //MEMBUAT FUNGSI UNTUK MENGAMBIL DATA BERDASARKAN ID
    public function find($slug)
    {
        return $this->blogentry->where('slug',$slug)->get();
    }

    //MEMBUAT FUNGSI UNTUK MENGAMBIL DATA BERDASRAKAN COLOMN YANG DITERIMA
    public function findBy($column, $data)
    {
        return $this->blogentry->where($column, $data)->get();
    }
}
