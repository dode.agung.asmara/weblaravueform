<?php

namespace App\Repositories\Backend;



use App\Repositories\BaseRepository;
use App\Models\Innovations\Innovations;

/**
 * Class UserRepository.
 */
class InnovationsRepository extends BaseRepository
{
    /**
     * UserRepository constructor.
     *
     * @param  User  $model
     */
    protected $innovations;

    public function __construct(Innovations $innovations)
    {
        $this->innovations = $innovations;
    }


    public function showall()
    {
        return $this->innovations::get();
    }

}
