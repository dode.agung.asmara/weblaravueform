<?php

namespace App\Models\Kuisioners;

use App\Models\Innovations\Innovation;
use Illuminate\Database\Eloquent\Model;

class Kuisioner extends Model
{
    protected $table = 'kuisioners';
    protected $guarded = ['id'];

    public function innovation()
    {
        return $this->hasOne(Innovation::class);
    }
}
