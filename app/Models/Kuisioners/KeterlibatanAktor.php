<?php

namespace App\Models\Kuisioners;

use Illuminate\Database\Eloquent\Model;

class KeterlibatanAktor extends Model
{
    protected $table = 'keterlibatan_aktors';
    protected $guarded = [];
}
