<?php

namespace App\Models\Auth\Traits\Relationship;

use App\Models\Auditor;
use App\Models\Auth\PasswordHistory;
use App\Models\Auth\SocialAccount;
use App\Models\Innovators\Innovator;

/**
 * Class UserRelationship.
 */
trait UserRelationship
{
    /**
     * @return mixed
     */
    public function providers()
    {
        return $this->hasMany(SocialAccount::class);
    }

    /**
     * @return mixed
     */
    public function passwordHistories()
    {
        return $this->hasMany(PasswordHistory::class);
    }

    /**
     * @return mixed
     */
    public function innovator()
    {
        return $this->hasOne(Innovator::class,'user_id');
    }
    /**
     * @return mixed
     */
    public function auditor()
    {
        return $this->hasOne(Auditor::class,'user_id');
    }
}
