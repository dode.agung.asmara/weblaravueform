<?php

namespace App\Models\Innovations;

use Illuminate\Database\Eloquent\Model;

class PengalamanInovasi extends Model
{
    protected $table = 'pengalaman_inovasis';
    protected $guarded = [];

    public function innovation()
    {
        return $this->belongsTo(Innovation::class);
    }
}
