<?php

namespace App\Models\Innovations;

use App\Models\Innovators\Innovator;
use App\Models\Kuisioners\Kuisioner;
use App\Models\VerifikasiDokumen;
use Illuminate\Database\Eloquent\Model;

class Innovation extends Model
{
    protected $table = 'innovations';
    protected $guarded = ['id'];

    public function jenisinovasi()
    {
        return $this->belongsTo(JenisInovasi::class, 'jenis_inovasi_id');
    }

    public function bentukinovasi()
    {
        return $this->belongsTo(BentukInovasi::class, 'bentuk_inovasi_id');
    }

    public function bidanginovasi()
    {
        return $this->belongsTo(BidangInovasi::class, 'bidang_inovasi_id');
    }

    public function sisteminovasi()
    {
        return $this->belongsTo(SistemInovasi::class, 'sistem_inovasi_id');
    }

    public function tahapaninovasi()
    {
        return $this->belongsTo(TahapanInovasi::class, 'tahapan_inovasi_id');
    }

    public function covidinovasi()
    {
        return $this->belongsTo(CovidInovasi::class, 'covid_inovasi_id');
    }

    public function inisiatorinovasi()
    {
        return $this->belongsTo(InisiatorInovasi::class, 'inisiator_inovasi_id');
    }

    public function innovator()
    {
        return $this->belongsTo(Innovator::class, 'innovator_id')->with(['jenis_innovators','user']);
    }

    public function urusaninovasi()
    {
        return $this->belongsToMany(UrusanInovasi::class, 'inovasi_urusaninovasis','innovation_id','urusaninovasi_id');
    }

    public function penghargaans()
    {
        return $this->hasMany(PenghargaanInovasi::class);
    }

    public function pengalamans()
    {
        return $this->hasMany(PengalamanInovasi::class);
    }

    public function hkis()
    {
        return $this->hasOne(Hki::class);
    }

    public function kuisioner()
    {
        return $this->belongsTo(Kuisioner::class);
    }

    public function varifikasidokumen()
    {
        return $this->hasOne(VerifikasiDokumen::class);
    }

    public function scopeGetJenisInovasi($query,$value)
    {
        if (isset($value) && $value!=='null') {
            return $query->where('jenis_inovasi_id',$value)->where('status', 'approve');
        }
        return ;
    }

    public function scopeGetBidangInovasi($query,$value)
    {
        if (isset($value) && $value!=='null') {
            return $query->where('bidang_inovasi_id',$value)->where('status', 'approve');
        }
        return ;
    }

    public function scopeGetBentukInovasi($query,$value)
    {
        if (isset($value) && $value!=='null') {
            return $query->where('bentuk_inovasi_id',$value)->where('status', 'approve');
        }
        return ;
    }

    public function scopeGetSistemInovasi($query,$value)
    {
        if (isset($value) && $value!=='null') {
            return $query->where('sistem_inovasi_id',$value)->where('status', 'approve');
        }
        return ;
    }

    public function scopeGetTahapanInovasi($query,$value)
    {
        if (isset($value) && $value!=='null') {
            return $query->where('tahapan_inovasi_id',$value)->where('status', 'approve');
        }
        return ;
    }

    public function scopeGetCovidInovasi($query,$value)
    {
        if (isset($value) && $value!=='null') {
            return $query->where('covid_inovasi_id',$value)->where('status', 'approve');
        }
        return ;
    }

    public function scopeGetInisiatorInovasi($query,$value)
    {
        if (isset($value) && $value!=='null') {
            return $query->where('inisiator_inovasi_id',$value)->where('status', 'approve');
        }
        return ;
    }

}
