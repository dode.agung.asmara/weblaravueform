<?php

namespace App\Models\Innovations;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\JsonResponse;

class TahapanInovasi extends Model
{
    protected $table = 'tahapan_inovasi';
    protected $guarded = [];
    // protected $fillable = [
    //             'id',
    //             'name',
    // ];
}
