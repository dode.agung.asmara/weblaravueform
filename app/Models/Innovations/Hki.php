<?php

namespace App\Models\Innovations;

use Illuminate\Database\Eloquent\Model;

class Hki extends Model
{
     protected $table = 'hkis';
    protected $guarded = [];

    public function innovation()
    {
        return $this->belongsTo(Innovation::class);
    }
}
