<?php

namespace App\Models\Innovations;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\JsonResponse;

class JenisInovasi extends Model
{
    protected $table = 'jenis_inovasi';
    protected $guarded = [];
    // protected $fillable = [
    //             'id',
    //             'name',
    // ];
}
