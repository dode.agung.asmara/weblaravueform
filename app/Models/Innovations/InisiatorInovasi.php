<?php

namespace App\Models\Innovations;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\JsonResponse;

class InisiatorInovasi extends Model
{
    protected $table = 'inisiator_inovasi';
    protected $guarded = [];
    // protected $fillable = [
    //             'id',
    //             'name',
    // ];
}
