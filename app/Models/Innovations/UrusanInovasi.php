<?php

namespace App\Models\Innovations;

use Illuminate\Database\Eloquent\Model;

class UrusanInovasi extends Model
{
    public function innovation()
    {
        return $this->belongsToMany(Innovation::class, 'inovasi_urusaninovasis','urusaninovasi_id','innovation_id');
    }
}
