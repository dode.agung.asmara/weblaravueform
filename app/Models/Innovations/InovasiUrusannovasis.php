<?php

namespace App\Models\Innovations;

use Illuminate\Database\Eloquent\Model;

class InovasiUrusanInovasi extends Model
{
    protected $table = 'inovasi_urusaninovasis';
    protected $guarded = ['id'];

}
