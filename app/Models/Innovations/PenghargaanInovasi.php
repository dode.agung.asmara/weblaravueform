<?php

namespace App\Models\Innovations;

use Illuminate\Database\Eloquent\Model;

class PenghargaanInovasi extends Model
{
    protected $table = 'penghargaan_inovasis';
    protected $guarded = [];

    /**
     * Get the post that owns the inovation.
     */
    public function innovation()
    {
        return $this->belongsToMany(Innovation::class);
    }
}
