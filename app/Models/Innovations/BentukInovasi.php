<?php

namespace App\Models\Innovations;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\JsonResponse;

class BentukInovasi extends Model
{
    protected $table = 'bentuk_inovasi';
    protected $guarded = [];
    // protected $fillable = [
    //             'id',
    //             'name',
    // ];
}
