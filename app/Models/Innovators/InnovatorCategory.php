<?php

namespace App\Models\Innovators;

use Illuminate\Database\Eloquent\Model;

class InnovatorCategory extends Model
{
    protected $table = 'innovator_categorys';

    public function innovators()
    {
        return $this->hasMany(Innovator::class);
    }

}
