<?php

namespace App\Models\Innovators;

use App\Models\Auth\User;
use App\Models\Innovations\Innovation;
use Illuminate\Database\Eloquent\Model;

class Innovator extends Model
{
      // $name = DB::table('working_permit')->where('name', 'John')->pluck('name');
      protected $table = 'innovators';
      protected $guarded = [];

    //   public function innovator()
    //   {
    //       return $this->belongsTo(User::class, 'user_id');
    //   }
    //   
      public function user()
      {
        return $this->belongsTo(User::class, 'user_id');
      }

      public function opd()
      {
          return $this->belongsTo(InnovatorCategories::class, 'category_id')->where('id',1);
      }

      public function nonopd()
      {
          return $this->belongsTo(InnovatorCategories::class, 'category_id')->where('id',2);
      }
      /**
     * @return mixed
     */
    public function innovation()
    {
        return $this->hasMany(Innovation::class,'innovator_id');
    }

    public function inovator_categorys()
    {
        return $this->belongsTo(InnovatorCategory::class, 'category_id');
    }

     public function jenis_innovators()
    {
        return $this->belongsTo(JenisInnovator::class, 'jenis_id');
    }

    public function scopeGetKategoriInovator($query,$value)
    {
        if (isset($value) && $value!=='null') {
            return $query->where('category_id',$value);
        }
        return ;
    }

    public function scopeGetJenisInovator($query,$value)
    {
        // dd()
        if (isset($value) && $value!=='null') {
            return $query->where('jenis_id',$value);
        }
        return ;
    }

    public function scopeWhereHasInovasi($query,$data)
    {

            return $query->whereHas('innovation', function ($query) use ($data) {
                                  if((isset($data['jenisInovasi']) && $data['jenisInovasi']!=='null')){
                                    $query->getJenisInovasi($data['jenisInovasi']);
                                  }

                                  if((isset($data['bentukInovasi']) && $data['bentukInovasi']!=='null')){
                                    $query->getBentukInovasi($data['bentukInovasi']);
                                  }

                                  if((isset($data['bidangInovasi']) && $data['bidangInovasi']!=='null')){
                                    $query->getBidangInovasi($data['bidangInovasi']);
                                  }

                                  if((isset($data['sistemInovasi']) && $data['sistemInovasi']!=='null')){
                                    $query->getSistemInovasi($data['sistemInovasi']);
                                  }

                                  if((isset($data['tahapanInovasi']) && $data['tahapanInovasi']!=='null')){
                                    $query->getTahapanInovasi($data['tahapanInovasi']);
                                  }
                                  
                                  if((isset($data['covidInovasi']) && $data['covidInovasi']!=='null')){
                                    $query->getCovidInovasi($data['covidInovasi']);
                                  }
                                  if((isset($data['inisiatorInovasi']) && $data['inisiatorInovasi']!=='null')){
                                    $query->getInisiatorInovasi($data['inisiatorInovasi']);
                                  }
                                    $query->where('status', 'approve');
                                });
        
        // return ;
    }    


}
