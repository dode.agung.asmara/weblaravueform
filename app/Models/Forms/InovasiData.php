<?php

namespace App\Models\Forms;

use App\Models\Forms\Bimtek;
use App\Models\Forms\Replikasi;
use App\Models\Forms\IgaSinovik;
use App\Models\Forms\InfoLayanan;
use App\Models\Forms\Sosialisasi;
use App\Models\Forms\Stakeholder;
use App\Models\Forms\CovidInovasi;
use App\Models\Forms\JenisInovasi;
use App\Models\Forms\OnlineSistem;
use App\Models\Forms\PenggunaanIt;
use App\Models\Forms\BentukInovasi;
use App\Models\Forms\PedomanTeknis;
use App\Models\Forms\ProgramRenstra;
use App\Models\Forms\TahapanInovasi;
use App\Models\Forms\JejaringInovasi;
use App\Models\Forms\KemudahanProses;
use App\Models\Forms\KepuasanInovasi;
use App\Models\Forms\KetersediaanSdm;
use App\Models\Forms\KualitasInovasi;
use App\Models\Forms\RegulasiInovasi;
use App\Models\Forms\DukunganAnggaran;
use App\Models\Forms\InisiatorInovasi;
use App\Models\Forms\KecepatanInovasi;
use App\Models\Forms\KemudahanLayanan;
use App\Models\Forms\LayananPengaduan;
use Illuminate\Database\Eloquent\Model;
use App\Models\Forms\KemanfaatanInovasi;
use App\Models\Forms\PengelolaanInovasi;

class InovasiData extends Model
{
    protected $table = 'inovasi_data';
    protected $guarded = ['id'];


    //Reletions Table Function for Component 1
    public function bentukinovasi()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(BentukInovasi::class,'bentuk_inovasi_id');
    }
    public function covidinovasi()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(CovidInovasi::class,'covid_inovasi_id');
    }
    public function inisiatorinovasi()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(InisiatorInovasi::class,'inisiator_inovasi_id');
    }
    public function jenisinovasi()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(JenisInovasi::class,'jenis_inovasi_id');
    }
    public function tahapaninovasi()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(TahapanInovasi::class,'tahapan_inovasi_id');
    }

    //Reletions Table Function for Component 2
    public function RegulasiInovasi()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(RegulasiInovasi::class,'regulasi_inovasi_id');
    }
    public function ketersediaansdm()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(KetersediaanSdm::class,'ketersediaan_sdm_id');
    }
    public function dukungananggaran()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(DukunganAnggaran::class,'dukungan_anggaran_id');
    }
    public function penggunaanit()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(PenggunaanIt::class,'penggunaan_it_id');
    }
    public function bimtek()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(Bimtek::class,'bimtek_id');
    }
    public function programrenstra()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(ProgramRenstra::class,'program_renstra_id');
    }
    public function jejaringinovasi()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(JejaringInovasi::class,'jejaring_inovasi_id');
    }
    public function replikasi()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(Replikasi::class,'replikasi_id');
    }

    //Reletions Table Function for Component 3
    public function pedomanteknis()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(PedomanTeknis::class,'pedoman_teknis_id');
    }
    public function pengelolaaninovasi()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(PengelolaanInovasi::class,'pengelolaan_inovasi_id');
    }
    public function infolayanan()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(InfoLayanan::class,'info_layanan_id');
    }
    public function layananpengaduan()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(LayananPengaduan::class,'layanan_pengaduan_id');
    }
    public function Stakeholder()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(Stakeholder::class,'stakeholder_id');
    }
    public function kemudahanlayanan()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(KemudahanLayanan::class,'kemudahan_layanan_id');
    }
    public function kemudahanproses()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(KemudahanProses::class,'kemudahan_proses_id');
    }
    public function OnlineSistem()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(OnlineSistem::class,'online_sistem_id');
    }

    //Reletions Table Function for Component 3
    public function kecepataninovasi()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(KecepatanInovasi::class,'kecepatan_inovasi_id');
    }
    public function kemanfaataninovasi()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(KemanfaatanInovasi::class,'kemanfaatan_inovasi_id');
    }
    public function kepuasaninovasi()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(KepuasanInovasi::class,'kepuasan_inovasi_id');
    }
    public function sosialisasi()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(Sosialisasi::class,'sosialisasi_id');
    }
    public function kualitasinovasi()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(KualitasInovasi::class,'kualitas_inovasi_id');
    }
    public function igasinovik()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(IgaSinovik::class,'iga_sinovik_id');
    }

}
