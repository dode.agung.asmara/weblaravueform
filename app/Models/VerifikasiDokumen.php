<?php

namespace App\Models;

use App\Models\Innovations\Innovation;
use Illuminate\Database\Eloquent\Model;

class VerifikasiDokumen extends Model
{
    protected $table = 'verifikasi_dokumens';
    protected $guarded = [];

    /**
     * Get the post that owns the inovation.
     */
    public function innovation()
    {
        return $this->belongsTo(Innovation::class);
    }
    public function auditor()
    {
        return $this->belongsTo(Auditor::class);
    }
}
