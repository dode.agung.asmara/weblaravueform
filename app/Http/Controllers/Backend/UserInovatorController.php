<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Auth\User;
use App\Models\Auth\Role;

class UserInovatorController extends Controller
{

    protected $model;
    protected $role;

    public function __construct(User $user, Role $role)
    {
        $this->model = $user;
        $this->role = $role;
    }

    public function index()
    {
        return view('backend.user.inovator');
    }

    public function list(Request $request)
    {
        return $this->model::with('roles')
                            ->whereHas('roles', function($q){
                                $q->where('name', 'innovator');
                            })
                            ->orderBy('confirmed')
                            ->get();
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $user = $this->model::find($id);

        $user->syncRoles($data['role']);
    }

    public function setstatus($id, $status){

        $this->model::where('id', $id)->update([
            'confirmed' => $status,
        ]);
    }

    public function role(Request $request)
    {
        return $this->role::where('name', '<>', 'administrator')->get();
    }

}
