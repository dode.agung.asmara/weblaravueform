<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Innovations\Innovation;
use App\Models\Innovators\Innovator;

class InnovatorsController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */

    public function getDataInovasi($id)
    {
        $data = Innovation::with(['kuisioner','urusaninovasi'])->where('id', $id)->first();
        return $data;
    }
    public function inovasi()
    {
        if (auth()->check()) {
            if (auth()->user()->hasRole(['innovator'])) {
                $innovator = auth()->user()->innovator()->first();
            }
        }

        //Data Kuisioner
        $innovation = Innovation::with('innovator')->where('innovator_id', $innovator->id)->get();
        // $innovation = '';
        $namainnovator = $innovator->nama;
        $kuisioner = '';

        // dd($innovatorid->category_id);
        return view('backend.innovators.inovasi')
        ->with('innovation', $innovation)
        ->with('namainnovator', $namainnovator)
        ->with('kuisioner', $kuisioner);
    }
    public function inovasiall()
    {
        if (auth()->check()) {
            if (auth()->user()->hasRole(['innovator'])) {
                $innovator = auth()->user()->innovator()->first();
            }
        }

        //Data Kuisioner
        $innovation = Innovation::with('innovator')->where('status','like','%'.'Approve'.'%')->get();
        // $innovation = '';
        $namainnovator = $innovator->nama;
        $kuisioner = '';

        // dd($innovatorid->category_id);
        return view('backend.innovators.allinovasi')
        ->with('innovation', $innovation)
        ->with('namainnovator', $namainnovator)
        ->with('kuisioner', $kuisioner);
    }
}
