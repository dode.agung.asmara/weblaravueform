<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\VerifikasiDokumen;
use App\Models\Innovations\Innovation;
use App\Models\Innovators\Innovator;

class AuditorController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */

    public function inovasi()
    {
        return view('backend.auditors.inovasi');
    }

    public function getCommentid($id)
    {
        if (auth()->check()) {

            if (auth()->user()->hasRole(['auditor'])) {
                $auditor = auth()->user()->auditor()->first();
            }
        }
        $auditorid = $auditor->id;
        $data = VerifikasiDokumen::where([['innovation_id', $id],['auditor_id', $auditorid]])->first();
        return $data;
    }
    public function getInovasi()
    {
        if (auth()->check()) {
            if (auth()->user()->hasRole(['administrator'])) {
                //  return 'admin.dashboard';
            }
            if (auth()->user()->hasRole(['innovator'])) {
                $innovator = auth()->user()->innovator()->first();
            }
            if (auth()->user()->hasRole(['auditor'])) {
                // return 'auditor.dashboard';
            }
        }
        $innovatorid = $innovator->id;
        $data = Innovation::whereHas('innovator', function($q) use($innovatorid) {
            $q->where([['innovator_id', $innovatorid],['kuisioner_id', 1]]);
        })->with(['kuisioner','urusaninovasi','penghargaans','pengalamans','hkis'])->first();

        // $data = Innovation::with('innovator')->where([['innovator_id', $innovatorid->id],['kuisioner_id', 1]])->first();
        // dd($innovation1);
        return $data;
    }
    //function for insert data innovation in the beginning
    public function InsertComments(Request $request)
    {
        if (auth()->check()) {

            if (auth()->user()->hasRole(['auditor'])) {
                $auditor = auth()->user()->auditor()->first();
            }
        }

        $data = $request->all();
        $auditorid = $auditor->id;
        $datacomments = [
            'auditor_id' => $auditorid,
            'innovation_id' => $data['innovation_id'],
            'data' => $data['data'],
        ];
        $datacoment = VerifikasiDokumen::create($datacomments);
        return $datacoment->id;
    }
    public function UpdateComments(Request $request, $id)
    {
        if (auth()->check()) {

            if (auth()->user()->hasRole(['auditor'])) {
                $auditor = auth()->user()->auditor()->first();
            }
        }
        $auditorid = $auditor->id;
        $data = $request->all();
        $datacomments = [
            'auditor_id' => $auditorid,
            'innovation_id' => $data['innovation_id'],
            'data' => $data['data'],
        ];
        $datacoment = VerifikasiDokumen::findorFail($id);
        $datacoment->update($datacomments);
        return $datacoment->id;
    }
    public function UpdateInnovation(Request $request,$id)
    {
        $data = $request->all();
        
        $dataStatus = [
            'status' =>  $data['status'],
        ];

        if($data['status'] == 'approve'){
            $dataStatus['proses_inovasi_id'] = 3;
        }

        $innovation = Innovation::findorFail($id);
        $innovation->update($dataStatus);

        return $innovation->id;
    }
    //
    public function getData()
    {
        $data = Innovation::whereHas('kuisioner', function($q) {
            $q->where([['status', '!=', 'draft'],['status', '!=', 'deleted']]);
        })->with(['innovator','urusaninovasi','penghargaans','pengalamans','hkis','kuisioner','varifikasidokumen'])->get();
        return $data;
    }
    public function getComment($id)
    {
        $data = VerifikasiDokumen::where([['innovation_id', $id],['auditor_id','>',1]])->first();
        return $data;
    }


    public function commentAuditor(Request $request,$id)
    {
        $data = $request->all();
        $dataComment = [
            'data' => $data['data'],
        ];

        $innovation = Innovation::findorFail($id);
        $innovation->varifikasidokumen()->updateOrCreate($dataComment);
    }
    public function updateComment(Request $request,$id)
    {
        $data = $request->all();
        $dataComment = [
            'data' => $data['data'],
        ];
        $dataStatus = [
            'status' =>  $data['status'],
        ];

        $innovation = Innovation::findorFail($id);
        $innovation->update($dataStatus);
        // dd($id);
        $Verifikasidokumen = VerifikasiDokumen::where('innovation_id',$id)->first();
        $Verifikasidokumen->update($dataComment);
        return $Verifikasidokumen->id;
    }
    public function updateStatus(Request $request,$id)
    {
        // $data = $request->all();
        $dataStatus = [
            'status' =>  'proses',
        ];

        $innovation = Innovation::findorFail($id);
        $innovation->update($dataStatus);
        return $innovation->id;
    }


}
