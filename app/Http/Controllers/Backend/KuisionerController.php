<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Kuisioners\Bimtek;
use App\Http\Controllers\Controller;
use App\Models\Kuisioners\Kuisioner;
use App\Models\Kuisioners\Replikasi;
use App\Models\Innovations\Innovation;
use App\Models\Kuisioners\InfoLayanan;
use App\Models\Kuisioners\Sosialisasi;
use App\Models\Kuisioners\Stakeholder;
use App\Models\Kuisioners\OnlineSistem;
use App\Models\Kuisioners\PenggunaanIt;
use App\Models\Kuisioners\PedomanTeknis;
use App\Models\Kuisioners\RencanaNonopd;
use App\Models\Kuisioners\JejaringNonopd;
use App\Models\Kuisioners\ProgramRenstra;
use App\Models\Kuisioners\RegulasiNonopd;
use App\Models\Kuisioners\JejaringInovasi;
use App\Models\Kuisioners\KemudahanProses;
use App\Models\Kuisioners\KepuasanInovasi;
use App\Models\Kuisioners\KetersediaanSdm;
use App\Models\Kuisioners\PengelolaNonopd;
use App\Models\Kuisioners\RegulasiInovasi;
use App\Models\Kuisioners\DukunganAnggaran;
use App\Models\Kuisioners\KecepatanInovasi;
use App\Models\Kuisioners\KemudahanLayanan;
use App\Models\Kuisioners\LayananPengaduan;
use App\Models\Kuisioners\KemanfaatanInovasi;
use App\Models\Kuisioners\KeterlibatanAktor;
use App\Models\Kuisioners\MonitoringInovasi;
use App\Models\Kuisioners\PelaksanaInovasi;
use App\Models\Kuisioners\PengelolaanInovasi;
use App\Models\Kuisioners\SosialisasiInovasi;

class KuisionerController extends Controller
{
    //function for post data from form Form1Kuisioners
    public function UpdateKuisioners(Request $request,$id)
    {
        $data = $request->all();
        $kuisioner = Kuisioner::findorFail($id);
        $kuisioner->update($data);
        return $kuisioner->id;
    }

    public function Form1Kuisioners(Request $request)
    {
        $data = $request->all();
        $kuisioner = Kuisioner::UpdateOrCreate($data);
        return $kuisioner->id;
    }
    public function Form2Kuisioners(Request $request,$id)
    {
        $data = $request->all();
        $kuisioner = Kuisioner::findorFail($id);
        $kuisioner->update($data);
        return $kuisioner->id;
    }
    public function Form3Kuisioners(Request $request,$id)
    {
        if (auth()->check()) {
            if (auth()->user()->hasRole(['administrator'])) {
                //  return 'admin.dashboard';
            }
            if (auth()->user()->hasRole(['innovator'])) {
                $innovatorid = auth()->user()->innovator()->first();
            }
            if (auth()->user()->hasRole(['auditor'])) {
                // return 'auditor.dashboard';
            }
        }
        $data = $request->all();
        $datakuisioner = [
            'kecepatan_inovasi_id' => $data['kecepatan_inovasi_id'],
            'kecepatan_file' => $data['kecepatan_file'],
            'kecepatan_filename' => $data['kecepatan_filename'],
            'kemanfaatan_inovasi_id' => $data['kemanfaatan_inovasi_id'],
            'kemanfaatan_file' => $data['kemanfaatan_file'],
            'kemanfaatan_filename' => $data['kemanfaatan_filename'],
            'kepuasan_inovasi_id' => $data['kepuasan_inovasi_id'],
            'kepuasan_file' => $data['kepuasan_file'],
            'kepuasan_filename' => $data['kepuasan_filename'],
            'sosialisasi_id' => $data['sosialisasi_id'],
            'sosialisasi_file' => $data['sosialisasi_file'],
            'sosialisasi_filename' => $data['sosialisasi_filename'],
        ];
        $kuisioner = Kuisioner::findorFail($id);
        $kuisioner->update($datakuisioner);
        //update in innovation table with kuisioner id
        $innovatinid = $data['innovation_id'];
        $innovation = Innovation::with('innovator')->where([['innovator_id', $innovatorid->id],['id', $innovatinid]])->first();
        $innovation->kuisioner()->associate($kuisioner)->save();

        return $kuisioner->id;
    }

    //update kuisioner
    public function Updateform1Kuisioners(Request $request,$id)
    {
        $data = $request->all();
        $kuisioner = Kuisioner::findorFail($id);
        $kuisioner->update($data);
        return $kuisioner->id;
    }

    public function Updateform2Kuisioners(Request $request,$id)
    {
        $data = $request->all();
        $kuisioner = Kuisioner::findorFail($id);
        $kuisioner->update($data);
        return $kuisioner->id;
    }

    public function Updateform3Kuisioners(Request $request,$id)
    {
        if (auth()->check()) {
            if (auth()->user()->hasRole(['administrator'])) {
                //  return 'admin.dashboard';
            }
            if (auth()->user()->hasRole(['innovator'])) {
                $innovatorid = auth()->user()->innovator()->first();
            }
            if (auth()->user()->hasRole(['auditor'])) {
                // return 'auditor.dashboard';
            }
        }
        $data = $request->all();
        $datakuisioner = [
            'kecepatan_inovasi_id' => $data['kecepatan_inovasi_id'],
            'kecepatan_file' => $data['kecepatan_file'],
            'kecepatan_filename' => $data['kecepatan_filename'],
            'kemanfaatan_inovasi_id' => $data['kemanfaatan_inovasi_id'],
            'kemanfaatan_file' => $data['kemanfaatan_file'],
            'kemanfaatan_filename' => $data['kemanfaatan_filename'],
            'kepuasan_inovasi_id' => $data['kepuasan_inovasi_id'],
            'kepuasan_file' => $data['kepuasan_file'],
            'kepuasan_filename' => $data['kepuasan_filename'],
            'sosialisasi_id' => $data['sosialisasi_id'],
            'sosialisasi_file' => $data['sosialisasi_file'],
            'sosialisasi_filename' => $data['sosialisasi_filename'],
        ];
        $kuisioner = Kuisioner::findorFail($id);
        $kuisioner->update($datakuisioner);
        //update in innovation table with kuisioner id
        $innovatinid = $data['innovation_id'];
        $innovation = Innovation::with('innovator')->where([['innovator_id', $innovatorid->id],['id', $innovatinid]])->first();
        $innovation->kuisioner()->associate($kuisioner)->save();

        return $kuisioner->id;
    }

    //funtion for get all data to component 2 (vue js)
    public function getRegulasiInovasi()
    {
        $data = RegulasiInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getKetersediaanSdm()
    {
        $data = KetersediaanSdm::where('id', '>', 1)->get();
        return $data;
    }
    public function getDukunganAnggaran()
    {
        $data = DukunganAnggaran::where('id', '>', 1)->get();
        return $data;
    }
    public function getPenggunaanIt()
    {
        $data = PenggunaanIt::where('id', '>', 1)->get();
        return $data;
    }
    public function getBimtek()
    {
        $data = Bimtek::where('id', '>', 1)->get();
        return $data;
    }
    public function getProgramRenstra()
    {
        $data = ProgramRenstra::where('id', '>', 1)->get();
        return $data;
    }
    public function getJejaringInovasi()
    {
        $data = JejaringInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getReplikasi()
    {
        $data = Replikasi::where('id', '>', 1)->get();
        return $data;
    }

    //funtion for get all data to component 3 (vue js)
    public function getPedomanTeknis()
    {
        $data = PedomanTeknis::where('id', '>', 1)->get();
        return $data;
    }
    public function getLayananPengaduan()
    {
        $data = LayananPengaduan::where('id', '>', 1)->get();
        return $data;
    }
    public function getKemudahanLayanan()
    {
        $data = KemudahanLayanan::where('id', '>', 1)->get();
        return $data;
    }
    public function getKemudahanProses()
    {
        $data = KemudahanProses::where('id', '>', 1)->get();
        return $data;
    }
    public function getOnlineSistem()
    {
        $data = OnlineSistem::where('id', '>', 1)->get();
        return $data;
    }

    //funtion for get all data to component 4 (vue js)
    public function getKecepatanInovasi()
    {
        $data = KecepatanInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getKemanfaatanInovasi()
    {
        $data = KemanfaatanInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getSosialisasiInovasi()
    {
        $data = SosialisasiInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getKeterlibatanAktor()
    {
        $data = KeterlibatanAktor::where('id', '>', 1)->get();
        return $data;
    }
    public function getPelaksanaInovasi()
    {
        $data = PelaksanaInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getMonitoringInovasi()
    {
        $data = MonitoringInovasi::where('id', '>', 1)->get();
        return $data;
    }

    //upload file
    //function uploading file component 1
    public function uploadRegulasi(Request $request){
        $file = $request->file('regulasi_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];

        return $data;
    }
    public function uploadSdm(Request $request){
        $file = $request->file('sdm_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
    public function uploadDukunganAnggaran(Request $request){
        $file = $request->file('dukungananggaran_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
    public function uploadIt(Request $request){
        $file = $request->file('it_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
    public function uploadBimtek(Request $request){
        $file = $request->file('bimtek_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
    public function uploadRenstra(Request $request){
        $file = $request->file('renstra_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
    public function uploadJejaring(Request $request){
        $file = $request->file('jejaring_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
    public function uploadReplikasi(Request $request){
        $file = $request->file('replikasi_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
    public function uploadRencana(Request $request){
        $file = $request->file('rencana_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }

    //function uploading file component 2
    public function uploadPedoman(Request $request){
        $file = $request->file('pedoman_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
    public function uploadPengelolaan(Request $request){
        $file = $request->file('pengelolaan_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
    public function uploadInfo(Request $request){
        $file = $request->file('info_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
    public function uploadPengaduan(Request $request){
        $file = $request->file('pengaduan_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
    public function uploadStakeholder(Request $request){
        $file = $request->file('stakeholder_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
    public function uploadLayanan(Request $request){
        $file = $request->file('layanan_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
    public function uploadProses(Request $request){
        $file = $request->file('proses_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
    public function uploadOnline(Request $request){
        $file = $request->file('online_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }

    //function uploading file component 3
    public function uploadKecepatan(Request $request){
        $file = $request->file('kecepatan_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
    public function uploadKemanfaatan(Request $request){
        $file = $request->file('kemanfaatan_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
    public function uploadKepuasan(Request $request){
        $file = $request->file('kepuasan_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
    public function uploadSosialisasi(Request $request){
        $file = $request->file('sosialisasi_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
    //penambahan
    public function uploadKeterlibatan(Request $request){
        $file = $request->file('keterlibatan_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
    public function uploadPelaksana(Request $request){
        $file = $request->file('pelaksana_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
    public function uploadMonitoring(Request $request){
        $file = $request->file('monitoring_file');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
}
