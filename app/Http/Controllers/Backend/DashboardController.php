<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Innovators\Innovator;
use App\Models\Innovations\Innovation;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
    	$inovator = Innovator::get();
        $inovasi = Innovation::where('status', 'approve')->get();

        return view('backend.dashboard', compact('inovator','inovasi'));
    }

    public function mapping()
    {
        return view('backend.map');
    }

    public function listmap()
    {
        $inovator = Innovator::whereHas('innovation', function($q){
            $q->where('status', 'approve');
        })->with(array('innovation' => function($q){
            $q->where('status', 'approve');
        }))
        ->get();
        return response()->json($inovator);
    }

    public function inovasi()
    {
        return view('backend.auditors.inovasi');
    }

    public function getData()
    {
        $checkId ='false';
        $data = Innovation::whereHas('kuisioner', function($q) use($checkId) {
            $q->where('status', '!=', $checkId);
        })->with(['innovator','urusaninovasi','penghargaans','pengalamans','hkis','kuisioner','varifikasidokumen'])->get();
        return $data;
    }

    public function mapfilter(Request $request)
    {
        $data = $request->all();
        $inovator = Innovator::with(array('innovation' => function($q){
            $q->where('status', 'approve');
        }))
        ->whereHasInovasi($data)
        ->getKategoriInovator($data['kategoriInovator'])
        ->getJenisInovator($data['jenisInovator'])
        ->get();
        return $inovator;
    }

    public function previewInovasi($id)
    {
        $data = Innovation::where('id', $id)->with([
            'urusaninovasi','penghargaans','pengalamans','hkis','jenisinovasi',
            'bidanginovasi','bentukinovasi','sisteminovasi','tahapaninovasi',
            'covidinovasi','inisiatorinovasi','innovator'
        ])->first();

        return $data;
    }

}
