<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//PANGGIL UserRepository YANG TELAH DIBUAT
use App\Repositories\Backend\WorkingpermitRepository;

class WorkingpermitController extends Controller
{
    protected $workingpermit;

    public function __construct(WorkingpermitRepository $workingpermit)
    {
        //Instance repository UserRepository kedalam property $user
        $this->workingpermit = $workingpermit;
    }

    public function index()
    {
        //KARENA UserRepository TELAH ADA DIDALAM PROPERTY $user
        //MAKA PENGGUNAANNYA ADALAH $this->user->namaMethod()
        //DALAM HAL INI KITA AKAN MENGGUNAKAN getPaginate()
        //DIMANA METHOD INI MEMINTA PARAMETER JUMLAH DATA YANG AKAN DITAMPILKAN
        return $this->workingpermit->getPaginate(10);
    }

    public function all()
    { 
    	return $this->workingpermit->showall();
    }

    public function show($params=null)
    {
        //JIKA PARAMETER YANG DITERIMA BUKA NUMERIC
        if (!is_numeric($params)) {
            //MAKA MENGGUNAKAN METHOD findBy()
            //DENGAN MENGIRIMKAN PARAMETER YANG DIINGINKAN
            //UNTUK DIGUNAKAN MEMFILTER DATA
            return $this->workingpermit->findBy('email', $params);
        }
        //JIKA DIA ADALAH NUMERIC
        //MAKA MENGGUNAKAN METHOD find()
        //DENGAN MENGIRIMKAN ID YG AKAN DITAMPILKAN
            return $this->workingpermit->find($params);
        
    }

	
}
