<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Innovations\Hki;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\Models\Innovations\Innovation;
use Illuminate\Support\Facades\Storage;
use App\Models\Innovations\CovidInovasi;
use App\Models\Innovations\JenisInovasi;
use App\Models\Innovations\VideoInovasi;
use Illuminate\Support\Facades\Response;
use App\Models\Innovations\BentukInovasi;
use App\Models\Innovations\BidangInovasi;
use App\Models\Innovations\ProsesInovasi;
use App\Models\Innovations\SistemInovasi;
use App\Models\Innovations\UrusanInovasi;
use App\Models\Innovations\TahapanInovasi;
use App\Models\Innovations\AnggaranInovasi;
use App\Models\Innovations\InisiatorInovasi;
use App\Models\Innovations\PengalamanInovasi;
use App\Models\Innovations\PenghargaanInovasi;
use App\Models\Kuisioners\Kuisioner;
use App\Models\VerifikasiDokumen;

class InnovationsController extends Controller
{
    public function getData()
    {
        if (auth()->check()) {
            if (auth()->user()->hasRole(['administrator'])) {
                //  return 'admin.dashboard';
            }
            if (auth()->user()->hasRole(['innovator'])) {
                $innovator = auth()->user()->innovator()->first();
            }
            if (auth()->user()->hasRole(['auditor'])) {
                // return 'auditor.dashboard';
            }
        }
        $innovatorid = $innovator->id;
        $data = Innovation::whereHas('innovator', function($q) use($innovatorid) {
            $q->where([['innovator_id', $innovatorid],['status','!=', 'deleted']]);
        })->with(['kuisioner','urusaninovasi','penghargaans','pengalamans','hkis','varifikasidokumen'])->get();
        // dd($data);
        return $data;
    }
    public function getallData()
    {
        if (auth()->check()) {
            if (auth()->user()->hasRole(['administrator'])) {
                //  return 'admin.dashboard';
            }
            if (auth()->user()->hasRole(['innovator'])) {
                $innovator = auth()->user()->innovator()->first();
            }
            if (auth()->user()->hasRole(['auditor'])) {
                // return 'auditor.dashboard';
            }
        }
        $innovatorid = $innovator->id;
        // $data = Innovation::whereHas('innovator', function($q) use($innovatorid) {
        //     $q->where([['status','=', 'approve']]);
        // })->with(['kuisioner','urusaninovasi','penghargaans','pengalamans','hkis','varifikasidokumen'])->get();
        

        $data = Innovation::with(['kuisioner','urusaninovasi','penghargaans','pengalamans','hkis','varifikasidokumen'])
                ->where('status', 'approve')
                ->get();
 
        return $data;
    }

    public function getInovasi()
    {
        if (auth()->check()) {
            if (auth()->user()->hasRole(['administrator'])) {
                //  return 'admin.dashboard';
            }
            if (auth()->user()->hasRole(['innovator'])) {
                $innovator = auth()->user()->innovator()->first();
            }
            if (auth()->user()->hasRole(['auditor'])) {
                // return 'auditor.dashboard';
            }
        }
        $innovatorid = $innovator->id;
        $data = Innovation::whereHas('innovator', function($q) use($innovatorid) {
            $q->where([['innovator_id', $innovatorid],['status', 'draft']]);
        })->with(['kuisioner','urusaninovasi','penghargaans','pengalamans','hkis','varifikasidokumen'])->first();

        // $data = Innovation::with('innovator')->where([['innovator_id', $innovatorid->id],['kuisioner_id', 1]])->first();
        // dd($innovation1);
        return $data;
    }

    public function previewInovasi()
    {
        if (auth()->check()) {
            if (auth()->user()->hasRole(['administrator'])) {
                //  return 'admin.dashboard';
            }
            if (auth()->user()->hasRole(['innovator'])) {
                $innovator = auth()->user()->innovator()->first();
            }
            if (auth()->user()->hasRole(['auditor'])) {
                // return 'auditor.dashboard';
            }
        }
        $innovatorid = $innovator->id;
        $data = Innovation::whereHas('innovator', function($q) use($innovatorid) {
            $q->where([['innovator_id', $innovatorid],['status', 'draft']]);
        })->with([
            'urusaninovasi','penghargaans','pengalamans','hkis','jenisinovasi',
            'bidanginovasi','bentukinovasi','sisteminovasi','tahapaninovasi',
            'covidinovasi','inisiatorinovasi','innovator'
        ])->first();

        return $data;
    }

    public function getInnovator()
    {
        if (auth()->check()) {
            if (auth()->user()->hasRole(['administrator'])) {
                //  return 'admin.dashboard';
            }
            if (auth()->user()->hasRole(['innovator'])) {
                $inovator = auth()->user()->innovator()->first();
            }
            if (auth()->user()->hasRole(['auditor'])) {
                // return 'auditor.dashboard';
            }
        }
        // dd($innovation1);
        return $inovator;
    }


    //funtion for get all data inovations (vue js)
    public function getTahapanInovasi()
    {
        $data = TahapanInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getInisiatorInovasi(){
        $data = InisiatorInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getSistemInovasi(){
        $data = SistemInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getBentukInovasi(){
        $data = BentukInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getJenisInovasi(){
        $data = JenisInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getCovidInovasi(){
        $data = CovidInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getBidangInovasi(){
        $data = BidangInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getUrusanInovasi(){
        $alldata = UrusanInovasi::where('id', '>', 1)->get(['id','name']);
        // $data =[
        // //     'id' =>  $alldata->id,
        // //     'name' =>  $alldata->name,
        // // ];
        return $alldata;
    }
    public function getAnggaranInovasi(){
        $data = AnggaranInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getProsesInovasi(){
        $data = ProsesInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getVideoInovasi(){
        $data = VideoInovasi::where('id', '>', 1)->get();
        return $data;
    }

    //function for insert data innovation in the beginning
    public function InsertComments(Request $request)
    {
        $data = $request->all();
        $datacomments = [
            'auditor_id' => $data['auditor_id'],
            'innovation_id' => $data['innovation_id'],
            'data' => $data['data'],
        ];
        $datacoment = VerifikasiDokumen::create($datacomments);
        return $datacoment->id;
    }
    public function UpdateComments(Request $request, $id)
    {
        $data = $request->all();
        $datacomments = [
            'auditor_id' => $data['auditor_id'],
            'innovation_id' => $data['innovation_id'],
            'data' => $data['data'],
        ];
        $datacoment = VerifikasiDokumen::findorFail($id);
        $datacoment->update($datacomments);
        return $datacoment->id;
    }
    public function InsertInnovations(Request $request)
    {
        if (auth()->check()) {
            if (auth()->user()->hasRole(['administrator'])) {
                //  return 'admin.dashboard';
            }
            if (auth()->user()->hasRole(['innovator'])) {
                $innovator = auth()->user()->innovator()->first();
            }
            if (auth()->user()->hasRole(['auditor'])) {
                // return 'auditor.dashboard';
            }
        }
        $innovatorid = $innovator->id;

        $datainnovation = [
            'innovator_id' => $innovatorid,
            'status' => 'draft',
            'checkinovasi' => 0
        ];

        $innovation = Innovation::create($datainnovation);
        $innovation->hkis()->create();
        $kuisioner = Kuisioner::create();
        $innovation->kuisioner()->associate($kuisioner)->save();
        $penghargaan = [
            'nama_penghargaan' => '',
            'pemberi_penghargaan' => '',
            'tahun_penghargaan' => '',
        ];
        $innovation->penghargaans()->create($penghargaan);
        $pengalaman =[
            'nama_pengalaman' => '',
            'penyelenggara' => '',
            'tahun_pengalaman' => '',
            'predikat' => '',
        ];
        $innovation->pengalamans()->create($pengalaman);
        return $innovation->id;
    }
    public function UpdateInnovations(Request $request, $id)
    {
        $data = $request->all();
        // dd($data);
        $data['urusaninovasi'] = json_decode($data['urusaninovasi']);

        $datainnovation = [
            'innovator_id' => $data['innovator_id'],
            'nama_singkat' => $data['nama_singkat'],
            'nama_lengkap' =>  $data['nama_lengkap'],
            'tahapan_inovasi_id' =>  $data['tahapan_inovasi_id'],
            'inisiator_inovasi_id' =>  $data['inisiator_inovasi_id'],
            'sistem_inovasi_id' =>  $data['sistem_inovasi_id'],
            'bentuk_inovasi_id' => $data['bentuk_inovasi_id'],
            'jenis_inovasi_id' => $data['jenis_inovasi_id'],
            'covid_inovasi_id' =>  $data['covid_inovasi_id'],
            'bidang_inovasi_id' =>  $data['bidang_inovasi_id'],
            'waktu_ujicoba' =>  $data['waktu_ujicoba'],
            'waktu_implementasi' =>  $data['waktu_implementasi'],
            'checkinovasi' =>  $data['checkinovasi'],
            'rancang_bangun' => $data['rancang_bangun'],
            'tujuan' => $data['tujuan'],
            'manfaat' => $data['manfaat'],
            'hasil' => $data['hasil'],
            'anggaran_inovasi_id' => $data['anggaran_inovasi_id'],
            'ket_anggaran' => $data['ket_anggaran'],
            'file_anggaran' => $data['file_anggaran'],
            'proses_inovasi_id' => $data['proses_inovasi_id'],
            'ket_proses' => $data['ket_proses'],
            'file_proses' => $data['file_proses'],
            'logo' => $data['logo'],
            'photos' => $data['photos'],
            'video_inovasi_id' => $data['video_inovasi_id'],
            'ket_video' => $data['ket_video'],
            'linkvideos' => $data['linkvideos'],
        ];


        $innovation = Innovation::findorFail($id);
        $innovation->update($datainnovation);

        $hki = [
            'nomor' => $data['nomor'],
            'tanggal' => $data['tanggal'],
            'nama_pencipta' => $data['nama_pencipta'],
            'pemegang_hakcipta' => $data['pemegang_hakcipta'],
            'jenis_ciptaan' => $data['jenis_ciptaan'],
            'judul_ciptaan' => $data['judul_ciptaan'],
            'nomor_pencatatan' => $data['nomor_pencatatan'],
        ];
        if ($data['nomor']!=null) {
            $innovation->hkis()->update($hki);
        }
         // Laravel attach pivot to table with multiple values (one to many)
        if ($data['penghargaans']!=null) {
            $data['penghargaans'] = json_decode($data['penghargaans']);
            $penghargaans = $data['penghargaans'];
            $innovation->penghargaans()->delete();
            foreach ($penghargaans as $key => $value) {
                $penghargaan = [
                    'nama_penghargaan' => $value->nama_penghargaan,
                    'pemberi_penghargaan' => $value->pemberi_penghargaan,
                    'tahun_penghargaan' => $value->tahun_penghargaan,
                ];
                $innovation->penghargaans()->create($penghargaan);
            }

        }

         // Laravel attach pivot to table with multiple values (one to many)
        if ($data['pengalamans']!=null) {
            $data['pengalamans'] = json_decode($data['pengalamans']);
            $pengalamans = $data['pengalamans'];
            $innovation->pengalamans()->delete();
            foreach ($pengalamans as $key => $value) {
                $pengalaman =[
                    'nama_pengalaman' => $value->nama_pengalaman,
                    'penyelenggara' => $value->penyelenggara,
                    'tahun_pengalaman' => $value->tahun_pengalaman,
                    'predikat' => $value->predikat,
                ];
                // dd($pengalamans);
                $innovation->pengalamans()->create($pengalaman);
            }
        }

        // Laravel attach pivot to table with multiple values (many to many)
        if ($data['urusaninovasi']!=null) {
            foreach ($data['urusaninovasi'] as $key => $value) {
                $urusaninovasiids[] = $value->id;
            }

            $innovation->urusaninovasi()->sync($urusaninovasiids);
        }
        return $innovation->id;
    }
    public function updateFinalisasi($id)
    {
        $datainnovation = [
            'status' => 'pending',
            'checkinovasi' => 1
        ];

        $innovation = Innovation::findorFail($id);
        $innovation->update($datainnovation);

        return $innovation->id;
    }
    public function UpdateStatus(Request $request,$id)
    {
        $data = $request->all();
        $status = Innovation::findorFail($id);
        $status->update($data);
        return $status->id;
    }
    public function Form2Innovations(Request $request, $id)
    {
        $data = $request->all();
        $innovations = [
            'rancang_bangun' => $data['rancang_bangun'],
            'tujuan' => $data['tujuan'],
            'manfaat' => $data['manfaat'],
            'hasil' => $data['hasil'],
            'anggaran_inovasi_id' => $data['anggaran_inovasi_id'],
            'ket_anggaran' => $data['ket_anggaran'],
            'file_anggaran' => $data['file_anggaran'],
            'proses_inovasi_id' => $data['proses_inovasi_id'],
            'ket_proses' => $data['ket_proses'],
            'file_proses' => $data['file_proses'],
            'logo' => $data['logo'],
            'photos' => $data['photos'],
            'video_inovasi_id' => $data['video_inovasi_id'],
            'ket_video' => $data['ket_video'],
            'linkvideos' => $data['linkvideos'],
        ];

        $hki = [
            'nomor' => $request['nomor'],
            'tanggal' => $request['tanggal'],
            'nama_pencipta' => $request['nama_pencipta'],
            'pemegang_hakcipta' => $request['pemegang_hakcipta'],
            'jenis_ciptaan' => $request['jenis_ciptaan'],
            'judul_ciptaan' => $request['judul_ciptaan'],
            'nomor_pencatatan' => $request['nomor_pencatatan'],
        ];

        $innovation = Innovation::findorFail($id);
        $innovation->update($innovations);
        $innovation->hkis()->create($hki);
         // Laravel attach pivot to table with multiple values (one to many)
        $data['penghargaans'] = json_decode($data['penghargaans']);
        foreach ($data['penghargaans'] as $key => $value) {

            $penghargaans =[
                'nama_penghargaan' => $value->nama_penghargaan,
                'pemberi_penghargaan' => $value->pemberi_penghargaan,
                'tahun_penghargaan' => $value->tahun_penghargaan,
            ];
            $innovation->penghargaans()->create($penghargaans);
        }
         // Laravel attach pivot to table with multiple values (one to many)
        $data['pengalamans'] = json_decode($data['pengalamans']);
        foreach ($data['pengalamans'] as $key => $value) {
            $pengalamans =[
                'nama_pengalaman' => $value->nama_pengalaman,
                'penyelenggara' => $value->penyelenggara,
                'tahun_pengalaman' => $value->tahun_pengalaman,
                'predikat' => $value->predikat,
            ];
            $innovation->pengalamans()->create($pengalamans);
        }

        return $innovation->id;
    }

    public function Updateform1Innovations(Request $request,$id)
    {
        $data = $request->all();
        // dd($data);
        $data['urusaninovasi'] = json_decode($data['urusaninovasi']);

        $datainnovation = [
            'innovator_id' => $data['innovator_id'],
            'nama_singkat' => $data['nama_singkat'],
            'nama_lengkap' =>  $data['nama_lengkap'],
            'tahapan_inovasi_id' =>  $data['tahapan_inovasi_id'],
            'inisiator_inovasi_id' =>  $data['inisiator_inovasi_id'],
            'sistem_inovasi_id' =>  $data['sistem_inovasi_id'],
            'bentuk_inovasi_id' => $data['bentuk_inovasi_id'],
            'jenis_inovasi_id' => $data['jenis_inovasi_id'],
            'covid_inovasi_id' =>  $data['covid_inovasi_id'],
            'bidang_inovasi_id' =>  $data['bidang_inovasi_id'],
            'waktu_ujicoba' =>  $data['waktu_ujicoba'],
            'waktu_implementasi' =>  $data['waktu_implementasi'],
        ];
        $innovation = Innovation::findorFail($id);
        $innovation->update($datainnovation);

        // Laravel attach pivot to table with multiple values (many to many)
        foreach ($data['urusaninovasi'] as $key => $value) {
            $urusaninovasiids[] = $value->id;
        }

        $innovation->urusaninovasi()->sync($urusaninovasiids);

        return $innovation->id;
    }

    public function Updateform2Innovations(Request $request, $id)
    {
        $data = $request->all();
        $innovations = [
            'rancang_bangun' => $data['rancang_bangun'],
            'tujuan' => $data['tujuan'],
            'manfaat' => $data['manfaat'],
            'hasil' => $data['hasil'],
            'anggaran_inovasi_id' => $data['anggaran_inovasi_id'],
            'ket_anggaran' => $data['ket_anggaran'],
            'file_anggaran' => $data['file_anggaran'],
            'proses_inovasi_id' => $data['proses_inovasi_id'],
            'ket_proses' => $data['ket_proses'],
            'file_proses' => $data['file_proses'],
            'logo' => $data['logo'],
            'photos' => $data['photos'],
            'video_inovasi_id' => $data['video_inovasi_id'],
            'ket_video' => $data['ket_video'],
            'linkvideos' => $data['linkvideos'],
        ];

        $hki = [
            'nomor' => $request['nomor'],
            'tanggal' => $request['tanggal'],
            'nama_pencipta' => $request['nama_pencipta'],
            'pemegang_hakcipta' => $request['pemegang_hakcipta'],
            'jenis_ciptaan' => $request['jenis_ciptaan'],
            'judul_ciptaan' => $request['judul_ciptaan'],
            'nomor_pencatatan' => $request['nomor_pencatatan'],
        ];

        $innovation = Innovation::findorFail($id);
        $innovation->update($innovations);
        $innovation->hkis()->update($hki);
        // $khi= Hki::where('innovation_id',$id)->first();
        // $khi->update($hki);
         // Laravel attach pivot to table with multiple values (one to many)
        $data['penghargaans'] = json_decode($data['penghargaans']);
        foreach ($data['penghargaans'] as $key => $value) {

            $penghargaans =[
                'nama_penghargaan' => $value->nama_penghargaan,
                'pemberi_penghargaan' => $value->pemberi_penghargaan,
                'tahun_penghargaan' => $value->tahun_penghargaan,
            ];
            $innovation->penghargaans()->update($penghargaans);
        }
         // Laravel attach pivot to table with multiple values (one to many)
        $data['pengalamans'] = json_decode($data['pengalamans']);
        foreach ($data['pengalamans'] as $key => $value) {
            $pengalamans =[
                'nama_pengalaman' => $value->nama_pengalaman,
                'penyelenggara' => $value->penyelenggara,
                'tahun_pengalaman' => $value->tahun_pengalaman,
                'predikat' => $value->predikat,
            ];
            $innovation->pengalamans()->update($pengalamans);
        }

        return $innovation->id;
    }

    //function for upload file
    public function uploadAnggaran(Request $request)
    {
        $file = $request->file('file_anggaran');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
    public function uploadProses(Request $request)
    {
        $file = $request->file('file_proses');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
    public function uploadLogo(Request $request)
    {
        // dd($request->all());
        $file = $request->file('logo');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }
    public function uploadPhotos(Request $request)
    {
        $file = $request->file('photos');
        $tujuan_upload = 'uploadfile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $getname = $randno.$nama_file;
        $file->move($tujuan_upload,$getname);

        $data = [
            'name' => $getname,
            'file' => $tujuan_upload.'/'.$getname
        ];
        return $data;
    }

    //delete file
    public function deleteFiles(Request $request)
    {
        unlink($request['filename']);
    }
    //get file
    public function getFiles(Request $request)
    {
        $file = $request['filename'];
        return Response::download($file);
    }
    //delete inovasi
    public function deleteInovasi($id)
    {
        $inovasi = Innovation::findOrFail($id);
        $inovasi->delete();
    }
    public function deleteDatainovasi($id)
    {
        $datainnovation = [
            'status' =>'deleted',
        ];

        $innovation = Innovation::findorFail($id);
        $innovation->update($datainnovation);
    }
}
