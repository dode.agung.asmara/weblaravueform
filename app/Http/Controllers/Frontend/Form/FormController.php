<?php

namespace App\Http\Controllers\Frontend\Form;

use Illuminate\Http\Request;
use App\Models\Forms\InovasiData;
use App\Models\Forms\JenisInovasi;
use App\Http\Controllers\Controller;
use App\Models\Forms\TahapanInovasi;
use App\Models\Forms\InisiatorInovasi;
use App\Http\Requests\Frontend\InovasiRequest;
use App\Models\Forms\BentukInovasi;
use App\Models\Forms\Bimtek;
use App\Models\Forms\CovidInovasi;
use App\Models\Forms\DukunganAnggaran;
use App\Models\Forms\IgaSinovik;
use App\Models\Forms\InfoLayanan;
use App\Models\Forms\JejaringInovasi;
use App\Models\Forms\KecepatanInovasi;
use App\Models\Forms\KemanfaatanInovasi;
use App\Models\Forms\KemudahanLayanan;
use App\Models\Forms\KemudahanProses;
use App\Models\Forms\KepuasanInovasi;
use App\Models\Forms\KetersediaanSdm;
use App\Models\Forms\KualitasInovasi;
use App\Models\Forms\LayananPengaduan;
use App\Models\Forms\OnlineSistem;
use App\Models\Forms\PedomanTeknis;
use App\Models\Forms\PengelolaanInovasi;
use App\Models\Forms\PenggunaanIt;
use App\Models\Forms\ProgramRenstra;
use App\Models\Forms\RegulasiInovasi;
use App\Models\Forms\Replikasi;
use App\Models\Forms\Sosialisasi;
use App\Models\Forms\Stakeholder;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }

    //function insert data componnent 1 with vue js
    public function form1send(Request $request)
    {
        $data = $request->all();
        $formaplikasi = InovasiData::UpdateOrCreate($data);
        return $formaplikasi->id;
    }

    //function update data componnent 2 with vue js where with current id
    public function form2send(Request $request, $id)
    {
        $data = InovasiData::findorFail($id);
        $data->update($request->all());
        // $data = $request->all();
        // $formaplikasi = InovasiData::find($id)->update($data);

        return $data->id;
    }

    //function update data componnent 3 with vue js where with current id
    public function form3send(Request $request,$id)
    {
        $data = InovasiData::findorFail($id);
        $data->update($request->all());

        // $data = $request->all();
        // $formaplikasi = InovasiData::find($id)->update($data);

        return $data->id;
    }

    //function update data componnent 4 with vue js where with current id
    public function form4send(Request $request,$id)
    {
        // $data = $request->all();
        // InovasiData::find($id)->update($data);

        $data = InovasiData::findorFail($id);
        $data->update($request->all());


        //Notif sukses belum
        // return $formaplikasi->id;
        return $data;
    }

    //function uploading file component 1
    public function uploadAnggaran(Request $request){
        $file = $request->file('anggaran_file');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }

    public function uploadProsesbisnis(Request $request){
        $file = $request->file('prosesbisnis_file');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }

    //function uploading file component 2
    public function uploadRegulasi(Request $request){
        $file = $request->file('regulasi_file');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }
    public function uploadSdm(Request $request){
        $file = $request->file('sdm_file');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }
    public function uploadDukunganAnggaran(Request $request){
        $file = $request->file('dukungananggaran_file');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }
    public function uploadIt(Request $request){
        $file = $request->file('it_file');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }
    public function uploadBimtek(Request $request){
        $file = $request->file('bimtek_file');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }
    public function uploadRenstra(Request $request){
        $file = $request->file('renstra_file');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }
    public function uploadJejaring(Request $request){
        $file = $request->file('jejaring_file');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }
    public function uploadReplikasi(Request $request){
        $file = $request->file('replikasi_file');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }

    //function uploading file component 3
    public function uploadPedoman(Request $request){
        $file = $request->file('pedoman_file');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }
    public function uploadPengelolaan(Request $request){
        $file = $request->file('pengelolaan_file');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }
    public function uploadInfo(Request $request){
        $file = $request->file('info_file');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }
    public function uploadPengaduan(Request $request){
        $file = $request->file('pengaduan_file');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }
    public function uploadStakeholder(Request $request){
        $file = $request->file('stakeholder_file');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }
    public function uploadLayanan(Request $request){
        $file = $request->file('layanan_file');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }
    public function uploadProses(Request $request){
        $file = $request->file('proses_file');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }
    public function uploadOnline(Request $request){
        $file = $request->file('online_file');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }

    //function uploading file component 4
    public function uploadKecepatan(Request $request){
        $file = $request->file('kecepatan_file');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }
    public function uploadKemanfaatan(Request $request){
        $file = $request->file('kemanfaatan_file');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }
    public function uploadKepuasan(Request $request){
        $file = $request->file('kepuasan_file');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }
    public function uploadSosialisasi(Request $request){
        $file = $request->file('sosialisasi_file');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }

    //get all data from inovasi data table to component (vue js)
    public function form1get()
    {
        $inovasidatas = InovasiData::all();
        return $inovasidatas;
    }

    //funtion for get all data to component 1 (vue js)
    public function getTahapanInovasi()
    {
        $data = TahapanInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getInisiatorInovasi()
    {
        $data = InisiatorInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getJenisInovasi()
    {
        $data = JenisInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getBentukInovasi()
    {
        $data = BentukInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getCovidInovasi()
    {
        $data = CovidInovasi::where('id', '>', 1)->get();
        return $data;
    }

    //funtion for get all data to component 2 (vue js)
    public function getRegulasiInovasi()
    {
        $data = RegulasiInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getKetersediaanSdm()
    {
        $data = KetersediaanSdm::where('id', '>', 1)->get();
        return $data;
    }
    public function getDukunganAnggaran()
    {
        $data = DukunganAnggaran::where('id', '>', 1)->get();
        return $data;
    }
    public function getPenggunaanIt()
    {
        $data = PenggunaanIt::where('id', '>', 1)->get();
        return $data;
    }
    public function getBimtek()
    {
        $data = Bimtek::where('id', '>', 1)->get();
        return $data;
    }
    public function getProgramRenstra()
    {
        $data = ProgramRenstra::where('id', '>', 1)->get();
        return $data;
    }
    public function getJejaringInovasi()
    {
        $data = JejaringInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getReplikasi()
    {
        $data = Replikasi::where('id', '>', 1)->get();
        return $data;
    }

    //funtion for get all data to component 3 (vue js)
    public function getPedomanTeknis()
    {
        $data = PedomanTeknis::where('id', '>', 1)->get();
        return $data;
    }
    public function getPengelolaanInovasi()
    {
        $data = PengelolaanInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getInfoLayanan()
    {
        $data = InfoLayanan::where('id', '>', 1)->get();
        return $data;
    }
    public function getLayananPengaduan()
    {
        $data = LayananPengaduan::where('id', '>', 1)->get();
        return $data;
    }
    public function getStakeholder()
    {
        $data = Stakeholder::where('id', '>', 1)->get();
        return $data;
    }
    public function getKemudahanLayanan()
    {
        $data = KemudahanLayanan::where('id', '>', 1)->get();
        return $data;
    }
    public function getKemudahanProses()
    {
        $data = KemudahanProses::where('id', '>', 1)->get();
        return $data;
    }
    public function getOnlineSistem()
    {
        $data = OnlineSistem::where('id', '>', 1)->get();
        return $data;
    }

    //funtion for get all data to component 4 (vue js)
    public function getKecepatanInovasi()
    {
        $data = KecepatanInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getKemanfaatanInovasi()
    {
        $data = KemanfaatanInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getKepuasanInovasi()
    {
        $data = KepuasanInovasi::where('id', '>', 1)->get();
        return $data;
    }
    public function getSosialisasi()
    {
        $data = Sosialisasi::where('id', '>', 1)->get();
        return $data;
    }
}
