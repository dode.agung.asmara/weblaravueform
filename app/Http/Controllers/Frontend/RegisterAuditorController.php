<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Auth\User;
use Illuminate\Support\Facades\Hash;
use App\Events\Frontend\Auth\UserRegistered;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Notifications\Frontend\Auth\UserNeedsConfirmation;
use App\Models\Auditor;

/**
 * Class HomeController.
 */
class RegisterAuditorController extends Controller
{

    public function registrasi()
    {
        return view('frontend.register_auditor');
    }

    public function registrasiproses(Request $request)
    {

        try{
            \DB::transaction(function() use ($request){

                $this->validate($request, [
                    'nama' => 'required',
                    'email' => 'required|email|unique:users,email',
                    'password' => 'required',
                ]);

                $user = User::create([
                    'first_name' => $request->nama,
                    'last_name' => 'Auditor',
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                    'confirmation_code' => md5(uniqid(mt_rand(), true)),
                    'active' => true,
                    'confirmed' => 0,
                ]);

                $user->assignRole(config('access.users.auditor_role'));

                Auditor::create([
                    'user_id' => $user->id,
                    'nama' => $request->nama,
                    'keterangan' => '',
                ]);

            });
            return response()->json(['message' => 'success']);        
        }catch(\Exceptions $e){
            return response()->json(['message' => 'error']);
        }

    }

    public function checkemail($email) 
    {
      $exists = User::where('email', $email)->exists(); 
      if($exists){
          return response()->json([
            'valid' => false
        ]);
      }else{
        return response()->json([
            'valid' => !$exists
        ]);
    }
}

}
