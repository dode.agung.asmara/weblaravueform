<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Auth\User;
use Illuminate\Support\Facades\Hash;
use App\Events\Frontend\Auth\UserRegistered;
use Illuminate\Foundation\Auth\RegistersUsers;

use App\Notifications\Frontend\Auth\UserNeedsConfirmation;
use App\Models\Innovators\InnovatorCategory;
use App\Models\Innovators\JenisInnovator;
use App\Models\Innovators\Innovator;
use App\Models\Innovations\Innovation;
use App\Models\Innovations\UrusanInovasi;

// use Artesaos\SEOTools\Facades\SEOTools;


/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */

    public function index()
    {
        $inovator = Innovator::get();
        $inovasi = Innovation::where('status', 'approve')->get();
        return view('frontend.index1', compact('inovator','inovasi'));
    }

    public function inovasi()
    {
        return view('frontend.inovasi1');
    }

    public function inovasilist()
    {
        return Innovation::with([
            'urusaninovasi','penghargaans','pengalamans','hkis','jenisinovasi',
            'bidanginovasi','bentukinovasi','sisteminovasi','tahapaninovasi',
            'urusaninovasi','covidinovasi','inisiatorinovasi','innovator'
        ])->where('status', 'approve')->get();
    }

    public function mapping()
    {
        return view('frontend.map');
    }

    public function listmap()
    {
        $inovator = Innovator::whereHas('innovation', function($q){
            $q->where('status', 'approve');
        })->with(array('innovation' => function($q){
            $q->where('status', 'approve');
        }))
        ->get();
        return response()->json($inovator);
    }

    public function mapfilter(Request $request)
    {
        $data = $request->all();
        $inovator = Innovator::with(array('innovation' => function($q){
            $q->where('status', 'approve');
        }))
        ->whereHasInovasi($data)
        ->getKategoriInovator($data['kategoriInovator'])
        ->getJenisInovator($data['jenisInovator'])
        ->get();
        return $inovator;
    }

    public function getData()
    {
        $checkId ='false';
        $data = Innovation::whereHas('kuisioner', function($q) use($checkId) {
            $q->where('status', '!=', $checkId);
        })->with(['innovator','urusaninovasi','penghargaans','pengalamans','hkis','kuisioner','varifikasidokumen'])->get();
        return $data;
    }

    public function previewInovasi($id)
    {
        $data = Innovation::whereHas('innovator', function($q) use($id) {
            $q->where([['innovator_id', $id],['status', 'draft']]);
        })->with([
            'urusaninovasi','penghargaans','pengalamans','hkis','jenisinovasi',
            'bidanginovasi','bentukinovasi','sisteminovasi','tahapaninovasi',
            'covidinovasi','inisiatorinovasi','innovator'
        ])->first();

        return $data;
    }

    public function getUrusanInovasi(){
        $alldata = UrusanInovasi::where('id', '>', 1)->get(['id','name']);
        return $alldata;
    }

    public function registrasi()
    {
        $kategori = InnovatorCategory::get();
        $jenis = JenisInnovator::get();
        $model = New Innovator;
        return view('frontend.register', compact('kategori', 'jenis', 'model'));
    }

    public function registrasiproses(Request $request)
    {

        try{

            if(!$request->nama_pic){
                $request->nama_pic=null;
            }
            if(!$request->email_cadangan){
                $request->email_cadangan=null;
            }
            if(!$request->kontak_cadangan){
                $request->kontak_cadangan=null;
            }

            $this->validate($request, [
                'nama' => 'required',
                'email' => 'required|email|unique:users,email',
                'email_cadangan' => 'email',
                'kategori' => 'required',
                'jenis' => 'required',
                'kontak' => 'required',
                'jalan' => 'required',
                'desa' => 'required',
                'kecamatan' => 'required',
                'kota' => 'required',
                'provinsi' => 'required',
                'map' => 'required',
                'password' => 'required',
            ]);

            $kategori = '';
            if($request->kategori === '1'){
                $kategori = 'OPD';
            }else if($request->kategori === '2'){
                $kategori = 'NON OPD';
            }

            $user = User::create([
                'first_name' => $request->nama,
                'last_name' => $kategori,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'active' => true,
                'confirmed' => ! (config('access.users.requires_approval') || config('access.users.confirm_email')),
            ]);

            if($user){

                $user->assignRole(config('access.users.innovator_role'));

                Innovator::create([
                    'user_id' => $user->id,
                    'category_id' => $request->get('kategori'),
                    'jenis_id' => $request->get('jenis'),
                    'nama' => $request->nama,
                    'nama_pic' => $request->nama_pic,
                    'email_cadangan' => $request->email_cadangan,
                    'kontak' => $request->kontak,
                    'kontak_cadangan' => $request->kontak_cadangan,
                    'jalan' => $request->jalan,
                    'desa' => $request->desa,
                    'kecamatan' => $request->kecamatan,
                    'kota' => $request->kota,
                    'provinsi' => $request->provinsi,
                    'map' => $request->map,
                ]);

                if (config('access.users.confirm_email')) {
                    $user->notify(new UserNeedsConfirmation($user->confirmation_code));
                } 
                
                return response()->json(['message'=>'sukses']);
            }

        } catch (\Exception $e) {
            return response()->json(['message'=>'gagal']);
        }

    }

    public function checkemail($email) 
    {
      $exists = User::where('email', $email)->exists(); 
      if($exists){
          return response()->json([
            'valid' => false
        ]);
      }else{
        return response()->json([
            'valid' => !$exists
        ]);
    }
}

}
