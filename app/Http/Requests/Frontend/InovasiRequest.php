<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;

class InovasiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'email' => 'required|email',
                'nama_inovasi' => 'required',
                'nama_odp' => 'required',
                'tahap_inovasi' => 'required',
                'inisiator_inovasi' => 'required',
                'jenis_inovasi' => 'required',
                'bentuk_inovasi' => 'required',
                'covid_inovasi' => 'required',
                'tgl_uji' => 'required',
                'tgl_implementasi' => 'required',
                'rencana_bangun' => 'required',
                'manfaat_inovasi' => 'required',
                'hasil_inovasi' => 'required',
                'anggaran' => 'required',
                'proses_bisnis' => 'required',

                'regulasi_inovasi' => 'required',
                'data_regulasi' => 'required',
                'nama_regulasi' => 'required',
                'ketersediaan_sdm' => 'required',
                'data_sdm' => 'required',
                'nama_sdm' => 'required',
                'dukungan_anggaran' => 'required',
                'data_anggaran' => 'required',
                'nama_anggaran' => 'required',
                'penggunaan_it' => 'required',
                'data_it' => 'required',
                'nama_it' => 'required',
                'bimtek' => 'required',
                'data_bimtek' => 'required',
                'nama_bimtek' => 'required',
                'program_renstra' => 'required',
                'data_renstra' => 'required',
                'nama_renstra' => 'required',
                'jejaring_inovasi' => 'required',
                'data_jejaring' => 'required',
                'nama_jejaring' => 'required',
                'replikasi' => 'required',
                'data_replikasi' => 'required',
                'nama_replikasi' => 'required',

                'pedoman_teknis' => 'required',
                'data_pedoman' => 'required',
                'nama_pedoman' => 'required',
                'pengelolaan_inovasi' => 'required',
                'data_pengelolaan' => 'required',
                'nama_pengelolaan' => 'required',
                'info_layanan' => 'required',
                'data_info' => 'required',
                'nama_info' => 'required',
                'layanan_pengaduan' => 'required',
                'data_pengaduan' => 'required',
                'nama_pengaduan' => 'required',
                'stakeholder' => 'required',
                'data_stakeholder' => 'required',
                'nama_stakeholder' => 'required',
                'kemudahan_layanan' => 'required',
                'data_kemudahan' => 'required',
                'nama_kemudahan' => 'required',
                'kemudahan_proses' => 'required',
                'data_proses' => 'required',
                'nama_proses' => 'required',
                'online_sistem' => 'required',
                'data_online' => 'required',
                'nama_online' => 'required',

                'kecepatan_inovasi' => 'required',
                'data_kecepatan' => 'required',
                'nama_kecepatan' => 'required',
                'kemanfaatan_inovasi' => 'required',
                'data_kemanfaatan' => 'required',
                'nama_kemanfaatan' => 'required',
                'kepuasan_inovasi' => 'required',
                'data_kepuasan' => 'required',
                'nama_kepuasan' => 'required',
                'sosialisasi' => 'required',
                'data_sosialisasi' => 'required',
                'nama_sosialisasi' => 'required',
                'kualitas_inovasi' => 'required',
                'data_kualitas' => 'required',
                'url1_kualitas' => 'required',
                'url2_kualitas' => 'required',
                'iga_sinovik' => 'required',
                'catatan' => 'required'
        ];
    }
}
