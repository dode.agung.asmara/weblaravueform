
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import '../../bootstrap';
import '../../plugins';
import Vue from 'vue';
import VeeValidate from 'vee-validate';
// Import Bootstrap an BootstrapVue CSS files (order is important)
import { BootstrapVue, IconsPlugin, BootstrapVueIcons } from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'bootstrap-vue/dist/bootstrap-vue-icons.min.css'

/**
 * Wrappers
 * Here are the code made do simplify the proccess with Toast, Ajax, etc
 */

// Toast Wrapper - Adapts the IziToast to use inside the AxiosWrapper
window.Toast = require('./wrappers/toastWrapper');
//Izi Toast
window.IziToast = require('izitoast');
import 'izitoast/dist/css/iziToast.css'
Vue.use(BootstrapVueIcons)

const VueUploadComponent = require('vue-upload-component')

window.Vue = Vue;
Vue.use(BootstrapVue)
Vue.use(VeeValidate, {
    inject: true,
    fieldsBagName: "veeFields",
    errorBagName: "veeErrors"
});


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('file-upload', VueUploadComponent)

// data innnovation components
Vue.component('data-inovasi', require('./components/DataInovasi.vue').default);
Vue.component('add-inovasi', require('./components/AddInovasi.vue').default);
Vue.component('preview-inovasi', require('./components/PreviewInovasi.vue').default);

//innovation components
Vue.component('viewinovasi-opd', require('./components/ViewInovasiopd.vue').default);
Vue.component('viewinovasi-nonopd', require('./components/ViewInovasinonopd.vue').default);

//Kuisioner components
Vue.component('viewkuisioner-opd', require('./components/ViewKuisioneropd.vue').default);
Vue.component('viewkuisioner-nonopd', require('./components/ViewKuisionernonopd.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


const app = new Vue({
    el: '#app',
    data: {
        isShowDataInovasi:true,
        isShowAddInovasi:false,
        isShowViewInovasiopd:false,
        isShowViewInovasinonopd:false,
        isShowViewKuisioneropd:false,
        isShowViewKuisionernonopd:false,

        isShowPreviewInovasi:false,
        currentData:{}
    },
    methods: {
    	 getDataComponent(value){
    	 	this.currentData = value.data
    	 	if(value.visit==='DataInovasi'){
    	 		this.reset()
    	 		this.isShowDataInovasi=true
    	 	}
    	 	if(value.visit==='AddInovasi'){
    	 		this.reset()
    	 		this.isShowAddInovasi=true
    	 	}
    	 	if(value.visit==='ViewInovasiopd'){
    	 		this.reset()
    	 		this.isShowViewInovasiopd=true
    	 	}
    	 	if(value.visit==='ViewInovasinonopd'){
    	 		this.reset()
    	 		this.isShowViewInovasinonopd=true
    	 	}
    	 	if(value.visit==='ViewKuisioneropd'){
    	 		this.reset()
    	 		this.isShowViewKuisioneropd=true
    	 	}
    	 	if(value.visit==='ViewKuisionernonopd'){
    	 		this.reset()
    	 		this.isShowViewKuisionernonopd=true
    	 	}
            if(value.visit==='PreviewInovasi'){
                this.reset()
                this.isShowPreviewInovasi=true
            }
    	 },
    	 reset(){
            this.isShowDataInovasi=false
            this.isShowAddInovasi=false
            this.isShowViewInovasiopd=false
            this.isShowViewInovasinonopd=false
            this.isShowViewKuisioneropd=false
            this.isShowViewKuisionernonopd=false
            this.isShowPreviewInovasi=false
    	 }


    }
});
