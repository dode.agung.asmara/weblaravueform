
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import '../../bootstrap';
import '../../plugins';
import Vue from 'vue';
import VeeValidate from 'vee-validate';
// Import Bootstrap an BootstrapVue CSS files (order is important)
import { BootstrapVue, IconsPlugin, BootstrapVueIcons  } from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import VueSweetalert2 from 'vue-sweetalert2'
/**
 * Wrappers
 * Here are the code made do simplify the proccess with Toast, Ajax, etc
 */

// Toast Wrapper - Adapts the IziToast to use inside the AxiosWrapper
window.Toast = require('./wrappers/toastWrapper');
//Izi Toast
window.IziToast = require('izitoast');
import 'izitoast/dist/css/iziToast.css'


const VueUploadComponent = require('vue-upload-component')
Vue.use(VueSweetalert2)
window.Vue = Vue;
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(VeeValidate, {
    inject: true,
    fieldsBagName: "veeFields",
    errorBagName: "veeErrors"
});


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('file-upload', VueUploadComponent)

// data innnovation components
Vue.component('data-inovasiall', require('./components/DataInovasiall.vue').default);
Vue.component('add-inovasi', require('./components/AddInovasi.vue').default);
Vue.component('preview-inovasi', require('./components/PreviewInovasi.vue').default);

//innovation components
Vue.component('inovasi-opd', require('./components/InovasiOpd.vue').default);
Vue.component('inovasi-nonopd', require('./components/InovasiNonopd.vue').default);
Vue.component('edit-inovasiopd', require('./components/EditinovasiOpd.vue').default);
Vue.component('edit-inovasinonopd', require('./components/EditinovasiNonopd.vue').default);

//Kuisioner components
Vue.component('kuisioner-opd', require('./components/KuisionerOpd.vue').default);
Vue.component('kuisioner-non', require('./components/KuisionerNon.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


const app = new Vue({
    el: '#app',
    data: {

        isShowDataInovasi:true,
        isShowKuisionerOpd:false,
        isShowKuisionerNonOpd:false,
        isShowAddInovasi:false,

        isShowInovasiOpd:false,
        isShowInovasiNonopd:false,

        isShowEditinovasiOpd:false,
        isShowEditinovasiNonopd:false,

        isShowPreviewInovasi:false,


        currentData:{}
    },
    methods: {
    	 getDataComponent(value){
    	 	this.currentData = value.data
    	 	if(value.visit==='DataInovasi'){
    	 		this.reset()
    	 		this.isShowDataInovasi=true
    	 	}
    	 	if(value.visit==='KuisionerOpd'){
    	 		this.reset()
    	 		this.isShowKuisionerOpd=true
    	 	}
    	 	if(value.visit==='KuisionerNonopd'){
    	 		this.reset()
    	 		this.isShowKuisionerNonOpd=true
    	 	}
    	 	if(value.visit==='AddInovasi'){
    	 		this.reset()
    	 		this.isShowAddInovasi=true
    	 	}
    	 	if(value.visit==='InovasiOpd'){
    	 		this.reset()
    	 		this.isShowInovasiOpd=true
    	 	}
    	 	if(value.visit==='EditinovasiOpd'){
    	 		this.reset()
    	 		this.isShowEditinovasiOpd=true
    	 	}

    	 	if(value.visit==='InovasiNonopd'){
    	 		this.reset()
    	 		this.isShowInovasiNonopd=true
    	 	}

    	 	if(value.visit==='EditinovasiNonopd'){
    	 		this.reset()
    	 		this.isShowEditinovasiNonopd=true
    	 	}

            if(value.visit==='PreviewInovasi'){
                this.reset()
                this.isShowPreviewInovasi=true
            }

    	 },
    	 reset(){
            this.isShowDataInovasi=false
            this.isShowAddInovasi=false
            this.isShowInovasiOpd=false
            this.isShowInovasiNonopd=false
            this.isShowEditinovasiOpd=false
            this.isShowEditinovasiNonopd=false
            this.isShowKuisionerOpd=false
            this.isShowKuisionerNonOpd=false
            this.isShowPreviewInovasi=false
    	 }


    }
});
