
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import '../../../bootstrap';
import '../../../plugins';
import Vue from 'vue';
import VeeValidate from 'vee-validate';
import VueSweetalert2 from 'vue-sweetalert2';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import ToggleButton from 'vue-js-toggle-button'

window.Vue = Vue;

// Toast Wrapper - Adapts the IziToast to use inside the AxiosWrapper
window.Toast = require('./wrappers/toastWrapper');
//Izi Toast
window.IziToast = require('izitoast');
import 'izitoast/dist/css/iziToast.css'

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));


Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueSweetalert2);
Vue.use(VeeValidate, {
    inject: true,
    fieldsBagName: "veeFields",
    errorBagName: "veeErrors"
});
Vue.use(ToggleButton)

Vue.component('list-all', require('./components/Table.vue').default);
Vue.component('form-add', require('./components/FormAdd.vue').default);
Vue.component('form-edit', require('./components/FormEdit.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: {
        showListAll:true,
        showFormAdd:false,
        showFormEdit:false,

        choosedData:{},
    },
    methods: {// receives a place object via the autocomplete component
                
        getDataComponent(value){
            this.choosedData = value.data

            if(value.action=='list'){
                this.reset()
                this.showListAll=true
            }else if(value.action=='add'){
                this.reset()
                this.showFormAdd=true
            }else if(value.action=='edit'){
                this.reset()
                this.showFormEdit=true
            }
        },
        reset(){
            this.showListAll=false
            this.showFormAdd=false
            this.showFormEdit=false
        }

    }
});