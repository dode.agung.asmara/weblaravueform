
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import '../../bootstrap';
import '../../plugins';
import Vue from 'vue';
import VeeValidate from 'vee-validate';
import Multiselect from 'vue-multiselect';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// import VueScrollTo  from 'vue-scrollto';
// import DataTable from 'laravel-vue-datatable';


window.Vue = Vue;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));
// Vue.use(DataTable);

Vue.use(BootstrapVue)
// Vue.use(Dayjs)
// Vue.use(VueSweetalert2)
// Vue.use(VueMoment);
Vue.use(VeeValidate, {
    inject: true,
    fieldsBagName: "veeFields",
    errorBagName: "veeErrors"
});

Vue.component('multiselect', Multiselect)

Vue.component('table-innovation', require('./components/TableInnovation.vue').default);
Vue.component('form-edit', require('./components/FormEdit.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: {
        choosedData:{},
        showListAll:true,
        showFormAdd:false,
        showFormEdit:false,
    },
    methods: {// receives a place object via the autocomplete component
                
        getData(value){
            // console.log(value)
            if(value.action=='list'){
                this.showListAll=true
                this.showFormAdd=false
                this.showFormEdit=false
            }else if(value.action=='add'){
                this.showListAll=false
                this.showFormAdd=true
                this.showFormEdit=false
            }else if(value.action=='edit'){
                this.showListAll=false
                this.showFormAdd=false
                this.showFormEdit=true
            }
            this.choosedData = value.data
        }

    }
});