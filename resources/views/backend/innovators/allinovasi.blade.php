@extends('backend.layouts.app')
@section('content')
<data-inovasiall  v-if="isShowDataInovasi" @datacomponent="getDataComponent" :dataprops="currentData"></data-inovasiall>
<kuisioner-opd  v-if="isShowKuisionerOpd" @datacomponent="getDataComponent" :dataprops="currentData"></kuisioner-opd>
<kuisioner-non  v-if="isShowKuisionerNonOpd" @datacomponent="getDataComponent" :dataprops="currentData"></kuisioner-non>
<add-inovasi  v-if="isShowAddInovasi" @datacomponent="getDataComponent" :dataprops="currentData"></add-inovasi>
<preview-inovasi  v-if="isShowPreviewInovasi" @datacomponent="getDataComponent" :dataprops="currentData"></preview-inovasi>
<inovasi-opd  v-if="isShowInovasiOpd" @datacomponent="getDataComponent" :dataprops="currentData"></inovasi-opd>
<inovasi-nonopd  v-if="isShowInovasiNonopd" @datacomponent="getDataComponent" :dataprops="currentData"></inovasi-nonopd>
<edit-inovasiopd  v-if="isShowEditinovasiOpd" @datacomponent="getDataComponent" :dataprops="currentData"></edit-inovasiopd>
<edit-inovasinonopd  v-if="isShowEditinovasiNonopd" @datacomponent="getDataComponent" :dataprops="currentData"></edit-inovasiopd>


@endsection
@section('pagespecificscripts')
    {!! script(mix('js/innovationall.js')) !!}
@stop
