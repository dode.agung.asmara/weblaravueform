@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')

<div class="dashboardd">
    
<!-- inovator, kategori, jenis start -->
<!-- inovator, kategori, jenis end -->

<!-- inovasi jenis, bentuk, bidang start -->
    @include('backend.includes.dashboard.inovasi1')
<!-- inovasi jenis, bentuk, bidang end -->

<!-- inovasi inisiator, sistem, tahapan, anggaran, proses, covid start -->
    @include('backend.includes.dashboard.inovasi2')
<!-- inovasi inisiator, sistem, tahapan, anggaran, proses, covid end -->

<!-- <div class="content-wrapper fade-in" >   
    <section class="content" v-if="openTab">
        <div class="container-fluid">
            <div class="row">
                <input type="hidden" id="idUser" value="{{$idUser ?? ''}}">
                <transition name="slide-fade">
                    <div class="col-md-12" v-if="showTableWorkingPermit">
                         <table-workingpermit ref="tableWorkingPermit" @data-workingpermit="getDataWorkingPermit">
                                </table-workingpermit>
                    </div>
                </transition>                

                <transition name="slide-fade">
                    <div class="col-md-12" v-if="showFormWorkingPermit">
                         <table-workingpermit ref="tableWorkingPermit" @data-workingpermit="getDataWorkingPermit">
                                </table-workingpermit>
                    </div>
                </transition>
            </div>
        </div>
    </section>

</div> -->

</div>
@endsection

@section('pagespecificscripts')
    {!! script(mix('js/dashboard.js')) !!}
@stop