<ul class="nav">
  @role('administrator')
  <li class="nav-item nav-category">Main</li>
  <li class="nav-item">
    <a href="{{route('admin.dashboard')}}" class="nav-link">
      <i class="link-icon" data-feather="box"></i>
      <span class="link-title">Dashboard</span>
    </a>
  </li>
  <li class="nav-item">
    <a href="{{route('admin.mapping')}}" class="nav-link">
      <i class="link-icon" data-feather="map"></i>
      <span class="link-title">Map</span>
    </a>
  </li>
  <li class="nav-item nav-category">Inovasi</li>
  <li class="nav-item">
    <a href="{{route('admin.innovations')}}" class="nav-link">
      <i class="link-icon" data-feather="calendar"></i>
      <span class="link-title">List Inovasi</span>
    </a>
  </li>
  <li class="nav-item nav-category">User</li>
  <li class="nav-item">
    <a href="{{ route('admin.auditor') }}" class="nav-link">
      <i class="link-icon" data-feather="users"></i>
      <span class="link-title">Auditor</span>
    </a>
  </li>
  <li class="nav-item">
    <a href="{{ route('admin.inovator') }}" class="nav-link">
      <i class="link-icon" data-feather="users"></i>
      <span class="link-title">Inovator</span>
    </a>
  </li>
  @endrole

  @role('innovator')
  <li class="nav-item nav-category">Main</li>
  <li class="nav-item">
    <a href="{{route('innovator.dashboard')}}" class="nav-link">
      <i class="link-icon" data-feather="box"></i>
      <span class="link-title">Dashboard</span>
    </a>
  </li>
  <li class="nav-item">
    <a href="{{route('innovator.mapping')}}" class="nav-link">
      <i class="link-icon" data-feather="map"></i>
      <span class="link-title">Map</span>
    </a>
  </li>
  <li class="nav-item nav-category">Inovasi</li>
  <li id="nav-inovasime" class="nav-item">
    <a href="{{route('innovator.inovasisaya')}}" class="nav-link">
      <i class="link-icon" data-feather="map"></i>
      <span class="link-title">Inovasi Saya</span>
    </a>
  </li>
  <li id="nav-inovasiall" class="nav-item">
   <a href="{{route('innovator.inovasiall')}}" class="nav-link">
    <i class="link-icon" data-feather="calendar"></i>
    <span class="link-title">Semua Inovasi</span>
  </a>
</li>
@endrole

@role('auditor')
<li class="nav-item nav-category">Main</li>
<li class="nav-item">
  <a href="{{route('auditor.dashboard')}}" class="nav-link">
    <i class="link-icon" data-feather="box"></i>
    <span class="link-title">Dashboard</span>
  </a>
</li>
<li class="nav-item">
  <a href="{{route('auditor.mapping')}}" class="nav-link">
    <i class="link-icon" data-feather="map"></i>
    <span class="link-title">Map</span>
  </a>
</li>
<li class="nav-item nav-category">Inovasi</li>
<li class="nav-item">
  <a href="{{route('auditor.inovasi')}}" class="nav-link">
    <i class="link-icon" data-feather="calendar"></i>
    <span class="link-title">List</span>
  </a>
</li>
@endrole

<li class="nav-item nav-category">Account</li>
<li class="nav-item">
  <a href="{{ route('frontend.auth.logout') }}" class="nav-link">
    <i class="link-icon" data-feather="log-out"></i>
    <span class="link-title">Logout</span>
  </a>
</li>

</ul>
