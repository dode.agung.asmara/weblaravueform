<!-- jenis inovasi start -->
<div class="row">

  <div class="col-lg-4 col-xl-4 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="d-flex justify-content-between align-items-baseline mb-2">
          <h5 class="card-title mb-0">Total Inovasi</h5>
        </div>

        <div class="row">
          <div class="col-12 col-xl-12 stretch-card">
            <div class="row flex-grow">

              <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline">
                      <h5 class="card-title mb-0">Total Inovasi</h5>
                    </div>
                    <div class="row">
                      <div class="col-12 col-md-12 col-xl-12">
                        <h3 class="mb-2">{{count($inovasi)}}</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div> 

            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

    <div class="col-lg-8 col-xl-8 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-baseline mb-2">
              <h5 class="card-title mb-0">Jenis Inovasi</h5>
            </div>

            <div class="row">
              <div class="col-12 col-xl-12 stretch-card">
                <div class="row flex-grow">

                  <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                          <h5 class="card-title mb-0">Produk</h5>
                        </div>
                        <div class="row">
                          <div class="col-6 col-md-12 col-xl-5">
                            <h3 class="mb-2">{{count($inovasi->where('jenis_inovasi_id', 2))}}</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                          <h5 class="card-title mb-0">Pelayanan</h5>
                        </div>
                        <div class="row">
                          <div class="col-6 col-md-12 col-xl-5">
                            <h3 class="mb-2">{{count($inovasi->where('jenis_inovasi_id', 3))}}</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                   <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                          <h5 class="card-title mb-0">Proses</h5>
                        </div>
                        <div class="row">
                          <div class="col-6 col-md-12 col-xl-5">
                            <h3 class="mb-2">{{count($inovasi->where('jenis_inovasi_id', 4))}}</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  </div>
              </div>
            </div>

          </div>
        </div>
      </div>

</div>
<!-- jenis inovasi end -->

<!-- bentuk inovasi start -->
<div class="row">

    <div class="col-lg-12 col-xl-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-baseline mb-2">
              <h5 class="card-title mb-0">Bentuk Inovasi</h5>
            </div>

            <div class="row">
              <div class="col-12 col-xl-12 stretch-card">
                <div class="row flex-grow">

                  <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                          <h5 class="card-title mb-0">Pelayanan Publik</h5>
                        </div>
                        <div class="row">
                          <div class="col-6 col-md-12 col-xl-5">
                            <h3 class="mb-2">{{count($inovasi->where('bentuk_inovasi_id', 2))}}</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                          <h5 class="card-title mb-0">Tata Kelola Pemda</h5>
                        </div>
                        <div class="row">
                          <div class="col-6 col-md-12 col-xl-5">
                            <h3 class="mb-2">{{count($inovasi->where('bentuk_inovasi_id', 3))}}</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                   <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                          <h5 class="card-title mb-0">Bentuk Lainnya</h5>
                        </div>
                        <div class="row">
                          <div class="col-6 col-md-12 col-xl-5">
                            <h3 class="mb-2">{{count($inovasi->where('bentuk_inovasi_id', 4))}}</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  </div>
              </div>
            </div>

          </div>
        </div>
      </div>

</div>
<!-- bentuk inovasi end -->

<!-- bidang inovasi -->
<div class="row">

    <div class="col-lg-12 col-xl-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-baseline mb-2">
              <h5 class="card-title mb-0">Bidang Inovasi</h5>
            </div>

            <div class="row">
              <div class="col-12 col-xl-12 stretch-card">
                <div class="row flex-grow">

                  <div class="col-md-3 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <div class="d-flex justify-content-between align-items-baseline">
                  <h5 class="card-title mb-0">Pangan</h5>
                </div>
                <div class="row">
                  <div class="col-6 col-md-12 col-xl-5">
                    <h3 class="mb-2">{{count($inovasi->where('bidang_inovasi_id', 2))}}</h3>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-3 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <div class="d-flex justify-content-between align-items-baseline">
                  <h5 class="card-title mb-0">Hankam</h5>
                </div>
                <div class="row">
                  <div class="col-6 col-md-12 col-xl-5">
                    <h3 class="mb-2">{{count($inovasi->where('bidang_inovasi_id', 3))}}</h3>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-3 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <div class="d-flex justify-content-between align-items-baseline">
                  <h5 class="card-title mb-0">Manufaktur</h5>
                </div>
                <div class="row">
                  <div class="col-6 col-md-12 col-xl-5">
                    <h3 class="mb-2">{{count($inovasi->where('bidang_inovasi_id', 4))}}</h3>
                  </div>
                </div>
              </div>
            </div>
          </div>

           <div class="col-md-3 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <div class="d-flex justify-content-between align-items-baseline">
                  <h5 class="card-title mb-0">Material Maju</h5>
                </div>
                <div class="row">
                  <div class="col-6 col-md-12 col-xl-5">
                    <h3 class="mb-2">{{count($inovasi->where('bidang_inovasi_id', 5))}}</h3>
                  </div>
                </div>
              </div>
            </div>
          </div>

           <div class="col-md-3 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <div class="d-flex justify-content-between align-items-baseline">
                  <h5 class="card-title mb-0">Kesehatan & Obat</h5>
                </div>
                <div class="row">
                  <div class="col-6 col-md-12 col-xl-5">
                    <h3 class="mb-2">{{count($inovasi->where('bidang_inovasi_id', 6))}}</h3>
                  </div>
                </div>
              </div>
            </div>
          </div>

           <div class="col-md-3 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <div class="d-flex justify-content-between align-items-baseline">
                  <h5 class="card-title mb-0">Teknologi Informasi</h5>
                </div>
                <div class="row">
                  <div class="col-6 col-md-12 col-xl-5">
                    <h3 class="mb-2">{{count($inovasi->where('bidang_inovasi_id', 7))}}</h3>
                  </div>
                </div>
              </div>
            </div>
          </div>

           <div class="col-md-3 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <div class="d-flex justify-content-between align-items-baseline">
                  <h5 class="card-title mb-0">Energi & Sumber Daya Mineral</h5>
                </div>
                <div class="row">
                  <div class="col-6 col-md-12 col-xl-5">
                    <h3 class="mb-2">{{count($inovasi->where('bidang_inovasi_id', 8))}}</h3>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-3 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <div class="d-flex justify-content-between align-items-baseline">
                  <h5 class="card-title mb-0">Transportasi</h5>
                </div>
                <div class="row">
                  <div class="col-6 col-md-12 col-xl-5">
                    <h3 class="mb-2">{{count($inovasi->where('bidang_inovasi_id', 9))}}</h3>
                  </div>
                </div>
              </div>
            </div>
          </div>

                  </div>
              </div>
            </div>

          </div>
        </div>
      </div>

</div>
<!-- bidang inovasi end -->