<!-- inovator & kategori start -->
<div class="row">

  <div class="col-lg-4 col-xl-4 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="d-flex justify-content-between align-items-baseline mb-2">
          <h5 class="card-title mb-0">Total Inovator</h5>
        </div>

        <div class="row">
          <div class="col-12 col-xl-12 stretch-card">
            <div class="row flex-grow">

              <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline">
                      <h5 class="card-title mb-0">Total Inovator</h5>
                    </div>
                    <div class="row">
                      <div class="col-12 col-md-12 col-xl-12">
                        <h3 class="mb-2">{{count($inovator)}}</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div> 

            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="col-lg-8 col-xl-8 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="d-flex justify-content-between align-items-baseline mb-2">
          <h5 class="card-title mb-0">Kategori inovator</h5>
        </div>

        <div class="row">
          <div class="col-12 col-xl-12 stretch-card">
            <div class="row flex-grow">

              <div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline">
                      <h5 class="card-title mb-0">Inovator OPD</h5>
                    </div>
                    <div class="row">
                      <div class="col-6 col-md-12 col-xl-5">
                        <h3 class="mb-2">{{count($inovator->where('category_id', 1))}}</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline">
                      <h5 class="card-title mb-0">Inovator NON OPD</h5>
                    </div>
                    <div class="row">
                      <div class="col-6 col-md-12 col-xl-5">
                        <h3 class="mb-2">{{count($inovator->where('category_id', 2))}}</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
        
      </div> 
    </div>
  </div>

</div>
<!-- inovator &kategori end -->

<!-- jenis inovator -->
<div class="row">

  <div class="col-lg-12 col-xl-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="d-flex justify-content-between align-items-baseline mb-2">
          <h5 class="card-title mb-0">Jenis Inovator</h5>
        </div>

        <div class="row">
          <div class="col-12 col-xl-12 stretch-card">
            <div class="row flex-grow">

              <div class="col-md-3 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline">
                      <h5 class="card-title mb-0">Perorangan</h5>
                    </div>
                    <div class="row">
                      <div class="col-6 col-md-12 col-xl-5">
                        <h3 class="mb-2">{{count($inovator->where('jenis_id', 1))}}</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-3 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline">
                      <h5 class="card-title mb-0">Komunitas</h5>
                    </div>
                    <div class="row">
                      <div class="col-6 col-md-12 col-xl-5">
                        <h3 class="mb-2">{{count($inovator->where('jenis_id', 2))}}</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-3 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline">
                      <h5 class="card-title mb-0">Pelajar</h5>
                    </div>
                    <div class="row">
                      <div class="col-6 col-md-12 col-xl-5">
                        <h3 class="mb-2">{{count($inovator->where('jenis_id', 3))}}</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-3 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline">
                      <h5 class="card-title mb-0">Mahasiswa</h5>
                    </div>
                    <div class="row">
                      <div class="col-6 col-md-12 col-xl-5">
                        <h3 class="mb-2">{{count($inovator->where('jenis_id', 4))}}</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-3 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline">
                      <h5 class="card-title mb-0">Akademisi</h5>
                    </div>
                    <div class="row">
                      <div class="col-6 col-md-12 col-xl-5">
                        <h3 class="mb-2">{{count($inovator->where('jenis_id', 5))}}</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-3 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline">
                      <h5 class="card-title mb-0">Lembaga</h5>
                    </div>
                    <div class="row">
                      <div class="col-6 col-md-12 col-xl-5">
                        <h3 class="mb-2">{{count($inovator->where('jenis_id', 6))}}</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-3 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline">
                      <h5 class="card-title mb-0">Perangkat Daerah</h5>
                    </div>
                    <div class="row">
                      <div class="col-6 col-md-12 col-xl-5">
                        <h3 class="mb-2">{{count($inovator->where('jenis_id', 7))}}</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

</div>
<!-- jenis inovator end -->