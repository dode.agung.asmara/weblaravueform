<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Denpasar City | Research & Development</title>
  <!-- core:css -->
  <link rel="stylesheet" href="{{ url('frontend//assets/')}}/vendors/core/core.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="{{ url('frontend/assets/')}}/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
  <!-- end plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{ url('frontend/assets/')}}/fonts/feather-font/css/iconfont.css">
  <link rel="stylesheet" href="{{ url('frontend/assets/')}}/vendors/flag-icon-css/css/flag-icon.min.css">
  <!-- endinject -->
  <!-- Layout styles -->
  <link rel="stylesheet" href="{{ url('frontend/assets/')}}/css/demo_1/style.css">
  <link rel="stylesheet" href="{{ url('frontend/assets/')}}/css/custom.css">
  <!-- End layout styles -->
  <link rel="shortcut icon" href="{{ url('assets/')}}/img/favicon-new.png" />
</head>
<body>
  <div class="main-wrapper">

    <nav class="sidebar">
      <div class="sidebar-header">
        <a href="#" class="sidebar-brand">
          <img src="{{asset('assets')}}/img/logo-black.jpg">
        </a>
        <div class="sidebar-toggler not-active">
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
      <div class="sidebar-body">
        @include('backend.includes.menu')
      </div>
    </nav>

    <div class="page-wrapper">

      <!-- partial:partials/_navbar.html -->
      @include('backend.includes.topmenuinfo')
      <!-- partial -->

      <div id="app">
        <div class="page-content">
              {{-- <div class="row">
                  <div class="col-md-12 grid-margin">
                    <div class="card">

                        <img src="{{url('frontend/assets/')}}/images/header.jpg" class="img-fluid" alt="Responsive image">

                    </div>
                  </div>
                </div> --}}
                @yield('content')
              </div>
            </div>
            <!-- partial:partials/_footer.html -->
            @include('backend.includes.footer')
            <!-- partial -->
          </div>
        </div>

        <!-- core:js -->
        <script src="{{ url('frontend/assets/')}}/vendors/core/core.js"></script>
        <!-- endinject -->
        <!-- plugin js for this page -->
        <script src="{{ url('frontend/assets/')}}/vendors/chartjs/Chart.min.js"></script>
        <script src="{{ url('frontend/assets/')}}/vendors/jquery.flot/jquery.flot.js"></script>
        <script src="{{ url('frontend/assets/')}}/vendors/jquery.flot/jquery.flot.resize.js"></script>
        <script src="{{ url('frontend/assets/')}}/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
        <script src="{{ url('frontend/assets/')}}/vendors/apexcharts/apexcharts.min.js"></script>
        <script src="{{ url('frontend/assets/')}}/vendors/progressbar.js/progressbar.min.js"></script>
        <!-- end plugin js for this page -->
        <!-- inject:js -->
        <script src="{{ url('frontend/assets/')}}/vendors/feather-icons/feather.min.js"></script>
        <script src="{{ url('frontend/assets/')}}/js/template.js"></script>
        <!-- endinject -->
        <!-- custom js for this page -->
        <script src="{{ url('frontend/assets/')}}/js/dashboard.js"></script>
        <script src="{{ url('frontend/assets/')}}/js/datepicker.js"></script>
        <script src="{{ url('frontend/assets/')}}/js/file-upload.js"></script>
        <!-- <script src="{{asset('assets')}}/js/custom.js"></script> -->
        <script>
          $(document).ready(function(){
            if($('#nav-inovasime').hasClass('active')){
              $("#nav-inovasiall").removeClass("active");
            }
          })
        </script>
        <!-- end custom js for this page -->
        <!-- Scripts -->
        @stack('before-scripts')
        {!! script(mix('js/manifest.js')) !!}
        {!! script(mix('js/vendor.js')) !!}
        {{-- {!! script(mix('js/backend.js')) !!} --}}
        @stack('after-scripts')
        @yield('pagespecificscripts')
      </body>
      </html>
