@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')

<list-all v-if="showListAll" @datacomponent="getDataComponent" :dataprop="choosedData"></list-all>
<form-add v-if="showFormAdd" @datacomponent="getDataComponent" :dataprop="choosedData"></form-add>
<form-edit v-if="showFormEdit" @datacomponent="getDataComponent" :dataprop="choosedData"><form-edit>

@endsection

@section('pagespecificscripts')
	{!! script(mix('js/back-user-inovator.js')) !!}
@stop