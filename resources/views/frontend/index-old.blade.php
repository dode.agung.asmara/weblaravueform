@extends('frontend.layouts.app')
@section('content')
    <page1-component v-if="isShowComponent1"  @datacomponent="getDataComponent" :dataprops="currentData"></page1-component>
    <page2-component v-if="isShowComponent2"  @datacomponent="getDataComponent" :dataprops="currentData"></page2-component>
    <page3-component v-if="isShowComponent3"  @datacomponent="getDataComponent" :dataprops="currentData"></page3-component>
    <page4-component v-if="isShowComponent4"  @datacomponent="getDataComponent" :dataprops="currentData"></page4-component>
    <thankyou-component v-if="isShowThankyouComponent"  @datacomponent="getDataComponent" :dataprops="currentData"></thankyou-component>
@endsection

@section('pagespecificscripts')
    {!! script(mix('js/homepage.js')) !!}
@stop

{{-- @extends('frontend.layouts.app')
@section('content')
    <inovasi1-component v-if="isShowComponent1"  @datacomponent="getDataComponent" :dataprops="currentData"></inovasi1-component>
    <inovasi2-component v-if="isShowComponent2"  @datacomponent="getDataComponent" :dataprops="currentData"></inovasi2-component>
    <inovasi3-component v-if="isShowComponent3"  @datacomponent="getDataComponent" :dataprops="currentData"></inovasi3-component>
    <inovasi4-component v-if="isShowComponent4"  @datacomponent="getDataComponent" :dataprops="currentData"></inovasi4-component>
    <thankyou-component v-if="isShowThankyouComponent"  @datacomponent="getDataComponent" :dataprops="currentData"></thankyou-component>
@endsection

@section('pagespecificscripts')
    {!! script(mix('js/inovasi.js')) !!}
@stop --}}
