@extends('frontend.auth.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.login_box_title'))

@section('content')

<!-- <div class="main-wrapper" style="padding: 60px;"> -->
<div class="main-wrapper">
  <div class="page-wrapper full-page wrapper-login">
    <div class="page-content d-flex align-items-center justify-content-center">

      <div class="row w-100 mx-0 auth-page" >
        <div class="col-md-8 col-xl-6 mx-auto">
          <div class="card mb-0">
            <div class="row">

              <div class="col-md-4 pr-md-0">
                <div class="auth-left-wrapper">
                </div>
              </div>

              <div class="col-md-8 pr-md-0">
                <div class="auth-form-wrapper px-4 py-5">
                  <a href="#" class="noble-ui-logo d-block mb-2 rounded-pill">Login<span>Management</span></a>
                  <h5 class="text-muted font-weight-normal mb-4">@lang('labels.frontend.auth.login_box_title')</h5>
                  {{ html()->form('POST', route('frontend.auth.login.post'))->class('forms-sample')->open() }}

                  <div class="form-group">

                    {{ html()->label(__('validation.attributes.frontend.email'))
                    ->for('email') }}

                    {{ html()->email('email')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.frontend.email'))
                    ->attribute('maxlength', 191)
                    ->required() }}

                  </div>
                  <div class="form-group">
                   {{ html()->label(__('validation.attributes.frontend.password'))->for('password') }}

                   {{ html()->password('password')
                   ->class('form-control')
                   ->placeholder(__('validation.attributes.frontend.password'))
                   ->required() }}

                 </div>
                 <div class="form-check form-check-flat form-check-primary">
<!--                         <label class="form-check-label">
                          <input type="checkbox" class="form-check-input">
                          Remember me
                        </label> -->

                        {{ html()->label(html()
                          ->checkbox('remember', true, 1) . ' ' . __('labels.frontend.auth.remember_me'))->for('remember') }}
                        </div>
                        <div class="mt-3">
                          {{ form_submit(__('labels.frontend.auth.login_button')) }}
                          <!-- <a href="../../dashboard-one.html" class="btn btn-primary mr-2 mb-2 mb-md-0 text-white">Login</a> -->

                        </div>
                        <!-- <a href="/register" class="d-block mt-3 text-muted">Not a user? Sign up</a> -->
                        {{ html()->form()->close() }}
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

      @endsection

      @push('after-scripts')
      @if(config('access.captcha.login'))
      @captchaScripts
      @endif
      <script>
        document.getElementById("footer").style.display="none";
      </script>
      @endpush
