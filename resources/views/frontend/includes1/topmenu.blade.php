
<ul class="navbar-nav">
    <li class="nav-item">
        <a href="{{url('login')}}" class="btn btn-primary btn-icon-text">Login</a>
    </li>
    <li class="nav-item">
        {{-- <a href="{{url('registrasi')}}" class="btn btn-success btn-icon-text">Registrasi</a> --}}
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalRegistrasi">
          Registrasi
      </button>
  </li>
</ul>
