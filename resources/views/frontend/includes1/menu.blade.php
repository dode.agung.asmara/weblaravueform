<ul class="nav">
  <li class="nav-item nav-category">Main</li>
  <li class="nav-item">
    <a href="{{route('frontend.dashboard')}}" class="nav-link">
      <i class="link-icon" data-feather="box"></i>
      <span class="link-title">Dashboard</span>
    </a>
  </li>
  <li class="nav-item">
    <a href="{{route('frontend.map')}}" class="nav-link">
      <i class="link-icon" data-feather="map"></i>
      <span class="link-title">Map</span>
    </a>
  </li>
  <li class="nav-item nav-category">Inovasi</li>
  <li class="nav-item">
    <a href="{{route('frontend.inovasi')}}" class="nav-link">
      <i class="link-icon" data-feather="calendar"></i>
      <span class="link-title">List Inovasi</span>
    </a>
  </li>
</ul>
