@extends('frontend.layouts.app-front')

@section('breadcrumb')
<div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
	<div>
		<h4 class="mb-3 mb-md-0">Map</h4>
	</div>
</div>
@endsection

@section('content')

<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<div class="d-flex justify-content-between align-items-baseline mb-2">
					<h6 class="card-title mb-0">Peta Penyebaran</h6>
				</div>
				<div class="row">
					<div class="col-12 col-md-12 col-xl-12">
						<mapping></mapping>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection

@section('pagespecificscripts')
	{!! script(mix('js/mapping.js')) !!}
@stop