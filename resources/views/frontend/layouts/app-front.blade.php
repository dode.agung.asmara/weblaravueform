<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Denpasar City | Research & Development</title>
  <!-- core:css -->
  <link rel="stylesheet" href="{{ url('frontend//assets/')}}/vendors/core/core.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="{{ url('frontend/assets/')}}/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
  <!-- end plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{ url('frontend/assets/')}}/fonts/feather-font/css/iconfont.css">
  <link rel="stylesheet" href="{{ url('frontend/assets/')}}/vendors/flag-icon-css/css/flag-icon.min.css">
  <!-- endinject -->
  <!-- Layout styles -->
  <link rel="stylesheet" href="{{ url('frontend/assets/')}}/css/demo_1/style.css">
  <link rel="stylesheet" href="{{ url('frontend/assets/')}}/css/custom.css">
  <!-- End layout styles -->
  <link rel="shortcut icon" href="{{ url('assets/')}}/img/favicon-new.png" />
</head>
<body>
  <div class="main-wrapper">

    <div class="page-wrapper">
      <!-- partial:partials/_navbar.html -->
      <div class="horizontal-menu">
        <nav class="sidebar">
          <div class="sidebar-header">
            <a href="#" class="sidebar-brand">
              <!-- Denpasar<span>City</span> -->
              <img src="{{asset('assets')}}/img/logo-black.jpg">
            </a>
            <div class="sidebar-toggler not-active">
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>
          <div class="sidebar-body">
            @include('frontend.includes1.menu')
          </div>
        </nav>
        <nav class="navbar">
          <a href="#" class="sidebar-toggler">
            <i data-feather="menu"></i>
          </a>
          <div class="navbar-content">
        <!-- <form class="search-form">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i data-feather="search"></i>
                    </div>
                </div>
                <input type="text" class="form-control" id="navbarForm" placeholder="Search here...">
            </div>
          </form> -->
          @include('frontend.includes1.topmenu')
        </div>
      </nav>

    </div>
    <!-- partial -->
    <div id="app">
      <div class="page-content">
              <!-- <div class="row">
                  <div class="col-md-12 grid-margin">
                    <div class="card">

                        <img src="{{url('frontend/assets/')}}/images/header.jpg" class="img-fluid" alt="Responsive image">

                    </div>
                  </div>
                </div> -->
                @yield('breadcrumb')

                @yield('content')

              </div>
            </div>

            <!-- Modal Registrasi -->
            <div class="modal fade" id="modalRegistrasi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Registrasi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body text-center">
                    <a href="{{route('frontend.registrasi')}}" class="btn btn-outline-success btn-icon-text">Innovator</a>
                    <a href="{{route('frontend.auditor.registrasi')}}" class="btn btn-outline-primary btn-icon-text">Auditor</a>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>

            <!-- partial:partials/_footer.html -->
            @include('frontend.includes1.footer')
            <!-- partial -->
          </div>
        </div>

        <!-- core:js -->
        <script src="{{ url('frontend/assets/')}}/vendors/core/core.js"></script>
        <!-- endinject -->
        <!-- plugin js for this page -->
        <script src="{{ url('frontend/assets/')}}/vendors/chartjs/Chart.min.js"></script>
        <script src="{{ url('frontend/assets/')}}/vendors/jquery.flot/jquery.flot.js"></script>
        <script src="{{ url('frontend/assets/')}}/vendors/jquery.flot/jquery.flot.resize.js"></script>
        <script src="{{ url('frontend/assets/')}}/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
        <script src="{{ url('frontend/assets/')}}/vendors/apexcharts/apexcharts.min.js"></script>
        <script src="{{ url('frontend/assets/')}}/vendors/progressbar.js/progressbar.min.js"></script>
        <!-- end plugin js for this page -->
        <!-- inject:js -->
        <script src="{{ url('frontend/assets/')}}/vendors/feather-icons/feather.min.js"></script>
        <script src="{{ url('frontend/assets/')}}/js/template.js"></script>
        <!-- endinject -->
        <!-- custom js for this page -->
        <script src="{{ url('frontend/assets/')}}/js/dashboard.js"></script>
        <script src="{{ url('frontend/assets/')}}/js/datepicker.js"></script>
        <script src="{{ url('frontend/assets/')}}/js/file-upload.js"></script>
        <!-- <script src="{{asset('assets')}}/js/custom.js"></script> -->
        <!-- end custom js for this page -->
        <!-- Scripts -->
        @stack('before-scripts')
        {!! script(mix('js/manifest.js')) !!}
        {!! script(mix('js/vendor.js')) !!}
        {{-- {!! script(mix('js/backend.js')) !!} --}}
        @stack('after-scripts')
        @yield('pagespecificscripts')
      </body>
      </html>
