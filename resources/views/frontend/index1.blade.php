@extends('frontend.layouts.app-front')
@section('breadcrumb')
<div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
	<div>
		<h4 class="mb-3 mb-md-0">Dashboard</h4>
	</div>
</div>
@endsection
@section('content')

<!-- innovator, inovasi jenis, bentuk, bidang start -->
@include('frontend.includes.dashboard.inovasi1')
<!-- inovasi jenis, bentuk, bidang end -->

<!-- inovasi inisiator, sistem, tahapan, anggaran, proses, covid start -->
@include('frontend.includes.dashboard.inovasi2')
<!-- inovasi inisiator, sistem, tahapan, anggaran, proses, covid end -->
@endsection