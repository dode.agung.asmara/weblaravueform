<footer id="footer" class="border-0 bg-dark">
	<div class="footer-copyright bg-dark pb-5">
		<div class="container">
			<div class="row">
				<div class="col">
					<p class="text-center text-color-light opacity-5">Porto © 2020. All Rights Reserved.</p>
				</div>
			</div>
		</div>
	</div>
</footer>