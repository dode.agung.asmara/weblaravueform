<!-- inisiator inovasi start -->
<div class="row">

    <div class="col-lg-12 col-xl-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-baseline mb-2">
              <h5 class="card-title mb-0">Inisiator Inovasi</h5>
            </div>

            <div class="row">
              <div class="col-12 col-xl-12 stretch-card">
                <div class="row flex-grow">

                  <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                          <h5 class="card-title mb-0">Kepala Daerah</h5>
                        </div>
                        <div class="row">
                          <div class="col-6 col-md-12 col-xl-5">
                            <h3 class="mb-2">{{count($inovasi->where('inisiator_inovasi_id', 2))}}</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                          <h5 class="card-title mb-0">Anggota DPRD</h5>
                        </div>
                        <div class="row">
                          <div class="col-6 col-md-12 col-xl-5">
                            <h3 class="mb-2">{{count($inovasi->where('inisiator_inovasi_id', 3))}}</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                   <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                          <h5 class="card-title mb-0">OPD</h5>
                        </div>
                        <div class="row">
                          <div class="col-6 col-md-12 col-xl-5">
                            <h3 class="mb-2">{{count($inovasi->where('inisiator_inovasi_id', 4))}}</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                          <h5 class="card-title mb-0">ASN</h5>
                        </div>
                        <div class="row">
                          <div class="col-6 col-md-12 col-xl-5">
                            <h3 class="mb-2">{{count($inovasi->where('inisiator_inovasi_id', 5))}}</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                          <h5 class="card-title mb-0">Masyarakat</h5>
                        </div>
                        <div class="row">
                          <div class="col-6 col-md-12 col-xl-5">
                            <h3 class="mb-2">{{count($inovasi->where('inisiator_inovasi_id', 6))}}</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  </div>
              </div>
            </div>

          </div>
        </div>
      </div>

</div>
<!-- inisiator inovasi end -->

<!-- sistem inovasi start -->
<div class="row">

    <div class="col-lg-12 col-xl-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-baseline mb-2">
              <h5 class="card-title mb-0">Sistem Inovasi</h5>
            </div>

            <div class="row">
              <div class="col-12 col-xl-12 stretch-card">
                <div class="row flex-grow">

                  <div class="col-md-6 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                          <h5 class="card-title mb-0">Digital</h5>
                        </div>
                        <div class="row">
                          <div class="col-6 col-md-12 col-xl-5">
                            <h3 class="mb-2">{{count($inovasi->where('sistem_inovasi_id', 2))}}</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-6 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                          <h5 class="card-title mb-0">Non Digital</h5>
                        </div>
                        <div class="row">
                          <div class="col-6 col-md-12 col-xl-5">
                            <h3 class="mb-2">{{count($inovasi->where('sistem_inovasi_id', 3))}}</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  </div>
              </div>
            </div>

          </div>
        </div>
      </div>

</div>
<!-- sistem inovasi end -->

<!-- tahapan inovasi -->
<div class="row">

    <div class="col-lg-12 col-xl-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-baseline mb-2">
              <h5 class="card-title mb-0">Tahapan Inovasi</h5>
            </div>

            <div class="row">
              <div class="col-12 col-xl-12 stretch-card">
                <div class="row flex-grow">

                  <div class="col-md-4 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <div class="d-flex justify-content-between align-items-baseline">
                  <h5 class="card-title mb-0">Inisiatif</h5>
                </div>
                <div class="row">
                  <div class="col-6 col-md-12 col-xl-5">
                    <h3 class="mb-2">{{count($inovasi->where('tahapan_inovasi_id', 2))}}</h3>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-4 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <div class="d-flex justify-content-between align-items-baseline">
                  <h5 class="card-title mb-0">Uji Coba</h5>
                </div>
                <div class="row">
                  <div class="col-6 col-md-12 col-xl-5">
                    <h3 class="mb-2">{{count($inovasi->where('tahapan_inovasi_id', 3))}}</h3>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-4 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <div class="d-flex justify-content-between align-items-baseline">
                  <h5 class="card-title mb-0">penerapan</h5>
                </div>
                <div class="row">
                  <div class="col-6 col-md-12 col-xl-5">
                    <h3 class="mb-2">{{count($inovasi->where('tahapan_inovasi_id', 4))}}</h3>
                  </div>
                </div>
              </div>
            </div>
          </div>

                  </div>
              </div>
            </div>

          </div>
        </div>
      </div>

</div>
<!-- tahapan inovasi end -->

<!-- anggaran inovasi start -->
<div class="row">

    <div class="col-lg-6 col-xl-6 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-baseline mb-2">
              <h5 class="card-title mb-0">Anggaran Inovasi</h5>
            </div>

            <div class="row">
              <div class="col-12 col-xl-12 stretch-card">
                <div class="row flex-grow">

                  <div class="col-md-6 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                          <h5 class="card-title mb-0">Tersedia</h5>
                        </div>
                        <div class="row">
                          <div class="col-6 col-md-12 col-xl-5">
                            <h3 class="mb-2">{{count($inovasi->where('anggaran_inovasi_id', 2))}}</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-6 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                          <h5 class="card-title mb-0">Belum Tersedia</h5>
                        </div>
                        <div class="row">
                          <div class="col-6 col-md-12 col-xl-5">
                            <h3 class="mb-2">{{count($inovasi->where('anggaran_inovasi_id', 3))}}</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  </div>
              </div>
            </div>

          </div>
        </div>
      </div>

<!-- </div> -->
<!-- anggaran inovasi end -->

<!-- proses inovasi start -->
<!-- <div class="row"> -->

    <div class="col-lg-6 col-xl-6 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-baseline mb-2">
              <h5 class="card-title mb-0">Proses Inovasi</h5>
            </div>

            <div class="row">
              <div class="col-12 col-xl-12 stretch-card">
                <div class="row flex-grow">

                  <div class="col-md-6 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                          <h5 class="card-title mb-0">Tersedia</h5>
                        </div>
                        <div class="row">
                          <div class="col-6 col-md-12 col-xl-5">
                            <h3 class="mb-2">{{count($inovasi->where('proses_inovasi_id', 2))}}</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-6 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                          <h5 class="card-title mb-0">Belum Tersedia</h5>
                        </div>
                        <div class="row">
                          <div class="col-6 col-md-12 col-xl-5">
                            <h3 class="mb-2">{{count($inovasi->where('proses_inovasi_id', 3))}}</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  </div>
              </div>
            </div>

          </div>
        </div>
      </div>

</div>
<!-- proses inovasi end -->

<!-- covid inovasi start -->
<div class="row">

    <div class="col-lg-12 col-xl-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-baseline mb-2">
              <h5 class="card-title mb-0">Covid-19 Inovasi</h5>
            </div>

            <div class="row">
              <div class="col-12 col-xl-12 stretch-card">
                <div class="row flex-grow">

                  <div class="col-md-6 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                          <h5 class="card-title mb-0">Covid-19</h5>
                        </div>
                        <div class="row">
                          <div class="col-6 col-md-12 col-xl-5">
                            <h3 class="mb-2">{{count($inovasi->where('covid_inovasi_id', 2))}}</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-6 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                          <h5 class="card-title mb-0">Non Covid-19</h5>
                        </div>
                        <div class="row">
                          <div class="col-6 col-md-12 col-xl-5">
                            <h3 class="mb-2">{{count($inovasi->where('covid_inovasi_id', 3))}}</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  </div>
              </div>
            </div>

          </div>
        </div>
      </div>

</div>
<!-- covid inovasi end -->