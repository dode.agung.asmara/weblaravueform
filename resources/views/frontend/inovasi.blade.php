@extends('frontend.layouts.app')

@section('content')

<div class="container mt-4 mb-5" style="padding-top: 100px; min-height: 400px;">
	<div id="inovasi" class="row justify-content-center">
		<div class="col-lg-10 text-center">
			<span class="text-color-grey positive-ls-2 font-weight-medium custom-font-size-2 d-block appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">Inovasi</span>
			<h2 class="font-weight-bold text-10 mb-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">Daftar Inovasi</h2>
			<!-- <p class="custom-font-size-2 font-weight-light mb-4 pb-3 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="600">Vestibulum auctor felis eget orci semper vestibulum. Pellentesque ultricies nibh gravida, accumsan libero luctus, molestie nunc. In nibh ipsum, blandit id faucibus ac, finibus vitae dui.</p> -->
		</div>
	</div>
	<div class="custom-svg-wrapper-3">
		<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1677 357" data-plugin-float-element-svg="true">
			<g data-plugin-float-element data-plugin-options="{'startPos': 'top', 'speed': 0.3, 'isInsideSVG': true, 'transition': true, 'transitionDuration': 2000}">
				<circle class="custom-svg-fill-color-secondary" fill="#4F4BFC" cx="36" cy="317" r="17"/>
				<circle fill="#FFFFFF" cx="35.69" cy="317.31" r="15.43"/>
			</g>
			<g data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 0.2, 'isInsideSVG': true, 'transition': true, 'transitionDuration': 2000}">
				<circle fill="#DFDFDB" cx="103.5" cy="224.5" r="9.5"/>
			</g>
			<g data-plugin-float-element data-plugin-options="{'startPos': 'top', 'speed': 0.3, 'isInsideSVG': true, 'transition': true, 'transitionDuration': 2000}">
				<circle class="custom-svg-fill-color-secondary" fill="#5349FF" cx="1561" cy="31" r="6"/>
			</g>
			<g data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 0.2, 'isInsideSVG': true, 'transition': true, 'transitionDuration': 2000}">
				<circle class="custom-svg-fill-color-secondary" fill="#4F4BFC" cx="1628" cy="145.5" r="25"/>
				<circle fill="#FFFFFF" cx="1628.46" cy="145.96" r="22.69"/>
			</g>
		</svg>
		<div class="pricing-table pricing-table-no-gap custom-pricing-table-style-1 mb-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="800">
			<div class="col-md-12">
				<div v-if="showListAll">
					<table-innovation @data-table="getData"></table-innovation>
				</div>
				<div v-if="showFormEdit">
				    <form-edit @data-prop="getData" :dataprop="choosedData"></form-edit>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('pagespecificscripts')
{!! script(mix('js/home.js')) !!}
@endsection