@extends('frontend.layouts.app-front')
@section('breadcrumb')
<div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
	<div>
		<h4 class="mb-3 mb-md-0">Inovasi</h4>
	</div>
</div>
@endsection
@section('content')
<data-inovasi v-if="isShowDataInovasi" @datacomponent="getDataComponent" :dataprops="currentData"></data-inovasi>
<add-inovasi v-if="isShowAddInovasi" @datacomponent="getDataComponent" :dataprops="currentData"></add-inovasi>
<viewinovasi-opd v-if="isShowViewInovasiopd"  @datacomponent="getDataComponent" :dataprops="currentData"></viewinovasi-opd>
<viewinovasi-nonopd v-if="isShowViewInovasinonopd"  @datacomponent="getDataComponent" :dataprops="currentData"></viewinovasi-nonopd>
<viewkuisioner-opd v-if="isShowViewKuisioneropd"  @datacomponent="getDataComponent" :dataprops="currentData"></viewkuisioner-opd>
<viewkuisioner-nonopd v-if="isShowViewKuisionernonopd"  @datacomponent="getDataComponent" :dataprops="currentData"></viewkuisioner-nonopd>
<preview-inovasi v-if="isShowPreviewInovasi" @datacomponent="getDataComponent" :dataprops="currentData"></preview-inovasi>
@endsection

@section('pagespecificscripts')
    {!! script(mix('js/inovasi.js')) !!}
@stop
