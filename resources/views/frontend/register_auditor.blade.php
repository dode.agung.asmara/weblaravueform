@extends('frontend.layouts.app')
@section('content')

<section class="section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 ps-lg-6">
				<h2 class="font-weight-bold-none text-10 text-center pb-3 mb-0">Registrasi Auditor</h2>
				<div class="row">
					<div class="col">
						<div v-if="isShowForm">
                            <form-register @data-register="getData"></form-register>
                        </div>
                        <div v-if="isShowNotification">
                            <notif-register></notif-register>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</section>

@endsection
@section('pagespecificscripts')
{!! script(mix('js/register_auditor.js')) !!}
@endsection