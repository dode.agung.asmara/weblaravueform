<?php

use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\InnovationsController;
use App\Http\Controllers\Backend\UserAuditorController;
use App\Http\Controllers\Backend\UserInovatorController;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::get('innovations', [DashboardController::class, 'inovasi'])->name('innovations');
Route::get('previewinovasi/{id}', [DashboardController::class, 'previewInovasi'])->name('previewInovasi');
Route::get('map', [DashboardController::class, 'mapping'])->name('mapping');
Route::get('map/list', [DashboardController::class, 'listmap'])->name('listmap');
Route::post('map/filter', [DashboardController::class, 'mapfilter'])->name('mapfilter');

//get data
Route::get('getdata', [DashboardController::class, 'getData'])->name('getdata');
Route::get('get/tahapaninovasi', [InnovationsController::class, 'getTahapanInovasi'])->name('getTahapanInovasi');
Route::get('get/inisiatorinovasi', [InnovationsController::class, 'getInisiatorInovasi'])->name('getInisiatorInovasi');
Route::get('get/sisteminovasi', [InnovationsController::class, 'getSistemInovasi'])->name('getSistemInovasi');
Route::get('get/bentukinovasi', [InnovationsController::class, 'getBentukInovasi'])->name('getBentukInovasi');
Route::get('get/jenisinovasi', [InnovationsController::class, 'getJenisInovasi'])->name('getJenisInovasi');
Route::get('get/covidinovasi', [InnovationsController::class, 'getCovidInovasi'])->name('getCovidInovasi');
Route::get('get/bidanginovasi', [InnovationsController::class, 'getBidangInovasi'])->name('getBidangInovasi');
Route::get('get/urusaninovasi', [InnovationsController::class, 'getUrusanInovasi'])->name('getUrusanInovasi');
Route::get('get/anggaraninovasi', [InnovationsController::class, 'getAnggaranInovasi'])->name('getAnggaranInovasi');
Route::get('get/prosesinovasi', [InnovationsController::class, 'getProsesInovasi'])->name('getProsesInovasi');
Route::get('get/videoinovasi', [InnovationsController::class, 'getVideoInovasi'])->name('getVideoInovasi');

//post data
Route::post('form1/innovations', [InnovationsController::class, 'Form1Innovations'])->name('Form1innovations');
Route::post('form2/innovations/{id}', [InnovationsController::class, 'Form2Innovations'])->name('Form2innovations');

//upload file
Route::post('upload/file_anggaran', [InnovationsController::class, 'uploadAnggaran'])->name('upload.file_anggaran');
Route::post('upload/file_proses', [InnovationsController::class, 'uploadProses'])->name('upload.file_proses');
Route::post('upload/logo', [InnovationsController::class, 'uploadLogo'])->name('upload.logo');
Route::post('upload/foto', [InnovationsController::class, 'uploadFoto'])->name('upload.foto');

// user auditor
Route::get('auditor', [UserAuditorController::class, 'index'])->name('auditor');
Route::post('auditor/list', [UserAuditorController::class, 'list']);
Route::get('auditor/status/{id}/{status}', [UserAuditorController::class, 'setstatus']);
Route::post('auditor/edit/{id}', [UserAuditorController::class, 'update']);
Route::post('auditor/role', [UserAuditorController::class, 'role']);

// user inovator
Route::get('inovator', [UserInovatorController::class, 'index'])->name('inovator');
Route::post('inovator/list', [UserInovatorController::class, 'list']);
Route::get('inovator/status/{id}/{status}', [UserInovatorController::class, 'setstatus']);
Route::post('inovator/edit/{id}', [UserInovatorController::class, 'update']);
Route::post('inovator/role', [UserInovatorController::class, 'role']);