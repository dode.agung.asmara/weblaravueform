<?php

use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\InnovationsController;
use App\Http\Controllers\Backend\InnovatorsController;
use App\Http\Controllers\Backend\KuisionerController;
use App\Http\Controllers\Backend\KuisionernonopdController;
use App\Http\Controllers\Backend\KuisioneropdController;
use App\Http\Controllers\Backend\WorkingpermitController;

// All route names are prefixed with 'admin.'.

Route::redirect('/', '/innovators/dashboard', 301);
Route::get('inovasi', [InnovatorsController::class, 'inovasi'])->name('inovasisaya');
Route::get('inovasi/all', [InnovatorsController::class, 'inovasiall'])->name('inovasiall');
Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::get('map', [DashboardController::class, 'mapping'])->name('mapping');
Route::get('map/list', [DashboardController::class, 'listmap'])->name('listmap');
Route::post('map/filter', [DashboardController::class, 'mapfilter'])->name('mapfilter');
Route::get('previewinovasi/{id}', [DashboardController::class, 'previewInovasi']);
Route::get('inovasi/previewinovasi/{id}', [DashboardController::class, 'previewInovasi']);

Route::get('get/inovasi/{id}', [InnovatorsController::class, 'getDataInovasi'])->name('getdatainovasi');
Route::post('update/finalisasi/{id}', [InnovationsController::class, 'updateFinalisasi'])->name('updatefinalisasi');

Route::get('getdata', [InnovationsController::class, 'getData'])->name('getdata');
Route::get('inovasi/getalldata', [InnovationsController::class, 'getallData'])->name('getalldata');
Route::get('getinovasi', [InnovationsController::class, 'getInovasi'])->name('getinovasi');
Route::get('getinnovator', [InnovationsController::class, 'getInnovator'])->name('getinnovator');
Route::get('previewinovasi', [InnovationsController::class, 'previewInovasi'])->name('previewInovasi');
//delete inovasi when innovator cancel input inovasi
Route::post('deleteinovasi/{id}', [InnovationsController::class, 'deleteInovasi'])->name('deleteinovasi');
//delete inovasi when innovator delete inovasi at list inovasi
Route::post('delete/datainovasi/{id}', [InnovationsController::class, 'deleteDatainovasi'])->name('delete.inovasi');

//delete file general
Route::post('delete/files', [InnovationsController::class, 'deleteFiles'])->name('delete.files');

//INNOVATION
//get data inovasi
Route::get('get/tahapaninovasi', [InnovationsController::class, 'getTahapanInovasi'])->name('getTahapanInovasi');
Route::get('get/inisiatorinovasi', [InnovationsController::class, 'getInisiatorInovasi'])->name('getInisiatorInovasi');
Route::get('get/sisteminovasi', [InnovationsController::class, 'getSistemInovasi'])->name('getSistemInovasi');
Route::get('get/bentukinovasi', [InnovationsController::class, 'getBentukInovasi'])->name('getBentukInovasi');
Route::get('get/jenisinovasi', [InnovationsController::class, 'getJenisInovasi'])->name('getJenisInovasi');
Route::get('get/covidinovasi', [InnovationsController::class, 'getCovidInovasi'])->name('getCovidInovasi');
Route::get('get/bidanginovasi', [InnovationsController::class, 'getBidangInovasi'])->name('getBidangInovasi');
Route::get('get/urusaninovasi', [InnovationsController::class, 'getUrusanInovasi'])->name('getUrusanInovasi');
Route::get('get/anggaraninovasi', [InnovationsController::class, 'getAnggaranInovasi'])->name('getAnggaranInovasi');
Route::get('get/prosesinovasi', [InnovationsController::class, 'getProsesInovasi'])->name('getProsesInovasi');
Route::get('get/videoinovasi', [InnovationsController::class, 'getVideoInovasi'])->name('getVideoInovasi');

//post data
Route::post('insert/innovations', [InnovationsController::class, 'InsertInnovations'])->name('insert.innovations');
Route::post('insert/comments', [InnovationsController::class, 'InsertComments'])->name('insert.comments');
Route::post('update/innovations/{id}', [InnovationsController::class, 'UpdateInnovations'])->name('update.innovations');
Route::post('update/status/{id}', [InnovationsController::class, 'UpdateStatus'])->name('update.status');
Route::post('update/comments/{id}', [InnovationsController::class, 'UpdateComments'])->name('update.comments');

// Route::post('form1/innovations', [InnovationsController::class, 'Form1Innovations'])->name('form1innovations');
// Route::post('form2/innovations/{id}', [InnovationsController::class, 'Form2Innovations'])->name('form2innovations');
//updata innovation
// Route::post('update/form1/innovations/{id}', [InnovationsController::class, 'Updateform1Innovations'])->name('updateForm1innovations');
// Route::post('update/form2/innovations/{id}', [InnovationsController::class, 'Updateform2Innovations'])->name('updateform2innovations');

//upload file innovation
Route::post('upload/file_anggaran', [InnovationsController::class, 'uploadAnggaran'])->name('upload.file_anggaran');
Route::post('upload/file_proses', [InnovationsController::class, 'uploadProses'])->name('upload.file_proses');
Route::post('upload/logo', [InnovationsController::class, 'uploadLogo'])->name('upload.logo');
Route::post('upload/photos', [InnovationsController::class, 'uploadPhotos'])->name('upload.photos');

//KUISIONER
//post data
// Route::post('form1/kuisioners', [KuisionerController::class, 'Form1Kuisioners'])->name('form1kuisioners');
// Route::post('form2/kuisioners/{id}', [KuisionerController::class, 'Form2Kuisioners'])->name('form2kuisioners');
// Route::post('form3/kuisioners/{id}', [KuisionerController::class, 'Form3Kuisioners'])->name('form2kuisioners');
//update daata
Route::post('update/kuisioner/{id}', [KuisionerController::class, 'UpdateKuisioners'])->name('updatekuisioners');
// Route::post('update/form2/kuisioners/{id}', [KuisionerController::class, 'Updateform2Kuisioners'])->name('updateform2kuisioners');
// Route::post('update/form3/kuisioners/{id}', [KuisionerController::class, 'Updateform3Kuisioners'])->name('updateform2kuisioners');

//get data kuisioner
Route::get('get/regulasiinovasi', [KuisionerController::class, 'getRegulasiInovasi'])->name('getregulasiinovasi');
Route::get('get/ketersediaansdm', [KuisionerController::class, 'getKetersediaanSdm'])->name('getketersediaansdm');
Route::get('get/dukungananggaran', [KuisionerController::class, 'getDukunganAnggaran'])->name('getdukungananggaran');
Route::get('get/penggunaanit', [KuisionerController::class, 'getPenggunaanIt'])->name('getpenggunaanit');
Route::get('get/bimtek', [KuisionerController::class, 'getBimtek'])->name('getbimtek');
Route::get('get/programrenstra', [KuisionerController::class, 'getProgramRenstra'])->name('getprogramrenstra');
Route::get('get/jejaringinovasi', [KuisionerController::class, 'getJejaringInovasi'])->name('getjejaringinovasi');
Route::get('get/replikasi', [KuisionerController::class, 'getReplikasi'])->name('getreplikasi');
Route::get('get/pedomanteknis', [KuisionerController::class, 'getPedomanTeknis'])->name('getpedomanteknis');;
Route::get('get/layananpengaduan', [KuisionerController::class, 'getLayananPengaduan'])->name('getlayananpengaduan');
Route::get('get/kemudahanlayanan', [KuisionerController::class, 'getKemudahanLayanan'])->name('getkemudahanlayanan');
Route::get('get/kemudahanproses', [KuisionerController::class, 'getKemudahanProses'])->name('getkemudahanproses');
Route::get('get/onlinesistem', [KuisionerController::class, 'getOnlineSistem'])->name('getonlinesistem');
Route::get('get/kecepataninovasi', [KuisionerController::class, 'getKecepatanInovasi'])->name('getkecepataninovasi');
Route::get('get/kemanfaataninovasi', [KuisionerController::class, 'getKemanfaatanInovasi'])->name('getkemanfaataninovasi');
//penambahan
Route::get('get/sosialisasiinovasi', [KuisionerController::class, 'getSosialisasiInovasi'])->name('getsosialisasiinovasi');
Route::get('get/keterlibatanaktor', [KuisionerController::class, 'getKeterlibatanAktor'])->name('getketerlibatanaktor');
Route::get('get/pelaksanainovasi', [KuisionerController::class, 'getPelaksanaInovasi'])->name('getpelaksanainovasi');
Route::get('get/monitoringinovasi', [KuisionerController::class, 'getMonitoringInovasi'])->name('getmonitoringinovasi');


//upload file
//upload file kuisioner 1
Route::post('upload/regulasi_file', [KuisionerController::class, 'uploadRegulasi'])->name('upload.regulasi_file');
Route::post('upload/sdm_file', [KuisionerController::class, 'uploadSdm'])->name('upload.sdm_file');
Route::post('upload/dukungananggaran_file', [KuisionerController::class, 'uploadDukunganAnggaran'])->name('upload.dukungananggaran_file');
Route::post('upload/it_file', [KuisionerController::class, 'uploadIt'])->name('upload.it_file');
Route::post('upload/bimtek_file', [KuisionerController::class, 'uploadBimtek'])->name('upload.bimtek_file');
Route::post('upload/renstra_file', [KuisionerController::class, 'uploadRenstra'])->name('upload.renstra_file');
Route::post('upload/jejaring_file', [KuisionerController::class, 'uploadJejaring'])->name('upload.jejaring_file');
Route::post('upload/replikasi_file', [KuisionerController::class, 'uploadReplikasi'])->name('upload.replikasi_file');
Route::post('upload/rencana_file', [KuisionerController::class, 'uploadRencana'])->name('upload.rencana_file');

//upload file kuisioner 2
Route::post('upload/pedoman_file', [KuisionerController::class, 'uploadPedoman'])->name('upload.pedoman_file');
Route::post('upload/pengelolaan_file', [KuisionerController::class, 'uploadPengelolaan'])->name('upload.pengelolaan_file');
Route::post('upload/info_file', [KuisionerController::class, 'uploadInfo'])->name('upload.info_file');
Route::post('upload/pengaduan_file', [KuisionerController::class, 'uploadPengaduan'])->name('upload.pengaduan_file');
Route::post('upload/stakeholder_file', [KuisionerController::class, 'uploadStakeholder'])->name('upload.stakeholder_file');
Route::post('upload/layanan_file', [KuisionerController::class, 'uploadLayanan'])->name('upload.layanan_file');
Route::post('upload/proses_file', [KuisionerController::class, 'uploadProses'])->name('upload.proses_file');
Route::post('upload/online_file', [KuisionerController::class, 'uploadOnline'])->name('upload.online_file');

//upload file kuisioner 3
Route::post('upload/kecepatan_file', [KuisionerController::class, 'uploadKecepatan'])->name('upload.kecepatan_file');
Route::post('upload/kemanfaatan_file', [KuisionerController::class, 'uploadKemanfaatan'])->name('upload.kemanfaatan_file');
Route::post('upload/kepuasan_file', [KuisionerController::class, 'uploadKepuasan'])->name('upload.kepuasan_file');
Route::post('upload/sosialisasi_file', [KuisionerController::class, 'uploadSosialisasi'])->name('upload.sosialisasi_file');
//penambahan
Route::post('upload/keterlibatan_file', [KuisionerController::class, 'uploadKeterlibatan'])->name('upload.keterlibatan_file');
Route::post('upload/pelaksana_file', [KuisionerController::class, 'uploadPelaksana'])->name('upload.pelaksana_file');
Route::post('upload/monitoring_file', [KuisionerController::class, 'uploadMonitoring'])->name('upload.monitoring_file');

// Route::get('/workingpermit', [WorkingpermitController::class, 'index'])->name('workingpermit');
// Route::get('/workingpermit/show/{params}', [WorkingpermitController::class, 'show'])->name('/workingpermit/show/{params}');
// Route::get('/workingpermit/showall', [WorkingpermitController::class, 'all'])->name('/workingpermit/showall');
