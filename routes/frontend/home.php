<?php

use App\Http\Controllers\Frontend\ContactController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\Blog\BlogController;
use App\Http\Controllers\Frontend\Paketharga\PakethargaController;
use App\Http\Controllers\Frontend\User\AccountController;
use App\Http\Controllers\Frontend\User\DashboardController;
use App\Http\Controllers\Frontend\User\ProfileController;
use App\Http\Controllers\Frontend\Form\FormController;
use App\Http\Controllers\Frontend\InovasiController;
use App\Http\Controllers\Frontend\RegisterAuditorController;

/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
// Route::get('/', function(){
// 	return redirect('map');
// })->name('index');

Route::redirect('/', '/map', 301)->name('index');

Route::get('dashboard', [HomeController::class, 'index'])->name('dashboard');
Route::get('inovasi', [HomeController::class, 'inovasi'])->name('inovasi');
Route::get('inovasi/list', [HomeController::class, 'inovasilist'])->name('inovasilist');

// Register Inovator
Route::get('registrasi', [HomeController::class, 'registrasi'])->name('registrasi');
Route::post('registrasi', [HomeController::class, 'registrasiproses'])->name('registrasiproses');
Route::get('checkemail/{id}', [HomeController::class, 'checkemail'])->name('checkemail');
Route::get('getdata', [HomeController::class, 'getData'])->name('getdata');

// Register Auditor
Route::get('/auditor/registrasi', [RegisterAuditorController::class, 'registrasi'])->name('auditor.registrasi');
Route::post('/auditor/registrasi', [RegisterAuditorController::class, 'registrasiproses']);
Route::get('/auditor/checkemail/{id}', [RegisterAuditorController::class, 'checkemail']);
Route::get('/auditor/getdata', [RegisterAuditorController::class, 'getData']);

// Map
Route::get('map', [HomeController::class, 'mapping'])->name('map');
Route::get('map/list', [HomeController::class, 'listmap'])->name('listmap');
Route::post('map/filter', [HomeController::class, 'mapfilter'])->name('mapfilter');

Route::get('previewinovasi/{id}', [HomeController::class, 'previewInovasi'])->name('previewInovasi');
Route::get('get/urusaninovasi', [HomeController::class, 'getUrusanInovasi'])->name('getUrusanInovasi');

//INNOVATION
//get data inovasi
// Route::get('get/tahapaninovasi', [InovasiController::class, 'getTahapanInovasi'])->name('getTahapanInovasi');
// Route::get('get/inisiatorinovasi', [InovasiController::class, 'getInisiatorInovasi'])->name('getInisiatorInovasi');
// Route::get('get/sisteminovasi', [InovasiController::class, 'getSistemInovasi'])->name('getSistemInovasi');
// Route::get('get/bentukinovasi', [InovasiController::class, 'getBentukInovasi'])->name('getBentukInovasi');
// Route::get('get/jenisinovasi', [InovasiController::class, 'getJenisInovasi'])->name('getJenisInovasi');
// Route::get('get/covidinovasi', [InovasiController::class, 'getCovidInovasi'])->name('getCovidInovasi');
// Route::get('get/bidanginovasi', [InovasiController::class, 'getBidangInovasi'])->name('getBidangInovasi');
// Route::get('get/urusaninovasi', [InovasiController::class, 'getUrusanInovasi'])->name('getUrusanInovasi');
// Route::get('get/anggaraninovasi', [InovasiController::class, 'getAnggaranInovasi'])->name('getAnggaranInovasi');
// Route::get('get/prosesinovasi', [InovasiController::class, 'getProsesInovasi'])->name('getProsesInovasi');
// Route::get('get/videoinovasi', [InovasiController::class, 'getVideoInovasi'])->name('getVideoInovasi');

Route::get('blogs', [BlogController::class, 'index'])->name('blogs');
Route::get('blogs/all', [BlogController::class, 'all'])->name('blogs.all');
Route::get('blog/{slug}', [BlogController::class, 'article'])->name('blog.article');

Route::get('paketharga', [PakethargaController::class, 'index'])->name('paketharga');

Route::get('contact', [ContactController::class, 'index'])->name('contact');
Route::post('contact/send', [ContactController::class, 'send'])->name('contact.send');

//uploading file component 1
Route::post('upload/anggaran_file', [FormController::class, 'uploadAnggaran'])->name('upload.anggaran_file');
Route::post('upload/prosesbisnis_file', [FormController::class, 'uploadProsesbisnis'])->name('upload.prosesbisnis_file');

//uploading file component 2
Route::post('upload/regulasi_file', [FormController::class, 'uploadRegulasi'])->name('upload.regulasi_file');
Route::post('upload/sdm_file', [FormController::class, 'uploadSdm'])->name('upload.sdm_file');
Route::post('upload/dukungananggaran_file', [FormController::class, 'uploadDukunganAnggaran'])->name('upload.dukungananggaran_file');
Route::post('upload/it_file', [FormController::class, 'uploadIt'])->name('upload.it_file');
Route::post('upload/bimtek_file', [FormController::class, 'uploadBimtek'])->name('upload.bimtek_file');
Route::post('upload/renstra_file', [FormController::class, 'uploadRenstra'])->name('upload.renstra_file');
Route::post('upload/jejaring_file', [FormController::class, 'uploadJejaring'])->name('upload.jejaring_file');
Route::post('upload/replikasi_file', [FormController::class, 'uploadReplikasi'])->name('upload.replikasi_file');

//uploading file component 3
Route::post('upload/pedoman_file', [FormController::class, 'uploadPedoman'])->name('upload.pedoman_file');
Route::post('upload/pengelolaan_file', [FormController::class, 'uploadPengelolaan'])->name('upload.pengelolaan_file');
Route::post('upload/info_file', [FormController::class, 'uploadInfo'])->name('upload.info_file');
Route::post('upload/pengaduan_file', [FormController::class, 'uploadPengaduan'])->name('upload.pengaduan_file');
Route::post('upload/stakeholder_file', [FormController::class, 'uploadStakeholder'])->name('upload.stakeholder_file');
Route::post('upload/layanan_file', [FormController::class, 'uploadLayanan'])->name('upload.layanan_file');
Route::post('upload/proses_file', [FormController::class, 'uploadProses'])->name('upload.proses_file');
Route::post('upload/online_file', [FormController::class, 'uploadOnline'])->name('upload.online_file');

//uploading file component 4
Route::post('upload/kecepatan_file', [FormController::class, 'uploadKecepatan'])->name('upload.kecepatan_file');
Route::post('upload/kemanfaatan_file', [FormController::class, 'uploadKemanfaatan'])->name('upload.kemanfaatan_file');
Route::post('upload/kepuasan_file', [FormController::class, 'uploadKepuasan'])->name('upload.kepuasan_file');
Route::post('upload/sosialisasi_file', [FormController::class, 'uploadSosialisasi'])->name('upload.sosialisasi_file');
Route::post('upload/kualitas_file', [FormController::class, 'uploadKualitas'])->name('upload.kualitas_file');

//Post Data Aplikasi
Route::post('/form1/send', [FormController::class, 'form1send'])->name('form1.send');
Route::post('/form2/send/{id}', [FormController::class, 'form2send'])->name('form2.send');
Route::post('/form3/send/{id}', [FormController::class, 'form3send'])->name('form3.send');
Route::post('/form4/send/{id}', [FormController::class, 'form4send'])->name('form4.send');

//Get data name to vue js
Route::get('/form1/get', [FormController::class, 'form1get'])->name('form1.get');
//to component 1 vue js
Route::get('/form1/get/tahapaninovasi', [FormController::class, 'getTahapanInovasi'])->name('form1.getTahapanInovasi');
Route::get('/form1/get/inisiatorinovasi', [FormController::class, 'getInisiatorInovasi'])->name('form1.getTahapanInovasi');
Route::get('/form1/get/jenisinovasi', [FormController::class, 'getJenisInovasi'])->name('form1.getJenisInovasi');
Route::get('/form1/get/bentukinovasi', [FormController::class, 'getBentukInovasi'])->name('form1.getBentukInovasi');
Route::get('/form1/get/covidinovasi', [FormController::class, 'getCovidInovasi'])->name('form1.getCovidInovasi');

//to component 2 vue js
Route::get('/form2/get/regulasiinovasi', [FormController::class, 'getRegulasiInovasi'])->name('form2.getRegulasiInovasi');
Route::get('/form2/get/ketersediaansdm', [FormController::class, 'getKetersediaanSdm'])->name('form2.getKetersediaanSdm');
Route::get('/form2/get/dukungananggaran', [FormController::class, 'getDukunganAnggaran'])->name('form2.getDukunganAnggaran');
Route::get('/form2/get/penggunaanit', [FormController::class, 'getPenggunaanIt'])->name('form2.getPenggunaanIt');
Route::get('/form2/get/bimtek', [FormController::class, 'getBimtek'])->name('form2.getBimtek');
Route::get('/form2/get/programrenstra', [FormController::class, 'getProgramRenstra'])->name('form2.getProgramRenstra');
Route::get('/form2/get/jejaringinovasi', [FormController::class, 'getJejaringInovasi'])->name('form2.getJejaringInovasi');
Route::get('/form2/get/replikasi', [FormController::class, 'getReplikasi'])->name('form2.getReplikasi');

//to component 3 vue js
Route::get('/form3/get/pedomanteknis', [FormController::class, 'getPedomanTeknis'])->name('form3.getPedomanTeknis');
Route::get('/form3/get/pengelolaaninovasi', [FormController::class, 'getPengelolaanInovasi'])->name('form3.getPengelolaanInovasi');
Route::get('/form3/get/infolayanan', [FormController::class, 'getInfoLayanan'])->name('form3.getInfoLayanan');
Route::get('/form3/get/layananpengaduan', [FormController::class, 'getLayananPengaduan'])->name('form3.getLayananPengaduan');
Route::get('/form3/get/stakeholder', [FormController::class, 'getStakeholder'])->name('form3.getStakeholder');
Route::get('/form3/get/kemudahanlayanan', [FormController::class, 'getKemudahanLayanan'])->name('form3.getKemudahanLayanan');
Route::get('/form3/get/kemudahanproses', [FormController::class, 'getKemudahanProses'])->name('form3.getKemudahanProses');
Route::get('/form3/get/onlinesistem', [FormController::class, 'getOnlineSistem'])->name('form3.getOnlineSistem');

//to component 4 vue js
Route::get('/form4/get/kecepataninovasi', [FormController::class, 'getKecepatanInovasi'])->name('form4.getKecepatanInovasi');
Route::get('/form4/get/kemanfaataninovasi', [FormController::class, 'getKemanfaatanInovasi'])->name('form4.getKemanfaatanInovasi');
Route::get('/form4/get/kepuasaninovasi', [FormController::class, 'getKepuasanInovasi'])->name('form4.getKepuasanInovasi');
Route::get('/form4/get/sosialisasi', [FormController::class, 'getSosialisasi'])->name('form4.getSosialisasi');
Route::get('/form4/get/kualitasinovasi', [FormController::class, 'getKualitasInovasi'])->name('form4.getKualitasInovasi');
Route::get('/form4/get/igasinovik', [FormController::class, 'getIgaSinovik'])->name('form4.getIgaSinovik');


/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 * These routes can not be hit if the password is expired
 */
// Route::group(['middleware' => ['auth', 'password_expires']], function () {
//     Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
//         // User Dashboard Specific
//         Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

//         // User Account Specific
//         Route::get('account', [AccountController::class, 'index'])->name('account');

//         // User Profile Specific
//         Route::patch('profile/update', [ProfileController::class, 'update'])->name('profile.update');
//     });
// });
