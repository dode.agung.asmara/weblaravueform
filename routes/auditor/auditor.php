<?php

use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\InnovatorsController;
use App\Http\Controllers\Backend\KuisionerController;
use App\Http\Controllers\Backend\AuditorController;
use App\Http\Controllers\Backend\InnovationsController;
use App\Http\Controllers\Backend\WorkingpermitController;

// All route names are prefixed with 'admin.'.

Route::redirect('/', '/auditor/dashboard', 301);
Route::get('inovasi', [AuditorController::class, 'inovasi'])->name('inovasi');

Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::get('map', [DashboardController::class, 'mapping'])->name('mapping');
Route::get('map/list', [DashboardController::class, 'listmap'])->name('listmap');
Route::post('map/filter', [DashboardController::class, 'mapfilter'])->name('mapfilter');
Route::get('previewinovasi/{id}', [DashboardController::class, 'previewInovasi'])->name('previewInovasi');


Route::get('getdata', [AuditorController::class, 'getData'])->name('getdata');
Route::get('getcommentid/{id}', [AuditorController::class, 'getCommentid'])->name('getCommentid');
Route::get('getinnovator', [InnovationsController::class, 'getInnovator'])->name('getinnovator');
Route::get('getcomment/{id}', [AuditorController::class, 'getComment'])->name('getcomment');
Route::post('insert/comments', [AuditorController::class, 'InsertComments'])->name('insert.comments');
Route::post('update/comments/{id}', [AuditorController::class, 'UpdateComments'])->name('update.comments');
Route::post('update/innovation/{id}', [AuditorController::class, 'UpdateInnovation'])->name('update.innovation');


Route::post('get/file', [InnovationsController::class, 'getFiles'])->name('get.files');
//delete file general
Route::post('delete/files', [InnovationsController::class, 'deleteFiles'])->name('delete.files');
// postComment Auditor
Route::post('comment/auditor/{id}', [AuditorController::class, 'commentAuditor'])->name('commentAuditor');
Route::post('update/commentauditor/{id}', [AuditorController::class, 'updateComment'])->name('update.commentAuditor');
Route::post('update/status/{id}', [AuditorController::class, 'updateStatus'])->name('update.status');

//INNOVATION
//get data inovasi
Route::get('get/tahapaninovasi', [InnovationsController::class, 'getTahapanInovasi'])->name('getTahapanInovasi');
Route::get('get/inisiatorinovasi', [InnovationsController::class, 'getInisiatorInovasi'])->name('getInisiatorInovasi');
Route::get('get/sisteminovasi', [InnovationsController::class, 'getSistemInovasi'])->name('getSistemInovasi');
Route::get('get/bentukinovasi', [InnovationsController::class, 'getBentukInovasi'])->name('getBentukInovasi');
Route::get('get/jenisinovasi', [InnovationsController::class, 'getJenisInovasi'])->name('getJenisInovasi');
Route::get('get/covidinovasi', [InnovationsController::class, 'getCovidInovasi'])->name('getCovidInovasi');
Route::get('get/bidanginovasi', [InnovationsController::class, 'getBidangInovasi'])->name('getBidangInovasi');
Route::get('get/urusaninovasi', [InnovationsController::class, 'getUrusanInovasi'])->name('getUrusanInovasi');
Route::get('get/anggaraninovasi', [InnovationsController::class, 'getAnggaranInovasi'])->name('getAnggaranInovasi');
Route::get('get/prosesinovasi', [InnovationsController::class, 'getProsesInovasi'])->name('getProsesInovasi');
Route::get('get/videoinovasi', [InnovationsController::class, 'getVideoInovasi'])->name('getVideoInovasi');

//post data
Route::post('form1/innovations', [InnovationsController::class, 'Form1Innovations'])->name('form1innovations');
Route::post('form2/innovations/{id}', [InnovationsController::class, 'Form2Innovations'])->name('form2innovations');

//updata innovation
Route::post('update/form1/innovations/{id}', [InnovationsController::class, 'Updateform1Innovations'])->name('updateForm1innovations');
Route::post('update/form2/innovations/{id}', [InnovationsController::class, 'Form2Innovations'])->name('updateform2innovations');

//upload file innovation
Route::post('upload/file_anggaran', [InnovationsController::class, 'uploadAnggaran'])->name('upload.file_anggaran');
Route::post('upload/file_proses', [InnovationsController::class, 'uploadProses'])->name('upload.file_proses');
Route::post('upload/logo', [InnovationsController::class, 'uploadLogo'])->name('upload.logo');
Route::post('upload/photos', [InnovationsController::class, 'uploadPhotos'])->name('upload.photos');

//KUISIONER
//post data
Route::post('form1/kuisioners', [KuisionerController::class, 'Form1Kuisioners'])->name('form1kuisioners');
Route::post('form2/kuisioners/{id}', [KuisionerController::class, 'Form2Kuisioners'])->name('form2kuisioners');
Route::post('form3/kuisioners/{id}', [KuisionerController::class, 'Form3Kuisioners'])->name('form2kuisioners');
//update daata
Route::post('update/form1/kuisioners/{id}', [KuisionerController::class, 'Updateform1Kuisioners'])->name('updateform1kuisioners');
Route::post('update/form2/kuisioners/{id}', [KuisionerController::class, 'Updateform2Kuisioners'])->name('updateform2kuisioners');
Route::post('update/form3/kuisioners/{id}', [KuisionerController::class, 'Updateform3Kuisioners'])->name('updateform2kuisioners');

//get data kuisioner
// kuisioner 1
Route::get('get/regulasiinovasi', [KuisionerController::class, 'getRegulasiInovasi'])->name('getregulasiinovasi');
Route::get('get/ketersediaansdm', [KuisionerController::class, 'getKetersediaanSdm'])->name('getketersediaansdm');
Route::get('get/dukungananggaran', [KuisionerController::class, 'getDukunganAnggaran'])->name('getdukungananggaran');
Route::get('get/penggunaanit', [KuisionerController::class, 'getPenggunaanIt'])->name('getpenggunaanit');
Route::get('get/bimtek', [KuisionerController::class, 'getBimtek'])->name('getbimtek');
Route::get('get/programrenstra', [KuisionerController::class, 'getProgramRenstra'])->name('getprogramrenstra');
Route::get('get/jejaringinovasi', [KuisionerController::class, 'getJejaringInovasi'])->name('getjejaringinovasi');
Route::get('get/replikasi', [KuisionerController::class, 'getReplikasi'])->name('getreplikasi');

// kuisioner 2
Route::get('get/pedomanteknis', [KuisionerController::class, 'getPedomanTeknis'])->name('getpedomanteknis');
Route::get('get/pengelolaaninovasi', [KuisionerController::class, 'getPengelolaanInovasi'])->name('getpengelolaaninovasi');
Route::get('get/infolayanan', [KuisionerController::class, 'getInfoLayanan'])->name('getinfolayanan');
Route::get('get/layananpengaduan', [KuisionerController::class, 'getLayananPengaduan'])->name('getlayananpengaduan');
Route::get('get/stakeholder', [KuisionerController::class, 'getStakeholder'])->name('getstakeholder');
Route::get('get/kemudahanlayanan', [KuisionerController::class, 'getKemudahanLayanan'])->name('getkemudahanlayanan');
Route::get('get/kemudahanproses', [KuisionerController::class, 'getKemudahanProses'])->name('getkemudahanproses');
Route::get('get/onlinesistem', [KuisionerController::class, 'getOnlineSistem'])->name('getonlinesistem');

// kuisioner 3
Route::get('get/kecepataninovasi', [KuisionerController::class, 'getKecepatanInovasi'])->name('getkecepataninovasi');
Route::get('get/kemanfaataninovasi', [KuisionerController::class, 'getKemanfaatanInovasi'])->name('getkemanfaataninovasi');
Route::get('get/kepuasaninovasi', [KuisionerController::class, 'getKepuasanInovasi'])->name('getkepuasaninovasi');
Route::get('get/sosialisasi', [KuisionerController::class, 'getSosialisasi'])->name('getsosialisasi');

//kuisioner non opd
Route::get('get/pengelolanonopd', [KuisionerController::class, 'getPengelolaNonOpd'])->name('getpengelolanonopd');
Route::get('get/regulasinonopd', [KuisionerController::class, 'getRegulasiNonOpd'])->name('getregulasinonopd');
Route::get('get/rencananonopd', [KuisionerController::class, 'getRencanaNonOpd'])->name('getrencananonopd');
Route::get('get/jejaringnonopd', [KuisionerController::class, 'getJejaringNonOpd'])->name('getjejaringnonopd');
//penambahan
Route::get('get/sosialisasiinovasi', [KuisionerController::class, 'getSosialisasiInovasi'])->name('getsosialisasiinovasi');
Route::get('get/keterlibatanaktor', [KuisionerController::class, 'getKeterlibatanAktor'])->name('getketerlibatanaktor');
Route::get('get/pelaksanainovasi', [KuisionerController::class, 'getPelaksanaInovasi'])->name('getpelaksanainovasi');
Route::get('get/monitoringinovasi', [KuisionerController::class, 'getMonitoringInovasi'])->name('getmonitoringinovasi');
//upload file
//upload file kuisioner 1
Route::post('upload/regulasi_file', [KuisionerController::class, 'uploadRegulasi'])->name('upload.regulasi_file');
Route::post('upload/sdm_file', [KuisionerController::class, 'uploadSdm'])->name('upload.sdm_file');
Route::post('upload/dukungananggaran_file', [KuisionerController::class, 'uploadDukunganAnggaran'])->name('upload.dukungananggaran_file');
Route::post('upload/it_file', [KuisionerController::class, 'uploadIt'])->name('upload.it_file');
Route::post('upload/bimtek_file', [KuisionerController::class, 'uploadBimtek'])->name('upload.bimtek_file');
Route::post('upload/renstra_file', [KuisionerController::class, 'uploadRenstra'])->name('upload.renstra_file');
Route::post('upload/jejaring_file', [KuisionerController::class, 'uploadJejaring'])->name('upload.jejaring_file');
Route::post('upload/replikasi_file', [KuisionerController::class, 'uploadReplikasi'])->name('upload.replikasi_file');
Route::post('upload/rencana_file', [KuisionerController::class, 'uploadRencana'])->name('upload.rencana_file');

//upload file kuisioner 2
Route::post('upload/pedoman_file', [KuisionerController::class, 'uploadPedoman'])->name('upload.pedoman_file');
Route::post('upload/pengelolaan_file', [KuisionerController::class, 'uploadPengelolaan'])->name('upload.pengelolaan_file');
Route::post('upload/info_file', [KuisionerController::class, 'uploadInfo'])->name('upload.info_file');
Route::post('upload/pengaduan_file', [KuisionerController::class, 'uploadPengaduan'])->name('upload.pengaduan_file');
Route::post('upload/stakeholder_file', [KuisionerController::class, 'uploadStakeholder'])->name('upload.stakeholder_file');
Route::post('upload/layanan_file', [KuisionerController::class, 'uploadLayanan'])->name('upload.layanan_file');
Route::post('upload/proses_file', [KuisionerController::class, 'uploadProses'])->name('upload.proses_file');
Route::post('upload/online_file', [KuisionerController::class, 'uploadOnline'])->name('upload.online_file');

//upload file kuisioner 3
Route::post('upload/kecepatan_file', [KuisionerController::class, 'uploadKecepatan'])->name('upload.kecepatan_file');
Route::post('upload/kemanfaatan_file', [KuisionerController::class, 'uploadKemanfaatan'])->name('upload.kemanfaatan_file');
Route::post('upload/kepuasan_file', [KuisionerController::class, 'uploadKepuasan'])->name('upload.kepuasan_file');
Route::post('upload/sosialisasi_file', [KuisionerController::class, 'uploadSosialisasi'])->name('upload.sosialisasi_file');




